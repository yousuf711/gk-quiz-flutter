// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appName" : MessageLookupByLibrary.simpleMessage("Gk quiz"),
    "bestScore" : MessageLookupByLibrary.simpleMessage("Best Score"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "checkAfter" : MessageLookupByLibrary.simpleMessage("Check after"),
    "chooseTheColor" : MessageLookupByLibrary.simpleMessage("choose the color"),
    "clearPreviousLevel" : MessageLookupByLibrary.simpleMessage("clear Previous Level.."),
    "coins" : MessageLookupByLibrary.simpleMessage("Coins"),
    "coinsError" : MessageLookupByLibrary.simpleMessage("coins not available"),
    "color" : MessageLookupByLibrary.simpleMessage("Color"),
    "communicate" : MessageLookupByLibrary.simpleMessage("Communicate"),
    "congratulations" : MessageLookupByLibrary.simpleMessage("Congratulations !!"),
    "dailyReward" : MessageLookupByLibrary.simpleMessage("Daily Rewarded"),
    "dashBoard" : MessageLookupByLibrary.simpleMessage("DashBoard"),
    "didThisAppHurtYouPhysically" : MessageLookupByLibrary.simpleMessage("Did this app hurt you physically ?"),
    "done" : MessageLookupByLibrary.simpleMessage("Done"),
    "exit" : MessageLookupByLibrary.simpleMessage("Exit"),
    "exitMsg" : MessageLookupByLibrary.simpleMessage("Are you sure you want to exit?"),
    "feedback" : MessageLookupByLibrary.simpleMessage("Feedback"),
    "fontSize" : MessageLookupByLibrary.simpleMessage("Font Size"),
    "fri" : MessageLookupByLibrary.simpleMessage("Fri"),
    "gameOver" : MessageLookupByLibrary.simpleMessage("Game Over"),
    "gameOverHeader" : MessageLookupByLibrary.simpleMessage("Game Over !!"),
    "gameOverMessage" : MessageLookupByLibrary.simpleMessage("Get 1 lives for continue game."),
    "great3" : MessageLookupByLibrary.simpleMessage("Great ! <3"),
    "highScore" : MessageLookupByLibrary.simpleMessage("High Score :"),
    "history" : MessageLookupByLibrary.simpleMessage("History"),
    "home" : MessageLookupByLibrary.simpleMessage("Home"),
    "level" : MessageLookupByLibrary.simpleMessage("Level"),
    "levels" : MessageLookupByLibrary.simpleMessage("Levels"),
    "lifelineError" : MessageLookupByLibrary.simpleMessage("Use any lifeline at one time..."),
    "liveMessage" : MessageLookupByLibrary.simpleMessage("Get have got 1 lives.."),
    "loading" : MessageLookupByLibrary.simpleMessage("Loading.."),
    "mon" : MessageLookupByLibrary.simpleMessage("Mon"),
    "next" : MessageLookupByLibrary.simpleMessage("Next"),
    "nextQuiz" : MessageLookupByLibrary.simpleMessage("Next Quiz"),
    "nightMode" : MessageLookupByLibrary.simpleMessage("Night Mode"),
    "no" : MessageLookupByLibrary.simpleMessage("No"),
    "noData" : MessageLookupByLibrary.simpleMessage("No data"),
    "ok" : MessageLookupByLibrary.simpleMessage("Ok"),
    "playNextQuiz" : MessageLookupByLibrary.simpleMessage("Play Next Quiz."),
    "plusSign" : MessageLookupByLibrary.simpleMessage("+"),
    "practiceDone" : MessageLookupByLibrary.simpleMessage("Practice Complete."),
    "practiceTitle" : MessageLookupByLibrary.simpleMessage("Practice !!"),
    "privacyPolicy" : MessageLookupByLibrary.simpleMessage("Privacy Policy"),
    "quit" : MessageLookupByLibrary.simpleMessage("Quit !!"),
    "quitMsg" : MessageLookupByLibrary.simpleMessage("Are you sure you want to exit?"),
    "rateUs" : MessageLookupByLibrary.simpleMessage("Rate Us"),
    "reminder" : MessageLookupByLibrary.simpleMessage("Reminder"),
    "repeat" : MessageLookupByLibrary.simpleMessage("Repeat"),
    "retry" : MessageLookupByLibrary.simpleMessage("Retry"),
    "reviewAnswer" : MessageLookupByLibrary.simpleMessage("Review Answer"),
    "sat" : MessageLookupByLibrary.simpleMessage("Sat"),
    "score" : MessageLookupByLibrary.simpleMessage("Score"),
    "selectDays" : MessageLookupByLibrary.simpleMessage("Select Days"),
    "settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "share" : MessageLookupByLibrary.simpleMessage("Share"),
    "showVideo" : MessageLookupByLibrary.simpleMessage("Show Video"),
    "sound" : MessageLookupByLibrary.simpleMessage("Sound"),
    "sp" : MessageLookupByLibrary.simpleMessage("sp"),
    "space" : MessageLookupByLibrary.simpleMessage(" "),
    "start" : MessageLookupByLibrary.simpleMessage("Start"),
    "strVideoError" : MessageLookupByLibrary.simpleMessage("video not loaded.please check network.."),
    "sun" : MessageLookupByLibrary.simpleMessage("Sun"),
    "test" : MessageLookupByLibrary.simpleMessage("Test_"),
    "thatsNotReallyCoolMan" : MessageLookupByLibrary.simpleMessage("That\'s not really cool man"),
    "thisIsCoolLikeThisApp" : MessageLookupByLibrary.simpleMessage("This is cool, like this app."),
    "thu" : MessageLookupByLibrary.simpleMessage("Thu"),
    "time" : MessageLookupByLibrary.simpleMessage("time"),
    "timeOut" : MessageLookupByLibrary.simpleMessage("Time Out !!"),
    "timeText" : MessageLookupByLibrary.simpleMessage("03:00:00"),
    "totalLevel" : MessageLookupByLibrary.simpleMessage("Total Levels"),
    "totalQuestion" : MessageLookupByLibrary.simpleMessage("Total Question"),
    "tue" : MessageLookupByLibrary.simpleMessage("Tue"),
    "useCoins" : MessageLookupByLibrary.simpleMessage("(Use 10 coins)"),
    "vibration" : MessageLookupByLibrary.simpleMessage("Vibration"),
    "viewAnswer" : MessageLookupByLibrary.simpleMessage("View Answer"),
    "watch" : MessageLookupByLibrary.simpleMessage("Watch"),
    "watchVideo" : MessageLookupByLibrary.simpleMessage("Watch Video(Earn 50 coins)"),
    "watchVideoAds" : MessageLookupByLibrary.simpleMessage("Watch Video ads"),
    "wed" : MessageLookupByLibrary.simpleMessage("Wed"),
    "wellItsAverage" : MessageLookupByLibrary.simpleMessage("Well, it\'s average."),
    "yes" : MessageLookupByLibrary.simpleMessage("Yes"),
    "youHaveGot" : MessageLookupByLibrary.simpleMessage("You have got")
  };
}
