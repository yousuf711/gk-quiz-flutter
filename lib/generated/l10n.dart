// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Gk quiz`
  String get appName {
    return Intl.message(
      'Gk quiz',
      name: 'appName',
      desc: '',
      args: [],
    );
  }

  /// `DashBoard`
  String get dashBoard {
    return Intl.message(
      'DashBoard',
      name: 'dashBoard',
      desc: '',
      args: [],
    );
  }

  /// `Reminder`
  String get reminder {
    return Intl.message(
      'Reminder',
      name: 'reminder',
      desc: '',
      args: [],
    );
  }

  /// `Coins`
  String get coins {
    return Intl.message(
      'Coins',
      name: 'coins',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get privacyPolicy {
    return Intl.message(
      'Privacy Policy',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get share {
    return Intl.message(
      'Share',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Rate Us`
  String get rateUs {
    return Intl.message(
      'Rate Us',
      name: 'rateUs',
      desc: '',
      args: [],
    );
  }

  /// `Communicate`
  String get communicate {
    return Intl.message(
      'Communicate',
      name: 'communicate',
      desc: '',
      args: [],
    );
  }

  /// `Total Levels`
  String get totalLevel {
    return Intl.message(
      'Total Levels',
      name: 'totalLevel',
      desc: '',
      args: [],
    );
  }

  /// `Total Question`
  String get totalQuestion {
    return Intl.message(
      'Total Question',
      name: 'totalQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Feedback`
  String get feedback {
    return Intl.message(
      'Feedback',
      name: 'feedback',
      desc: '',
      args: [],
    );
  }

  /// `Level`
  String get level {
    return Intl.message(
      'Level',
      name: 'level',
      desc: '',
      args: [],
    );
  }

  /// `Test_`
  String get test {
    return Intl.message(
      'Test_',
      name: 'test',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get ok {
    return Intl.message(
      'Ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `History`
  String get history {
    return Intl.message(
      'History',
      name: 'history',
      desc: '',
      args: [],
    );
  }

  /// `Game Over !!`
  String get gameOverHeader {
    return Intl.message(
      'Game Over !!',
      name: 'gameOverHeader',
      desc: '',
      args: [],
    );
  }

  /// `Get 1 lives for continue game.`
  String get gameOverMessage {
    return Intl.message(
      'Get 1 lives for continue game.',
      name: 'gameOverMessage',
      desc: '',
      args: [],
    );
  }

  /// `Get have got 1 lives..`
  String get liveMessage {
    return Intl.message(
      'Get have got 1 lives..',
      name: 'liveMessage',
      desc: '',
      args: [],
    );
  }

  /// `Show Video`
  String get showVideo {
    return Intl.message(
      'Show Video',
      name: 'showVideo',
      desc: '',
      args: [],
    );
  }

  /// `video not loaded.please check network..`
  String get strVideoError {
    return Intl.message(
      'video not loaded.please check network..',
      name: 'strVideoError',
      desc: '',
      args: [],
    );
  }

  /// `Game Over`
  String get gameOver {
    return Intl.message(
      'Game Over',
      name: 'gameOver',
      desc: '',
      args: [],
    );
  }

  /// `Time Out !!`
  String get timeOut {
    return Intl.message(
      'Time Out !!',
      name: 'timeOut',
      desc: '',
      args: [],
    );
  }

  /// `Review Answer`
  String get reviewAnswer {
    return Intl.message(
      'Review Answer',
      name: 'reviewAnswer',
      desc: '',
      args: [],
    );
  }

  /// `Retry`
  String get retry {
    return Intl.message(
      'Retry',
      name: 'retry',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get home {
    return Intl.message(
      'Home',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Levels`
  String get levels {
    return Intl.message(
      'Levels',
      name: 'levels',
      desc: '',
      args: [],
    );
  }

  /// `Play Next Quiz.`
  String get playNextQuiz {
    return Intl.message(
      'Play Next Quiz.',
      name: 'playNextQuiz',
      desc: '',
      args: [],
    );
  }

  /// `Next Quiz`
  String get nextQuiz {
    return Intl.message(
      'Next Quiz',
      name: 'nextQuiz',
      desc: '',
      args: [],
    );
  }

  /// `+`
  String get plusSign {
    return Intl.message(
      '+',
      name: 'plusSign',
      desc: '',
      args: [],
    );
  }

  /// `coins not available`
  String get coinsError {
    return Intl.message(
      'coins not available',
      name: 'coinsError',
      desc: '',
      args: [],
    );
  }

  /// `Score`
  String get score {
    return Intl.message(
      'Score',
      name: 'score',
      desc: '',
      args: [],
    );
  }

  /// `Best Score`
  String get bestScore {
    return Intl.message(
      'Best Score',
      name: 'bestScore',
      desc: '',
      args: [],
    );
  }

  /// `Congratulations !!`
  String get congratulations {
    return Intl.message(
      'Congratulations !!',
      name: 'congratulations',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to exit?`
  String get exitMsg {
    return Intl.message(
      'Are you sure you want to exit?',
      name: 'exitMsg',
      desc: '',
      args: [],
    );
  }

  /// `(Use 10 coins)`
  String get useCoins {
    return Intl.message(
      '(Use 10 coins)',
      name: 'useCoins',
      desc: '',
      args: [],
    );
  }

  /// `Exit`
  String get exit {
    return Intl.message(
      'Exit',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Practice !!`
  String get practiceTitle {
    return Intl.message(
      'Practice !!',
      name: 'practiceTitle',
      desc: '',
      args: [],
    );
  }

  /// `Practice Complete.`
  String get practiceDone {
    return Intl.message(
      'Practice Complete.',
      name: 'practiceDone',
      desc: '',
      args: [],
    );
  }

  /// `clear Previous Level..`
  String get clearPreviousLevel {
    return Intl.message(
      'clear Previous Level..',
      name: 'clearPreviousLevel',
      desc: '',
      args: [],
    );
  }

  /// `High Score :`
  String get highScore {
    return Intl.message(
      'High Score :',
      name: 'highScore',
      desc: '',
      args: [],
    );
  }

  /// `View Answer`
  String get viewAnswer {
    return Intl.message(
      'View Answer',
      name: 'viewAnswer',
      desc: '',
      args: [],
    );
  }

  /// `Watch Video ads`
  String get watchVideoAds {
    return Intl.message(
      'Watch Video ads',
      name: 'watchVideoAds',
      desc: '',
      args: [],
    );
  }

  /// `Select Days`
  String get selectDays {
    return Intl.message(
      'Select Days',
      name: 'selectDays',
      desc: '',
      args: [],
    );
  }

  /// `Sun`
  String get sun {
    return Intl.message(
      'Sun',
      name: 'sun',
      desc: '',
      args: [],
    );
  }

  /// `Mon`
  String get mon {
    return Intl.message(
      'Mon',
      name: 'mon',
      desc: '',
      args: [],
    );
  }

  /// `Tue`
  String get tue {
    return Intl.message(
      'Tue',
      name: 'tue',
      desc: '',
      args: [],
    );
  }

  /// `Wed`
  String get wed {
    return Intl.message(
      'Wed',
      name: 'wed',
      desc: '',
      args: [],
    );
  }

  /// `Thu`
  String get thu {
    return Intl.message(
      'Thu',
      name: 'thu',
      desc: '',
      args: [],
    );
  }

  /// `Fri`
  String get fri {
    return Intl.message(
      'Fri',
      name: 'fri',
      desc: '',
      args: [],
    );
  }

  /// `Sat`
  String get sat {
    return Intl.message(
      'Sat',
      name: 'sat',
      desc: '',
      args: [],
    );
  }

  /// `No data`
  String get noData {
    return Intl.message(
      'No data',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `Repeat`
  String get repeat {
    return Intl.message(
      'Repeat',
      name: 'repeat',
      desc: '',
      args: [],
    );
  }

  /// `Sound`
  String get sound {
    return Intl.message(
      'Sound',
      name: 'sound',
      desc: '',
      args: [],
    );
  }

  /// `Night Mode`
  String get nightMode {
    return Intl.message(
      'Night Mode',
      name: 'nightMode',
      desc: '',
      args: [],
    );
  }

  /// `Great ! <3`
  String get great3 {
    return Intl.message(
      'Great ! <3',
      name: 'great3',
      desc: '',
      args: [],
    );
  }

  /// `This is cool, like this app.`
  String get thisIsCoolLikeThisApp {
    return Intl.message(
      'This is cool, like this app.',
      name: 'thisIsCoolLikeThisApp',
      desc: '',
      args: [],
    );
  }

  /// `Well, it's average.`
  String get wellItsAverage {
    return Intl.message(
      'Well, it\'s average.',
      name: 'wellItsAverage',
      desc: '',
      args: [],
    );
  }

  /// `That's not really cool man`
  String get thatsNotReallyCoolMan {
    return Intl.message(
      'That\'s not really cool man',
      name: 'thatsNotReallyCoolMan',
      desc: '',
      args: [],
    );
  }

  /// `Did this app hurt you physically ?`
  String get didThisAppHurtYouPhysically {
    return Intl.message(
      'Did this app hurt you physically ?',
      name: 'didThisAppHurtYouPhysically',
      desc: '',
      args: [],
    );
  }

  /// `Color`
  String get color {
    return Intl.message(
      'Color',
      name: 'color',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get start {
    return Intl.message(
      'Start',
      name: 'start',
      desc: '',
      args: [],
    );
  }

  /// `Loading..`
  String get loading {
    return Intl.message(
      'Loading..',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Watch Video(Earn 50 coins)`
  String get watchVideo {
    return Intl.message(
      'Watch Video(Earn 50 coins)',
      name: 'watchVideo',
      desc: '',
      args: [],
    );
  }

  /// `Watch`
  String get watch {
    return Intl.message(
      'Watch',
      name: 'watch',
      desc: '',
      args: [],
    );
  }

  /// `choose the color`
  String get chooseTheColor {
    return Intl.message(
      'choose the color',
      name: 'chooseTheColor',
      desc: '',
      args: [],
    );
  }

  /// `Quit !!`
  String get quit {
    return Intl.message(
      'Quit !!',
      name: 'quit',
      desc: '',
      args: [],
    );
  }

  /// `Font Size`
  String get fontSize {
    return Intl.message(
      'Font Size',
      name: 'fontSize',
      desc: '',
      args: [],
    );
  }

  /// `sp`
  String get sp {
    return Intl.message(
      'sp',
      name: 'sp',
      desc: '',
      args: [],
    );
  }

  /// `Vibration`
  String get vibration {
    return Intl.message(
      'Vibration',
      name: 'vibration',
      desc: '',
      args: [],
    );
  }

  /// `You have got`
  String get youHaveGot {
    return Intl.message(
      'You have got',
      name: 'youHaveGot',
      desc: '',
      args: [],
    );
  }

  /// `Use any lifeline at one time...`
  String get lifelineError {
    return Intl.message(
      'Use any lifeline at one time...',
      name: 'lifelineError',
      desc: '',
      args: [],
    );
  }

  /// ` `
  String get space {
    return Intl.message(
      ' ',
      name: 'space',
      desc: '',
      args: [],
    );
  }

  /// `Daily Rewarded`
  String get dailyReward {
    return Intl.message(
      'Daily Rewarded',
      name: 'dailyReward',
      desc: '',
      args: [],
    );
  }

  /// `Check after`
  String get checkAfter {
    return Intl.message(
      'Check after',
      name: 'checkAfter',
      desc: '',
      args: [],
    );
  }

  /// `03:00:00`
  String get timeText {
    return Intl.message(
      '03:00:00',
      name: 'timeText',
      desc: '',
      args: [],
    );
  }

  /// `time`
  String get time {
    return Intl.message(
      'time',
      name: 'time',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to exit?`
  String get quitMsg {
    return Intl.message(
      'Are you sure you want to exit?',
      name: 'quitMsg',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}