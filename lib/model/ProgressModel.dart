import 'dart:collection';

class ProgressModel {
  String tableName, type;
  int id;

  int levelNo, isShow;
  int progress;
  int score;
  int highScore;

  ProgressModel.fromMap(dynamic obj) {
    this.id = obj['id'];
    this.progress = obj['progress'];
    this.tableName = obj['tableName'];
    this.levelNo = obj['level_no'];
    this.score = obj['score'];
    this.type = obj['type'];
    this.highScore = obj['high_score'];
    this.isShow = obj['isShow'];
  }

  Map<String, dynamic> toMap() {
    var map = new HashMap<String, dynamic>();
    map['id'] = id;
    map['progress'] = progress;
    map['tableName'] = tableName;
    map['level_no'] = levelNo;
    map['score'] = score;
    map['type'] = type;
    map['high_score'] = highScore;
    map['isShow'] = isShow;
    return map;
  }
}
