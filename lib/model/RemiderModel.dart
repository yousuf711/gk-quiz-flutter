import 'dart:collection';

class ReminderModel {
  int id;
  String time;
  String repeat;
  int ison;


  ReminderModel.fromMap(dynamic obj) {
    this.id = obj['id'];
    this.time = obj['time'];
    this.repeat = obj['repeat'];
    this.ison = obj['ison'];
  }

  Map<String, dynamic> toMap() {
    var map = new HashMap<String, dynamic>();
    map['id'] = id;
    map['time'] = time;
    map['repeat'] = repeat;
    map['ison'] = ison;
    return map;
  }

}
