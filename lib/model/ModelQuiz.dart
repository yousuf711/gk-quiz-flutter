import 'dart:collection';
class ModelQuiz {
  String answer;
  String image;
  bool isVisibleImage;
  List<String> allOptionList = List();
  int type;
  int subCatId;
  String question, option_1, option_2, option_3, option_4;
  String hint;
  int id;



  ModelQuiz();



  ModelQuiz.fromMap(dynamic obj) {
    this.id = obj['id'];
    this.question = obj['question'];
    this.answer = obj['answer'];
    this.image = obj['image'];
    this.subCatId = obj['sub_cat_id'];
    this.option_1 = obj['option_1'];
    this.option_2 = obj['option_2'];
    this.option_3 = obj['option_3'];
    this.option_4 = obj['option_4'];
  }

  Map<String, dynamic> toMap() {
    var map = new HashMap<String, dynamic>();
    map['id'] = id;
    map['question'] = question;
    map['answer'] = answer;
    map['image'] = image;
    map['sub_cat_id'] = subCatId;
    map['option_1'] = option_1;
    map['option_2'] = option_2;
    map['option_3'] = option_3;
    map['option_4'] = option_4;
    return map;
  }


}
