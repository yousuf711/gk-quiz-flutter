import 'dart:convert';

class CoinModel{


   String date;
   int videoCount;


   CoinModel({this.date, this.videoCount});



   CoinModel.fromMap(dynamic obj) {
     this.date = obj['date'];
     this.videoCount = obj['videoCount'];
   }


   static Map<String, dynamic> toMap(CoinModel music) => {
     'date': music.date,
     'videoCount': music.videoCount,

   };



   static String encode(List<CoinModel> musics) => json.encode(
     musics
         .map<Map<String, dynamic>>((music) => CoinModel.toMap(music))
         .toList(),
   );


   factory CoinModel.fromJson(Map<String, dynamic> jsonData) {
     return CoinModel(
       date: jsonData['date'],
       videoCount: jsonData['videoCount'],

     );
   }

   static List<CoinModel> decode(String musics) =>
       (json.decode(musics) as List<dynamic>)
           .map<CoinModel>((item) => CoinModel.fromJson(item))
           .toList();



}