class SubCategories{
   String title, type,icon;
   int   score, levelNo;
   int refId;
   int questionSize;
   String header,  content;
   int id;

   int getId() {
    return id;
  }

   void setId(int id) {
    this.id = id;
  }

   SubCategories.fromMap(dynamic obj) {
     this.id = obj['id'];
     this.title = obj['title'];
     this.refId = obj['ref_id'];
     this.icon = obj['image'];
     this.questionSize = obj['question_size'];
   }






   String getContent() {
    return content;
  }

   void setContent(String content) {
    this.content = content;
  }
  
}