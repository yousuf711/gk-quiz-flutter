// import 'package:ads/ads.dart';
// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_gk_design/PracticePage.dart';
import 'package:flutter_gk_design/QuizPage.dart';
import 'package:flutter_gk_design/ScorePage.dart';
import 'package:flutter_gk_design/generated/l10n.dart';
import 'package:flutter_gk_design/model/ProgressModel.dart';
import 'package:flutter_gk_design/model/SubCategories.dart';
import 'package:flutter_gk_design/util/ConstantData.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';
import 'package:toast/toast.dart';

import 'db/database_helper.dart';
import 'main.dart';
import 'model/DataModel.dart';
import 'model/MainModel.dart';

class LevelPage extends StatefulWidget {
  final SubCategories subCategories;
  final MainModel mainModel;
  // final Ads ad;

  LevelPage(this.mainModel, this.subCategories);
  // LevelPage(this.mainModel, this.subCategories,this.ad);

  @override
  _LevelPage createState() {
    return _LevelPage();
  }
}

class _LevelPage extends State<LevelPage> {
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  List<ProgressModel> progressList = new List();
   // Ads ads;

  void loadAds() {
    //
    // ads = Ads(ConstantData.getAppAdUnitId());
    //
    // var eventListener = (MobileAdEvent event) {
    //   if (event == MobileAdEvent.loaded) {
    //
    //
    //     if(_isClosed){
    //       print("Load---true");
    //       ads.closeBannerAd();
    //       ads?.dispose();
    //     }else{
    //       _isBannerLoaded = true;
    //       setState(() {});
    //     }
    //
    //   }
    //
    //   if (event == MobileAdEvent.opened) {
    //     print("failedToLoad---true");
    //   }
    //   if (event == MobileAdEvent.failedToLoad) {
    //     print("failedToLoad---true");
    //   }
    //
    // };
    //
    // ads.setBannerAd(
    //     adUnitId: ConstantData.getBannerAppId(),
    //     size: AdSize.banner,
    //     keywords: ['andriod, flutter'],
    //     childDirected: true,
    //     testDevices: null,
    //     listener: eventListener);
    //
    // ads.showBannerAd();
    // ads.showBannerAd(state: this, anchorOffset: null);



  }

  Future<bool> _requestPop() {
    closeAds();
    print("back----true");
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyHomePage(),
        ));
    return new Future.value(false);
  }

  void closeAds() {


      if(_isBannerLoaded) {
        // ads.closeBannerAd();
        // ads.dispose();

      }else{
        // ads.hideBannerAd();
      }




      setState(() {

      });


  }

  bool _isBannerLoaded = false;


  // Ads ads;



  @override
  void dispose() {



    super.dispose();
    // ads.closeBannerAd();
    // ads?.dispose();
    print("dispose----true");

  }
  AdmobBannerSize bannerSize;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bannerSize = AdmobBannerSize.BANNER;
    ConstantData.setThemePosition();
    getAllData();
    loadAds();
    setState(() {});
  }

  void getAllData() {
    progressList.clear();
    _databaseHelper
        .getProgressLevels(
            widget.subCategories.title, widget.subCategories.id.toString())
        .then((value) {
      if (value == null) {
        for (int i = 0; i < ConstantData.levelSize; i++) {
          // setState(() {
          _databaseHelper
              .insertLevel(0, widget.subCategories.title, (i + 1),
                  widget.subCategories.id.toString())
              .then((value) {
            print("getval2----$value");
          });
          // });
        }

        _databaseHelper
            .getProgressLevels(
                widget.subCategories.title, widget.subCategories.id.toString())
            .then((value) {
          setState(() {
            progressList.addAll(value);
          });
        });
      } else {
        setState(() {
          progressList.addAll(value);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    double height = SizeConfig.safeBlockVertical * 9;
    double margin = SizeConfig.safeBlockVertical * 2;
    double padding = SizeConfig.safeBlockVertical * 1.5;
    double cellSubSize = SizeConfig.safeBlockVertical * 8;

    return WillPopScope(
        child: Scaffold(
          backgroundColor: ConstantData.backgroundColors,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: ConstantData.backgroundColors,
            centerTitle: true,
            title: Text(widget.subCategories.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    fontFamily: "SFProText",
                    color: ConstantData.textColors)),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: Icon(
                    Icons.keyboard_backspace_outlined,
                    color: ConstantData.textColors,
                  ),
                  onPressed: _requestPop,
                );
              },
            ),
            actions: [
              IconButton(
                icon: Icon(
                  (CupertinoIcons.square_favorites_fill),
                  color: ConstantData.primaryColor,
                  size: 30,
                ),
                onPressed: () {
                  closeAds();

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PracticePage(widget.subCategories, widget.mainModel),
                        // builder: (context) => PracticePage(widget.subCategories, widget.mainModel,ads),
                      )).then((value) {
                    // ads.showBannerAd();
                    // ads.showBannerAd(state: this, anchorOffset: null);
                  });

                  // do something
                },
              ),
            ],
          ),
          body: Container(
            width: double.infinity,
            margin: EdgeInsets.only(
                left: margin,
                right: margin,
                top: margin,
                bottom: _isBannerLoaded ? ConstantData.bannerSize : 5),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(padding),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: ConstantData.getGradientColors(),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(widget.subCategories.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              fontFamily: "SFProText",
                              color: Colors.white)),
                      Container(
                        height: height,
                        padding: EdgeInsets.only(top: 15),
                        child: Row(
                          children: [
                            Expanded(
                              child: getLevelWidget(
                                  ConstantData.levelSize.toString(),
                                  S.of(context).totalLevel),
                              flex: 1,
                            ),
                            Container(
                              width: 3,
                              color: Colors.white,
                              height: double.infinity,
                            ),
                            Expanded(
                              child: getLevelWidget(
                                  widget.subCategories.questionSize.toString(),
                                  S.of(context).totalQuestion),
                              flex: 1,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: progressList.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: Container(
                          width: double.infinity,
                          // height: cellSize,
                          child: Card(
                            color: ConstantData.cardBackground,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            margin: EdgeInsets.only(top: 15),
                            child: Container(
                              padding: EdgeInsets.only(
                                  right: margin, top: 10, bottom: 10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 15, right: 15),
                                    height: cellSubSize,
                                    color: ConstantData.backgroundColors,
                                    width: cellSubSize,
                                    child: Icon(
                                      (progressList[index].isShow == 1)
                                          ? Icons.lock_open_outlined
                                          : Icons.lock_outline,
                                      size: 30,
                                      color: ConstantData.primaryColor,
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 2),
                                        child: Text(
                                            S.of(context).level +
                                                " " +
                                                progressList[index]
                                                    .levelNo
                                                    .toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18,
                                                fontFamily: "SFProText",
                                                color:
                                                    ConstantData.textColors)),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.only(top: 3, left: 2),
                                        child: Text(
                                            S.of(context).highScore + " " +
                                                progressList[index].highScore.toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 13,
                                                fontFamily: "SFProText",
                                                color:
                                                    ConstantData.textColors)),
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            (progressList[index].progress > 0)
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: 24,
                                            color: ConstantData.primaryColor,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                left: 3, right: 3),
                                            child: Icon(
                                              (progressList[index].progress > 1)
                                                  ? Icons.star
                                                  : Icons.star_border,
                                              size: 24,
                                              color: ConstantData.primaryColor,
                                            ),
                                          ),
                                          Icon(
                                            (progressList[index].progress > 2)
                                                ? Icons.star
                                                : Icons.star_border,
                                            size: 24,
                                            color: ConstantData.primaryColor,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  new Spacer(),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.navigate_next,
                                        color: ConstantData.primaryColor,
                                      ))
                                ],
                              ),
                            ),
                          ),
                        ),
                        onTap: () {
                          if (progressList[index].isShow == 1) {
                            closeAds();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => QuizPage(
                                  progressModel: progressList[index],
                                  subCatModel: widget.subCategories,
                                  mainModel: widget.mainModel,
                                  // ad:  ads,
                                  onChanged: _handleTapboxChanged,
                                ),
                              ),
                            ) .then((value) {
                              // ads.showBannerAd();
                              // ads.showBannerAd(state: this, anchorOffset: null);
                            });
                          } else {
                            Toast.show(
                                S.of(context).clearPreviousLevel, context,
                                duration: Toast.LENGTH_SHORT,
                                gravity: Toast.BOTTOM);
                          }
                        },
                      );
                    },
                  ),
                  flex: 1,
                )
              ],
            ),
          ),
          bottomNavigationBar: Container(
            height: 55,
            color: ConstantData.backgroundColors,
            child:AdmobBanner(
              adUnitId: ConstantData.getBannerAdUnitId(),
              adSize: bannerSize,
              listener: (AdmobAdEvent event,
                  Map<String, dynamic> args) {
                handleEvent(event, args, 'Banner');
              },
              onBannerCreated:
                  (AdmobBannerController controller) {


              },
            ),
          ),
        ),
        onWillPop: _requestPop);
  }
  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print("loaded--true");
        setState(() {});
        break;
      case AdmobAdEvent.opened:
        break;
      case AdmobAdEvent.closed:
        break;
      case AdmobAdEvent.failedToLoad:
        break;

      default:
    }
  }

  void _handleTapboxChanged(DataModel newValue) {
    if (newValue != null) {
      closeAds();

      print("finish===true---" + newValue.subCatModel.title);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ScorePage(
                  subCatModel: newValue.subCatModel,
                  modelCat: newValue.modelCat,
                  progressModel: newValue.progressModel,
                  rightCount: newValue.rightCount,
                  wrongCount: newValue.wrongCount,
                  refId: newValue.refId,
                  // ad: ads,
                  onChanged: onChange,
                )),
      ).then((value) {
        // ads.showBannerAd();
        // ads.showBannerAd(state: this, anchorOffset: null);
      });
    }
  }

  void onChange(bool newValue) {
    print("score----true0");
    getAllData();
    setState(() {});
  }

  Widget getLevelWidget(String s, String name) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(s,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
                fontFamily: "SFProText",
                color: Colors.white)),
        Text(name,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 15,
                fontFamily: "SFProText",
                color: Colors.white)),
      ],
    );
  }
}

