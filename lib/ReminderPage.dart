import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import 'DaysDialog.dart';
import 'db/database_helper.dart';
import 'package:intl/intl.dart';
import 'generated/l10n.dart';
import 'model/RemiderModel.dart';
import 'util/ConstantData.dart';
import 'util/PrefData.dart';
import 'util/SizeConfig.dart';
import 'util/ThemeColor.dart';

class ReminderPage extends StatefulWidget {
  @override
  _ReminderPage createState() => _ReminderPage();
}

class _ReminderPage extends State<ReminderPage> with TickerProviderStateMixin {
  List<String> dayList = new List();
  List<String> _selectedOrders = new List();
  List<String> _tempSelectedDays = [];

  List<ReminderModel> reminderList = new List();
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  TimeOfDay time;
  String insertTime;

  AnimationController animationController;

  void _getReminderList() {
    // modelQuizList =  List();
    print("gfgh--56767");

    reminderList.clear();
    _databaseHelper.getReminderList().then((value) {
      print("gfgh12--$value");
      setState(() {
        if (value != null) {
          print("gfgh--" + value.length.toString());
          reminderList = value;
        }
      });
    });
  }
  void _addDayList() {
    // modelQuizList =  List();
    print("gfgh--56767");

    dayList.clear();

    dayList.add(S.of(context).sun);
    dayList.add(S.of(context).mon);
    dayList.add(S.of(context).tue);
    dayList.add(S.of(context).wed);
    dayList.add(S.of(context).thu);
    dayList.add(S.of(context).fri);
    dayList.add(S.of(context).sat);
  }


  Future<bool> _requestPop() {
    print("back----true");
    Navigator.of(context).pop();
    return new Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ConstantData.setThemePosition();
    _addDayList();

    return WillPopScope(child:  Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ConstantData.backgroundColors,
        centerTitle: true,
        title: Text(S.of(context).reminder,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
                fontFamily: "SFProText",
                color: ConstantData.textColors)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(
                Icons.keyboard_backspace_outlined,
                color: ConstantData.textColors,
              ),
              onPressed: _requestPop,
            );
          },
        ),

      ),

      backgroundColor: ConstantData.backgroundColors,
      body: Container(
          color: ConstantData.backgroundColors,

          padding: EdgeInsets.all(10),
          child: _listView()),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(25),
        child: FloatingActionButton(
          backgroundColor: ConstantData.primaryColor,
          foregroundColor: Colors.white,
          onPressed: () {
            _addRemiders();
          },
          child: Icon(Icons.add),
        ),
      ),
    ), onWillPop: _requestPop);
  }

  Widget _listView() {
    if (reminderList.length > 0) {
      return Container(

        child: ListView.builder(

            shrinkWrap: true,
            itemCount: reminderList.length,
            itemBuilder: (BuildContext context, int index) {
              final Animation<double> anm =
              Tween<double>(begin: 0.0, end: 1.0).animate(
                // Tween<double>(begin: 0.0, end: 1.0).animate(
                CurvedAnimation(
                  parent: animationController,
                  curve: Interval((1 / reminderList.length) * index, 1.0,
                      curve: Curves.fastOutSlowIn),
                ),
              );
              animationController.forward();

              return Container(
                child: ReminderItem(
                  animationController: animationController,
                  animation: anm,
                  reminderModel: reminderList[index],
                  onChanged: _handleTapboxChanged,
                ),
              );
            }),
      );
    } else {
      return Center(
        child: Text(
          S.of(context).noData,
          style: TextStyle(
              fontFamily: "SFProText",
              fontWeight: FontWeight.normal, fontSize: 20, color: Colors.black),
        ),
      );
    }
  }

  void _handleTapboxChanged(ReminderModel newValue) {
    setState(() {
      _getReminderList();
    });
  }

  _pickTime() async {
    _showTimePicker();
    // time = TimeOfDay.now();

    // TimeOfDay t = await showTimePicker(context: context, initialTime: time);
    // if (t != null)
    //   setState(() {
    //     time = t;
    //     String s = time.hour.toString() + ":" + time.minute.toString();
    //     insertTime = s;
    //     // _repeatDayDialog();
    //
    //     _selectedOrders.clear();
    //     _tempSelectedDays.clear();
    //     _selectedOrders.addAll(dayList);
    //     _tempSelectedDays.addAll(dayList);
    //     showDialog(
    //         context: context,
    //         builder: (BuildContext context) {
    //           return _MyDialog(
    //             cities: dayList,
    //             selectedDays: _selectedOrders,
    //             onSelectedDaysListChanged: (days) {
    //               setState(() {
    //                 _tempSelectedDays = days;
    //               });
    //             },
    //             onSelectedOkChanged: (d) {
    //               setState(() {
    //                 print(
    //                     "daysvalue---" + _tempSelectedDays.toString() + "----");
    //                 _databaseHelper
    //                     .insertReminder(
    //                         insertTime, _tempSelectedDays.toString(), 1)
    //                     .then((value) => {print("object-value--$value")});
    //
    //                 _getReminderList();
    //               });
    //             },
    //           );
    //         });
    //   });
  }

  Future<void> _showTimePicker() async {
    TimeOfDay initialTime = TimeOfDay.now();
    TimeOfDay pickedTime = await showTimePicker(
      context: context,
      initialTime: initialTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark().copyWith(
              colorScheme: ColorScheme.dark(
            primary: ConstantData.primaryColor,
            onPrimary: Colors.white,
            surface: ConstantData.cardBackground,
            onSurface: ConstantData.textColors,
            onBackground: ConstantData.primaryColor,
          )),
          child: child,
        );

        // return Directionality(
        //   textDirection: TextDirection.rtl,
        //   child: child,
        // );
      },
    );

    if (pickedTime != null)
      setState(() {
        final now = new DateTime.now();
        DateTime selectTime = new DateTime(
            now.year, now.month, now.day, pickedTime.hour, pickedTime.minute);

        final DateFormat formatter = DateFormat('hh:mm a');

        final String formatted = formatter.format(selectTime);

        print("pickTime--" + formatted);

        time = pickedTime;
        insertTime = formatted;

        _selectedOrders.clear();
        _tempSelectedDays.clear();
        _selectedOrders.addAll(dayList);
        _tempSelectedDays.addAll(dayList);

        final alert = DaysDialog(
          dayList: dayList,
          selectedList: _tempSelectedDays,
          onSelectedOkChanged: (value) {
            if (value != null) {
              _tempSelectedDays = value;
              setState(() {
                _databaseHelper
                    .insertReminder(insertTime, _tempSelectedDays.toString(), 1)
                    .then((value) => {print("object-value--$value")});
                _getReminderList();
              });
            }
          },
        );

        showDialog(
          context: context,
          builder: (_) {
            return alert;
          },
        );

        // showDialog(
        //     context: context,
        //     builder: (BuildContext context) {
        //       return _MyDialog(
        //         cities: dayList,
        //         selectedDays: _selectedOrders,
        //         onSelectedDaysListChanged: (days) {
        //           setState(() {
        //             _tempSelectedDays = days;
        //           });
        //         },
        //         onSelectedOkChanged: (d) {
        //           setState(() {
        //             print(
        //                 "daysvalue---" + _tempSelectedDays.toString() + "----");
        //             _databaseHelper
        //                 .insertReminder(
        //                     insertTime, _tempSelectedDays.toString(), 1)
        //                 .then((value) => {print("object-value--$value")});
        //
        //             _getReminderList();
        //           });
        //         },
        //       );
        //     });
      });
  }

  _addRemiders() {
    _pickTime();
  }

  Widget checkbox(String title, bool boolValue) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        new Checkbox(
          value: boolValue,
          // tristate: true,
          onChanged: (bool value) {},
          activeColor: ConstantData.primaryColor,
        ),
        Text(title),
      ],
    );
  }

  @override
  void initState() {
    setThemePosition();

    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _getReminderList();


    super.initState();
  }

  setThemePosition() async {
    int posGet = await PrefData.getIsDarkMode();
    ThemeData themeData = ThemeColor.themesList[posGet];
    ConstantData.backgroundColors = themeData.backgroundColor;
    ConstantData.cardBackground = themeData.cardColor;
    ConstantData.textColors = themeData.textSelectionColor;
  }
}

class ReminderItem extends StatefulWidget {
  final  AnimationController animationController;
  final Animation<dynamic> animation;
  final  ReminderModel reminderModel;

  // final ValueChanged onReminderModelChanged;

  final ValueChanged<ReminderModel> onChanged;

  ReminderItem(
      {this.animationController,
      this.animation,
      this.reminderModel,
      this.onChanged});

  @override
  _ReminderItem createState() => _ReminderItem(
      this.animationController, this.animation, this.reminderModel);
}

class _ReminderItem extends State<ReminderItem> {
  AnimationController _animationController;
  ReminderModel reminderModel;
  final Animation<dynamic> animation;
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  _ReminderItem(this._animationController, this.animation, this.reminderModel);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 50 * (1.0 - animation.value), 0.0),
              child: Card(
                  margin: EdgeInsets.all(10),
                  color: ConstantData.cardBackground,
                  elevation: 1.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7),
                  ),
                  child: Container(
                    margin: EdgeInsets.all(8),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 8),
                              child: Text(
                                reminderModel.time,
                                style: TextStyle(
                                    fontFamily: "SFProText",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: ConstantData.textColors,
                                    decoration: TextDecoration.none),
                              ),
                            ),
                            new Spacer(),
                            Switch(
                              value: (reminderModel.ison == 1 ? true : false),
                              onChanged: (value) {
                                setState(() {
                                  if (value) {
                                    _databaseHelper.updateReminder(
                                        1, reminderModel.id);
                                  } else {
                                    _databaseHelper.updateReminder(
                                        0, reminderModel.id);
                                  }
                                  widget.onChanged(reminderModel);
                                });
                              },
                              activeTrackColor:
                                  ConstantData.primaryColor.withAlpha(45),
                              activeColor: ConstantData.primaryColor,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    S.of(context).repeat,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15,
                                        color: ConstantData.textColors,
                                        decoration: TextDecoration.none),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 5, bottom: 8),
                                    child: Text(
                                      reminderModel.repeat
                                          .replaceAll("[", "")
                                          .replaceAll("]", ""),
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,
                                          fontFamily: "SFProText",
                                          color: ConstantData.textColors,
                                          decoration: TextDecoration.none),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            new Spacer(),
                            GestureDetector(
                              child: Container(
                                margin: EdgeInsets.only(right: 8),
                                child: Icon(
                                  Icons.delete,
                                  color: ConstantData.primaryColor,
                                  size: 28,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  _databaseHelper
                                      .deleteRemider(reminderModel.id);
                                  widget.onChanged(reminderModel);
                                });
                              },
                            )
                          ],
                        ),
                      ],
                    ),
                  )),
            ),
          );
        });
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.cities,
    this.selectedDays,
    this.onSelectedDaysListChanged,
    this.onSelectedOkChanged,
  });

  final List<String> cities;
  final List<String> selectedDays;
  final ValueChanged<List<String>> onSelectedDaysListChanged;
  final ValueChanged<List<String>> onSelectedOkChanged;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  List<String> _tempSelectedDays = [];

  @override
  void initState() {
    _tempSelectedDays = widget.selectedDays;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        color: ConstantData.cardBackground,
        height: MediaQuery.of(context).size.height * .50,
        child: Column(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 2,
                  margin: EdgeInsets.only(top: 15),
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                  ),
                ),
                Center(
                  child: Text(
                    S.of(context).selectDays,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        fontFamily: "SFProText",
                        color: ConstantData.primaryColor,
                        decoration: TextDecoration.none),
                  ),
                ),
                Container(
                  height: 2,
                  margin: EdgeInsets.only(top: 15, right: 20, left: 20),
                  decoration: BoxDecoration(
                    color: ConstantData.primaryColor,
                  ),
                  child: Container(),
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: widget.cities.length,
                  itemBuilder: (BuildContext context, int index) {
                    final cityName = widget.cities[index];
                    return Container(
                      height: MediaQuery.of(context).size.height * .05,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: CheckboxListTile(
                          title: Text(cityName),
                          activeColor: ConstantData.primaryColor,
                          value: _tempSelectedDays.contains(cityName),
                          onChanged: (bool value) {
                            if (value) {
                              if (!_tempSelectedDays.contains(cityName)) {
                                setState(() {
                                  _tempSelectedDays.add(cityName);
                                });
                              }
                            } else {
                              if (_tempSelectedDays.contains(cityName)) {
                                setState(() {
                                  _tempSelectedDays.removeWhere(
                                      (String city) => city == cityName);
                                });
                              }
                            }
                            widget.onSelectedDaysListChanged(_tempSelectedDays);
                          }),
                    );
                  }),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              // textDirection: TextDirection.rtl,
              children: [
                new FlatButton(
                    child: new Text(
                      S.of(context).cancel,
                      style: TextStyle(color: ConstantData.primaryColor),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                new FlatButton(
                  child: new Text(
                    S.of(context).ok,
                    style: TextStyle(color: ConstantData.primaryColor),
                  ),
                  onPressed: () {
                    widget.onSelectedOkChanged(_tempSelectedDays);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
