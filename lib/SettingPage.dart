import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gk_design/util/scroll_picker.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:toast/toast.dart';

import 'generated/l10n.dart';
import 'util/ConstantData.dart';
import 'util/PrefData.dart';
import 'util/SizeConfig.dart';
import 'util/ThemeColor.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPage createState() => _SettingPage();
}

class _SettingPage extends State<SettingPage> with TickerProviderStateMixin {
  Future<bool> _requestPop() {
    print("back----true");
    Navigator.of(context).pop();
    return new Future.value(true);
  }

  int _isReminder;
  bool _isSound = true;
  bool _isVibration = true;
  int _isNightMode;
  int _fontSize = 16;
  int value = 16;

  RateMyApp _rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
    googlePlayIdentifier: 'com.example.flutter_gk_design',
    // googlePlayIdentifier: 'fr.skyost.example',
    appStoreIdentifier: '1491556149',
  );

  List<String> usStates = <String>[
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ConstantData.setThemePosition();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    _setReminder();
    _setSound();
    _setVibration();
    _setModes();
    _setFontSize();

    _rateMyApp.init().then((_) {
      _rateMyApp.conditions.forEach((condition) {
        if (condition is DebuggableCondition) {
          // print(condition.valuesAsString());
          // We iterate through our list of conditions and we print all debuggable ones.
        }
      });

      print('Are all conditions met ? ' +
          (_rateMyApp.shouldOpenDialog ? 'Yes' : 'No'));

      // if (_rateMyApp.shouldOpenDialog) _buildShowStarRateDialog(context);
    });
  }

  Future<void> _setFontSize() async {
    _fontSize = await PrefData().getFontSize();
    value = _fontSize;
    setState(() {});
  }

  Future<void> _setModes() async {
    _fontSize = await PrefData().getFontSize();
    _isNightMode = await PrefData.getIsDarkMode();

    setState(() {
      ConstantData.setThemePosition();
    });
  }

  Future<void> _setReminder() async {
    _isReminder = await PrefData.getReminder();
    setState(() {
      print("_isReminder----" + _isReminder.toString());
    });
  }

  Future<void> _setSound() async {
    _isSound = await PrefData.getSound();
    setState(() {});
  }
Future<void> _setVibration() async {
    _isVibration = await PrefData.getVibration();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ConstantData.setThemePosition();

    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: ConstantData.backgroundColors,
            centerTitle: true,
            title: Text(S.of(context).settings,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    fontFamily: "SFProText",
                    color: ConstantData.textColors)),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: Icon(
                    Icons.keyboard_backspace_outlined,
                    color: ConstantData.textColors,
                  ),
                  onPressed: _requestPop,
                );
              },
            ),
          ),
          backgroundColor: ConstantData.backgroundColors,
          body:
              // Expanded(child:  Container(color: Colors.black,),flex: 1,)
              Container(
                  height: double.infinity,
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: ListView(
                    primary: true,
                    shrinkWrap: true,
                    children: [
                      getVibrationCell(),
                      getSoundCell(),
                      getModeCell(),
                      getReminderCell(),
                      getColorCell(),
                      InkWell(
                        child: getFontCell(),
                        onTap: () {
                          showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(8))),
                                  child: Wrap(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        padding: EdgeInsets.all(
                                            SizeConfig.safeBlockVertical * 3),
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              begin: Alignment.topRight,
                                              end: Alignment.bottomLeft,
                                              colors: ConstantData
                                                  .getGradientColors(),
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              S.of(context).fontSize,
                                              textAlign: TextAlign.right,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                  fontFamily: "SFProText",
                                                  color: Colors.white),
                                            ),
                                            ScrollPicker(
                                              items: usStates,
                                              initialValue:
                                                  _fontSize.toString(),
                                              onChanged: (v) {
                                                print("value---$v");
                                                value = int.parse(v);
                                                print("value---$value");
                                              },
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(top: 20),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  InkWell(
                                                      child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  right: 15),
                                                          child: Text(
                                                            S.of(context).ok,
                                                            textAlign:
                                                                TextAlign.right,
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "SFProText",
                                                                color: Colors
                                                                    .white),
                                                          )),
                                                      onTap: () {
                                                        PrefData.addFontSize(
                                                            value);

                                                        print("font---" +
                                                            value.toString());
                                                        _setFontSize();
                                                        Navigator.pop(context);
                                                      }),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              });
                        },
                      ),
                      InkWell(
                        child: getSocialCell(
                            Icons.privacy_tip, S.of(context).privacyPolicy),
                        onTap: () {
                          ConstantData.launchURL("https://google.com");
                        },
                      ),
                      InkWell(
                        child: getSocialCell(Icons.share, S.of(context).share),
                        onTap: () {
                          ConstantData.shareApp(context);
                        },
                      ),
                      InkWell(
                        child:
                            getSocialCell(Icons.thumb_up, S.of(context).rateUs),
                        onTap: () {
                          _buildShowStarRateDialog(context);
                        },
                      ),
                      InkWell(
                        child:
                            getSocialCell(Icons.send, S.of(context).feedback),
                        onTap: () {
                          ConstantData.launchEmail('demo@gmail.com',
                              'Flutter Email Test', 'Hello Flutter');
                        },
                      )
                    ],
                  )),
        ),
        onWillPop: _requestPop);
  }

  _buildShowStarRateDialog(BuildContext context) {
    _rateMyApp.showStarRateDialog(context, actionsBuilder: (_, count) {
      final Widget cancelButton = FlatButton(
        child: Text(
          S.of(context).cancel,
          style: new TextStyle(
              fontFamily: "SFProText", color: ConstantData.primaryColor),
        ),
        onPressed: () async {
          Navigator.pop(context);

          setState(() {});
        },
      );
      if (count == null || count == 0) {
        // If there is no rating (or a 0 star rating), we only have to return our cancel button.
        return [cancelButton];
      }

      // Otherwise we can do some little more things...
      String message = 'You\'ve put ' + count.round().toString() + ' star(s). ';
      Color color;
      switch (count.round()) {
        case 1:
          message += S.of(context).didThisAppHurtYouPhysically;
          color = Colors.red;
          break;
        case 2:
          message += S.of(context).thatsNotReallyCoolMan;
          color = Colors.orange;
          break;
        case 3:
          message += S.of(context).wellItsAverage;
          color = Colors.blue;
          break;
        case 4:
          message += S.of(context).thisIsCoolLikeThisApp;
          color = Colors.deepOrange;
          break;
        case 5:
          message += S.of(context).great3;
          color = Colors.green;
          break;
      }

      return [
        FlatButton(
          child: Text(
            S.of(context).ok,
            style: new TextStyle(
                fontFamily: "SFProText", color: ConstantData.primaryColor),
          ),
          onPressed: () async {
            print(message);
            print("msgs&$message");

            Toast.show(message, context,
                duration: Toast.LENGTH_SHORT,
                gravity: Toast.BOTTOM,
                backgroundColor: color,
                textColor: Colors.white);

            await _rateMyApp.callEvent(RateMyAppEventType.rateButtonPressed);
            Navigator.pop<RateMyAppDialogButton>(
                context, RateMyAppDialogButton.rate);

            setState(() {});
          },
        ),
        cancelButton,
      ];
    });
  }

  Widget getReminderCell() {
    return Card(
        margin: EdgeInsets.all(10),
        color: ConstantData.cardBackground,
        elevation: 1.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        child: Container(
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.only(top: 5, bottom: 5),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                      margin: EdgeInsets.only(
                          left: SizeConfig.safeBlockVertical * 0.9),
                      // Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: ConstantData.getGradientColors(),
                        ),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.notifications_none,
                          color: Colors.white,
                          size: 25,
                        ),
                      )),
                  Container(
                    margin:
                        EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
                    child: Text(
                      S.of(context).reminder,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          fontFamily: "SFProText",
                          color: ConstantData.textColors,
                          decoration: TextDecoration.none),
                    ),
                  ),
                  new Spacer(),
                  Switch(
                    value: (_isReminder == 1 ? true : false),
                    onChanged: (value) {
                      setState(() {
                        if (value) {
                          PrefData.setReminder(1);
                        } else {
                          PrefData.setReminder(0);
                        }
                        _setReminder();
                      });
                    },
                    activeTrackColor: ConstantData.primaryColor.withAlpha(45),
                    activeColor: ConstantData.primaryColor,
                  )
                ],
              ),
            ],
          ),
        ));
  }

  Widget getColorCell() {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        left: SizeConfig.safeBlockVertical * 0.9),
                    // Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: ConstantData.getGradientColors(),
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.color_lens,
                        color: Colors.white,
                        size: 25,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
                  child: Text(
                    S.of(context).color,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        fontFamily: "SFProText",
                        color: ConstantData.textColors,
                        decoration: TextDecoration.none),
                  ),
                ),
                new Spacer(),
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(
                        right: SizeConfig.safeBlockVertical * 0.9),
                    // Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: ConstantData.primaryColor,
                    ),
                  ),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return MyColorDialog(
                            onSelectedOkChanged: (value) {
                              PrefData.setThemePos(value);
                              ConstantData.setThemePosition();
                              setState(() {});
                            },
                          );
                        });
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getModeCell() {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        left: SizeConfig.safeBlockVertical * 0.9),
                    // Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: ConstantData.getGradientColors(),
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.phone_android,
                        color: Colors.white,
                        size: 25,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
                  child: Text(
                    S.of(context).nightMode,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        fontFamily: "SFProText",
                        color: ConstantData.textColors,
                        decoration: TextDecoration.none),
                  ),
                ),
                new Spacer(),
                Switch(
                  value: (_isNightMode == 1 ? true : false),
                  onChanged: (value) {
                    setState(() {
                      if (value) {
                        PrefData.setDarkModes(1);
                      } else {
                        PrefData.setDarkModes(0);
                      }
                      _setModes();
                    });
                  },
                  activeTrackColor: ConstantData.primaryColor.withAlpha(45),
                  activeColor: ConstantData.primaryColor,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getVibrationCell() {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Row(
          children: [
            Container(
                margin:
                EdgeInsets.only(left: SizeConfig.safeBlockVertical * 0.9),
                // Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.red,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: ConstantData.getGradientColors(),
                  ),
                ),
                child: Center(
                  child: Icon(
                    Icons.vibration,
                    color: Colors.white,
                    size: 25,
                  ),
                )),
            Container(
              margin: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
              child: Text(
                S.of(context).vibration,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "SFProText",
                    // color: Colors.red,
                    color: ConstantData.textColors,
                    decoration: TextDecoration.none),
              ),
            ),
            new Spacer(),
            Switch(
              value: (_isVibration),
              onChanged: (value) {
                setState(() {
                  PrefData.setVibration(value);
                  _setVibration();
                });
              },
              activeTrackColor: ConstantData.primaryColor.withAlpha(45),
              activeColor: ConstantData.primaryColor,
            )
          ],
        ),
      ),
    );
  }
  Widget getSoundCell() {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Row(
          children: [
            Container(
                margin:
                    EdgeInsets.only(left: SizeConfig.safeBlockVertical * 0.9),
                // Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: Colors.red,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: ConstantData.getGradientColors(),
                  ),
                ),
                child: Center(
                  child: Icon(
                    Icons.volume_up_rounded,
                    color: Colors.white,
                    size: 25,
                  ),
                )),
            Container(
              margin: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
              child: Text(
                S.of(context).sound,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                    fontFamily: "SFProText",
                    // color: Colors.red,
                    color: ConstantData.textColors,
                    decoration: TextDecoration.none),
              ),
            ),
            new Spacer(),
            Switch(
              value: (_isSound),
              onChanged: (value) {
                setState(() {
                  PrefData.setSound(value);
                  _setSound();
                });
              },
              activeTrackColor: ConstantData.primaryColor.withAlpha(45),
              activeColor: ConstantData.primaryColor,
            )
          ],
        ),
      ),
    );
  }

  Widget getFontCell() {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        left: SizeConfig.safeBlockVertical * 0.9),
                    // Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: ConstantData.getGradientColors(),
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.text_fields,
                        color: Colors.white,
                        size: 25,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).fontSize,
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            fontFamily: "SFProText",
                            color: ConstantData.textColors,
                            decoration: TextDecoration.none),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          _fontSize.toString() + " " + S.of(context).sp,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors,
                              decoration: TextDecoration.none),
                        ),
                      )
                    ],
                  ),
                ),
                new Spacer(),
                Icon(
                  Icons.navigate_next,
                  color: ConstantData.primaryColor,
                  size: 28,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getSocialCell(var icon, String s) {
    return Card(
      margin: EdgeInsets.all(10),
      color: ConstantData.cardBackground,
      elevation: 1.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: EdgeInsets.only(
                        left: SizeConfig.safeBlockVertical * 0.9),
                    // Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: ConstantData.getGradientColors(),
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        icon,
                        color: Colors.white,
                        size: 25,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(left: SizeConfig.safeBlockVertical * 3),
                  child: Text(
                    s,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        fontFamily: "SFProText",
                        color: ConstantData.textColors,
                        decoration: TextDecoration.none),
                  ),
                ),
                new Spacer(),
                Icon(
                  Icons.navigate_next,
                  color: ConstantData.primaryColor,
                  size: 28,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MyColorDialog extends StatefulWidget {
  MyColorDialog({this.onSelectedOkChanged});

  final ValueChanged<int> onSelectedOkChanged;

  @override
  _MyColorDialog createState() => _MyColorDialog();
}

class _MyColorDialog extends State<MyColorDialog> {
  int _themePosition;

  @override
  void initState() {
    _setTheme();
    ConstantData.setThemePosition();
    setState(() {});
    super.initState();
  }

  Future<void> _setTheme() async {
    _themePosition = await PrefData.getThemePos();

    print("theme---" + _themePosition.toString());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        color: ConstantData.cardBackground,
        height: SizeConfig.safeBlockVertical * 50,
        child: Column(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 2,
                  margin: EdgeInsets.only(top: 15),
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                  ),
                ),
                Center(
                  child: Text(
                    S.of(context).chooseTheColor,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: ConstantData.primaryColor,
                        decoration: TextDecoration.none),
                  ),
                ),
                Container(
                  height: 2,
                  margin: EdgeInsets.only(top: 15, right: 20, left: 20),
                  decoration: BoxDecoration(
                    color: ConstantData.primaryColor,
                  ),
                  child: Container(),
                ),
              ],
            ),
            Expanded(
              child: new GridView.count(
                  crossAxisCount: 3,
                  childAspectRatio: 1,
                  padding: EdgeInsets.all(20),
                  primary: false,
                  shrinkWrap: true,
                  children:
                      List.generate(ThemeColor.themeColorsList.length, (index) {
                    Color color = ThemeColor.themeColorsList[index];
                    return Center(
                        child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(top: 15, left: 10, right: 10),
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: color,
                        ),
                        child: Visibility(
                            visible: (index == _themePosition),
                            child: Icon(Icons.check,
                                color: Colors.white, size: 35)),
                      ),
                      onTap: () {
                        setState(() {
                          PrefData.setThemePos(index);
                          widget.onSelectedOkChanged(index);
                          Navigator.of(context).pop();
                        });
                      },
                    ));
                  })),
            ),
          ],
        ),
      ),
    );
  }
}

// https://pub.dev/packages/flutter_material_pickers
