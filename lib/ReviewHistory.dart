// import 'package:ads/ads.dart';
// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gk_design/util/MyClipper.dart';
import 'package:flutter_gk_design/util/PrefData.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';

import 'db/database_helper.dart';
import 'generated/l10n.dart';
import 'model/HistoryModel.dart';
import 'util/ConstantData.dart';
import 'util/ThemeColor.dart';

class ReviewHistory extends StatefulWidget {
  final int _refId;

// final Ads ad;
  ReviewHistory(this._refId);

  // ReviewHistory(this._refId,this.ad);

  @override
  _ReviewHistory createState() {
    // TODO: implement createState
    return _ReviewHistory(this._refId);
  }
}

class _ReviewHistory extends State<ReviewHistory>
    with TickerProviderStateMixin {
  List<HistoryModel> historyModel = new List();
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  int _refId;

  List<Widget> pages = new List();
  int _totalSize = 0;
  int _selectedPosition = 0;

  _ReviewHistory(this._refId);

  // Ads ads;

  AdmobBannerSize bannerSize;

  @override
  void dispose() {
    // ads.removeBannerAd();
    // ads?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    SizeConfig().init(context);

    return WillPopScope(
      child: Scaffold(
        backgroundColor: ConstantData.primaryColor,
        body: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: ClipPath(
                clipper: MyClipper(),
                child: Container(
                  height: SizeConfig.safeBlockVertical * 55,
                  width: double.infinity,
                  color: ConstantData.backgroundColors,
                ),
              ),
            ),
            Column(
              children: [
                _appBar(),
                Expanded(
                  child: Container(child: _pagerWidget()),
                  flex: 1,
                ),
                Container(
                  height: 55,
                  color: ConstantData.backgroundColors,
                  child: AdmobBanner(
                    adUnitId: ConstantData.getBannerAdUnitId(),
                    adSize: bannerSize,
                    listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                      handleEvent(event, args, 'Banner');
                    },
                    onBannerCreated: (AdmobBannerController controller) {},
                  ),
                )
              ],
            )
          ],
        ),
        // bottomNavigationBar: ,
      ),
      onWillPop: _requestPop,
    );
  }



  void onPageChanged(int index) {
    _selectedPosition = index;
    setState(() {

    });
  }


  Widget _pagerWidget() {
    print("valueData-45--" + historyModel.length.toString());
    if (historyModel.length > 0) {
      return Container(
          child: PageView.builder(
        itemBuilder: (context, position) {
          return Container(
              child: HistoryItem(
            historyModel: historyModel[position],
          ));
        },
        itemCount: historyModel.length,
        onPageChanged: onPageChanged,
      ) // Can be null

          );
    } else {
      return Container(
        color: Colors.white,
      );
    }

    // return  PageView(
    //   children: pages,
    // );
  }

  void _getHistoryList() {
    print("_refId---" + _refId.toString());

    _databaseHelper.getHistoryData(_refId).then((value) {
      if (value != null) {
        setState(() {
          historyModel = value;
          print("valueData---" + historyModel.length.toString());

          for (int i = 0; i < historyModel.length; i++) {
            HistoryModel dataModel = historyModel[i];

            final split = dataModel.optionList
                .replaceAll("[", "")
                .replaceAll("]", "")
                .split(',');
            final Map<int, String> values = {
              for (int i = 0; i < split.length; i++) i: split[i]
            };

            List<String> optionList = List();

            optionList.add(values[0]);
            optionList.add(values[1]);
            optionList.add(values[2]);
            optionList.add(values[3]);

            // List<String> ab = json.decode(dataModel.optionList);
            historyModel[i].allOptionList = optionList;

            pages.add(_itemView(historyModel[i]));
            print("valueData12---" + historyModel.length.toString());

            _totalSize = historyModel.length;
          }

          print("pages---" + pages.length.toString());
        });
      }
    });
  }

  Widget _itemView(HistoryModel historyModel) {
    return Container(
      height: SizeConfig.safeBlockVertical * 93,
      width: double.infinity,
      color: Colors.blue,
      // child: Column(
      //   children: [
      //     Container(
      //       height: SizeConfig.safeBlockVertical * 75,
      //       child: _questionView(historyModel),
      //     ),
      //     Container(
      //       height: SizeConfig.safeBlockVertical * 35,
      //       margin: EdgeInsets.only(
      //           top: SizeConfig.safeBlockVertical * 60,
      //           right: SizeConfig.safeBlockVertical * 0.8,
      //           left: SizeConfig.safeBlockVertical * 0.8,
      //           bottom: SizeConfig.safeBlockVertical * 0.8),
      //       child: _optionView(historyModel),
      //     ),
      //   ],
      // ),
    );
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print("loaded--true");
        setState(() {});
        break;
      case AdmobAdEvent.opened:
        break;
      case AdmobAdEvent.closed:
        break;
      case AdmobAdEvent.failedToLoad:
        break;

      default:
    }
  }

  setThemePosition() async {
    int posGet = await PrefData.getIsDarkMode();
    ThemeData themeData = ThemeColor.themesList[posGet];
    ConstantData.backgroundColors = themeData.backgroundColor;
    ConstantData.cardBackground = themeData.cardColor;
    ConstantData.textColors = themeData.textSelectionColor;
  }

  @override
  void initState() {
    bannerSize = AdmobBannerSize.BANNER;
    setThemePosition();

    super.initState();
    _getHistoryList();
  }

  Future<bool> _requestPop() {
    print("back----true");
    // ads.removeBannerAd();
    // Navigator.of(context).pop();
    Navigator.of(context).pop();
    // Navigator.push(
    //     context, MaterialPageRoute(builder: (context) => MyHomePage(0)));
    return new Future.value(false);
  }

  Widget _appBar() {
    return Container(
        color: ConstantData.primaryColor,
        height: SizeConfig.safeBlockVertical * 7,
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2),
                child: GestureDetector(
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                    size: 20,
                  ),
                  onTap: () {
                    _requestPop();
                  },
                )),
            Container(
              margin: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 2),
              child: Text(
                S.of(context).history,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    fontFamily: "SFProText",
                    decoration: TextDecoration.none,
                    color: Colors.white),
              ),
            ),
            new Spacer(),
            Text(
              (_selectedPosition + 1).toString(),
              style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 22,
                  fontFamily: "SFProText",
                  color: Colors.white),
            ),
            Padding(
              padding: EdgeInsets.only(right: 15),
              child: Text(
                "/$_totalSize",
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    fontFamily: "SFProText",
                    color: Colors.white),
              ),
            )
          ],
        ));
  }
}

class HistoryItem extends StatefulWidget {
  final HistoryModel historyModel;

  HistoryItem({
    this.historyModel,
  });

  @override
  _HistoryItem createState() => _HistoryItem(this.historyModel);
}

class _HistoryItem extends State<HistoryItem> {
  final HistoryModel historyModel;

  _HistoryItem(this.historyModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            height: SizeConfig.safeBlockVertical * 48,
            child: _questionView(),
          ),
          Container(
            margin: EdgeInsets.only(
                top: SizeConfig.safeBlockVertical * 50,
                right: SizeConfig.safeBlockVertical * 0.8,
                left: SizeConfig.safeBlockVertical * 0.8,
                bottom: SizeConfig.safeBlockVertical * 0.8),
            child: _optionView(),
          ),
        ],
      ),
    );
  }

  Color _getViewColor(int i) {
    if (historyModel.allOptionList.length > 0) {
      print("userAnswer---" +
          historyModel.userAnswer +
          "---" +
          historyModel.allOptionList[i] +
          "-----" +
          (historyModel.userAnswer
                  .trim()
                  .contains(historyModel.allOptionList[i].trim()))
              .toString());
      if ((historyModel.answer
              .trim()
              .contains(historyModel.userAnswer.trim())) &&
          (historyModel.answer
              .trim()
              .contains(historyModel.allOptionList[i].trim()))) {
        return Colors.green;
      } else if ((historyModel.answer
          .trim()
          .contains(historyModel.allOptionList[i].trim()))) {
        return Colors.green;
      } else if (historyModel.userAnswer
          .trim()
          .contains(historyModel.allOptionList[i].trim())) {
        return Colors.red;
      } else {
        return ConstantData.cardBackground;
      }
    } else {
      return ConstantData.cardBackground;
    }
  }

  Color _getTextColor(int i) {
    if (historyModel.allOptionList.length > 0) {
      if ((historyModel.answer
              .trim()
              .contains(historyModel.userAnswer.trim())) &&
          (historyModel.answer
              .trim()
              .contains(historyModel.allOptionList[i].trim()))) {
        return Colors.white;
      } else if ((historyModel.answer
          .trim()
          .contains(historyModel.allOptionList[i].trim()))) {
        return Colors.white;
      } else if (historyModel.userAnswer
          .trim()
          .contains(historyModel.allOptionList[i].trim())) {
        return Colors.white;
      } else {
        return ConstantData.textColors;
      }
    } else {
      return ConstantData.textColors;
    }
  }

  Widget _optionView() {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Column(
        children: [
          Expanded(
            child: Visibility(
              child: GestureDetector(
                child: Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 1.2,
                            bottom: SizeConfig.safeBlockVertical * 1.2,
                            left: SizeConfig.safeBlockVertical * 8,
                            right: SizeConfig.safeBlockVertical * 8),
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.circular(8),
                          color: _getViewColor(0),
                        ),
                        child: Center(
                          child: AutoSizeText(
                            (historyModel.allOptionList.length > 0)
                                ? historyModel.allOptionList[0]
                                : "",
                            maxLines: 1,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w500,
                                color: _getTextColor(0)),
                          ),
                        )),
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
          Expanded(
            child: Visibility(
              child: GestureDetector(
                child: Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 1.2,
                            bottom: SizeConfig.safeBlockVertical * 1.2,
                            left: SizeConfig.safeBlockVertical * 8,
                            right: SizeConfig.safeBlockVertical * 8),
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.circular(8),
                          color: _getViewColor(1),
                          // border: Border.all(
                          //     color: _getViewColor(1),
                          //     // color: (listView[1]) ? Colors.green : Colors.white,
                          //     width: 2)
                        ),
                        child: Center(
                          child: AutoSizeText(
                            (historyModel.allOptionList.length > 0)
                                ? historyModel.allOptionList[1]
                                : "",
                            maxLines: 1,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w500,
                                color: _getTextColor(1)),
                          ),
                        )),
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
          Expanded(
            child: Visibility(
                child: GestureDetector(
              child: Stack(
                children: [
                  Container(
                      margin: EdgeInsets.only(
                          top: SizeConfig.safeBlockVertical * 1.2,
                          bottom: SizeConfig.safeBlockVertical * 1.2,
                          left: SizeConfig.safeBlockVertical * 8,
                          right: SizeConfig.safeBlockVertical * 8),
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(8),
                        color: _getViewColor(2),
                        // border: Border.all(
                        //     color: _getViewColor(2),
                        //     // color: (listView[2]) ? Colors.green : Colors.white,
                        //     width: 2)
                      ),
                      child: Center(
                        child: AutoSizeText(
                          (historyModel.allOptionList.length > 0)
                              ? historyModel.allOptionList[2]
                              : "",
                          maxLines: 1,
                          style: TextStyle(
                              fontFamily: "SFProText",
                              fontWeight: FontWeight.w500,
                              color: _getTextColor(2)),
                        ),
                      )),
                ],
              ),
            )),
            flex: 1,
          ),
          Expanded(
            child: Visibility(
              child: GestureDetector(
                child: Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 1.2,
                            bottom: SizeConfig.safeBlockVertical * 1.2,
                            left: SizeConfig.safeBlockVertical * 8,
                            right: SizeConfig.safeBlockVertical * 8),
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.circular(8),
                          color: _getViewColor(3),
                        ),
                        child: Center(
                          child: AutoSizeText(
                            (historyModel.allOptionList.length > 0)
                                ? historyModel.allOptionList[3]
                                : "",
                            maxLines: 1,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w500,
                                color: _getTextColor(3)),
                          ),
                        )),
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  Widget _questionView() {
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.safeBlockVertical * 8),
      height: SizeConfig.safeBlockVertical * 48,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 35, left: 35, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(5),
              color: ConstantData.shadow2,
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 25, left: 25, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(8),
              color: ConstantData.shadow1,
            ),
          ),
          Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 15, right: 15, left: 15),
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.circular(10),
                color: ConstantData.cardBackground,
              ),
              child: Container(
                padding: EdgeInsets.all(8),
                child: Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 6),
                        child: Column(
                          children: [
                            Visibility(
                              visible: (historyModel.image != null),
                              // visible: (questionImage != null),
                              child: Expanded(
                                // child: Image.asset(
                                //   "assets/images/" + questionImage,
                                //   fit: BoxFit.none,
                                // ),
                                child: _setImage(),

                                flex: 1,
                              ),
                            ),
                            Visibility(
                              visible: true,
                              // modelQuizList[_selecetedPosition].question != null,
                              child: Expanded(
                                child: Center(
                                  child: AutoSizeText(
                                    historyModel.question,
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        color: ConstantData.textColors),
                                  ),
                                ),
                                flex: 1,
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget _setImage() {
    if (historyModel.image == null) {
      return Container();
    } else {
      return Image.asset(
        "assets/images/" + historyModel.image,
        fit: BoxFit.none,
      );
    }
  }
}
