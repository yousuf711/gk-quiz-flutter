import 'dart:math';

// import 'package:ads/ads.dart';
// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter_gk_design/model/MainModel.dart';
import 'package:flutter_gk_design/model/SubCategories.dart';
import 'package:flutter_gk_design/util/MyClipper.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';

import 'db/database_helper.dart';
import 'generated/l10n.dart';
import 'model/ModelQuiz.dart';
import 'model/TestModel.dart';

import 'util/ConstantData.dart';

class PracticePage extends StatefulWidget {
  final SubCategories subCatModel;
  final MainModel modelCat;

  // final Ads ad;

  PracticePage(this.subCatModel, this.modelCat);

  // PracticePage(this.subCatModel, this.modelCat,this.ad);

  @override
  _PracticePage createState() => _PracticePage(this.subCatModel, this.modelCat);
}

class _PracticePage extends State<PracticePage>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  final SubCategories subCatModel;
  final MainModel modelCat;
  double topPosition;

  int _selectedPosition = 0;
  int _totalSize = 0;
  Color appBarBackground;
  List<TestModel> listView = new List();

  // FToast fToast;
  AdmobBannerSize bannerSize;
  // Ads ads;
  bool _isClick = true;

  _PracticePage(this.subCatModel, this.modelCat);

  List<ModelQuiz> modelQuizList = List<ModelQuiz>();
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  ModelQuiz modelQuiz;
  String questionImage;

  @override
  void initState() {
    super.initState();
    bannerSize = AdmobBannerSize.BANNER;
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

    // fToast = FToast();
    // fToast.init(context);
    topPosition = -80;
    appBarBackground = Colors.transparent;

    print("isRun---true$_selectedPosition");
    // ads = widget.ad;
    // ads.hideBannerAd();
    // videoAds.sho();
    // ads = Ads(ConstantData.getAppAdUnitId());
    //
    // var eventListener = (MobileAdEvent event) {
    //   if (event == MobileAdEvent.loaded) {
    //     _isBannerLoaded=true;
    //     print("Load---true");
    //   }
    //   if (event == MobileAdEvent.failedToLoad) {
    //     print("failedToLoad---true");
    //   }
    // };
    //
    // ads.setBannerAd(
    //     adUnitId: ConstantData.getBannerAppId(),
    //     size: AdSize.banner,
    //     keywords: ['dart', 'java'],
    //     childDirected: true,
    //     testDevices: null,
    //     listener: eventListener
    // );
    //
    // ads.showBannerAd();
    // ads.showBannerAd(state: this, anchorOffset: null);

    ConstantData.setThemePosition();
    _getQuizList();
    WidgetsBinding.instance.addObserver(this);
  }



//
  String getCString(String myString) {
//   myString = myString.replace("[", "");
//   myString = myString.replace("]", "");
    return myString.substring(0, 1).toUpperCase() + myString.substring(1);
  }

  List shuffle(List items) {
    var random = new Random();
    for (var i = items.length - 1; i > 0; i--) {
      // Pick a pseudorandom number according to the list length
      var n = random.nextInt(i + 1);

      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }

    return items;
  }

  void _getQuizList() {
    // modelQuizList =  List();

    _databaseHelper.getAllQuiz(subCatModel.id).then((value) {
      print("value258-----" + _selectedPosition.toString());
      // print("value258----$value----"+_selectedPosition.toString());

      if (value != null) {
        // setState(() {
        print("modelQuizListlength----" + value.length.toString());

        modelQuizList = value;

        _totalSize = modelQuizList.length;

        //
        for (int i = 0; i < modelQuizList.length; i++) {
          ModelQuiz dataModel = modelQuizList[i];

          List<String> optionList = List();

          optionList.add(getCString(dataModel.option_1));
          optionList.add(getCString(dataModel.option_2));
          optionList.add(getCString(dataModel.option_3));
          optionList.add(getCString(dataModel.option_4));

          shuffle(optionList);

          modelQuizList[i].allOptionList = optionList;

          if (i == 0) {
            modelQuizList[i].image = "world_cup.png";
          }
          if (modelQuizList[i].image == null ||
              modelQuizList[i].image.isEmpty) {
            modelQuizList[i].isVisibleImage = false;
          } else {
            modelQuizList[i].isVisibleImage = true;
          }
        }
        // });

        setState(() {
          if (modelQuizList.length > 0) {
            modelQuiz = modelQuizList[_selectedPosition];
            questionImage = modelQuiz.image;

            listView.add(new TestModel(false));
            listView.add(new TestModel(false));
            listView.add(new TestModel(false));
            listView.add(new TestModel(false));
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return WillPopScope(
        child: Scaffold(
          backgroundColor: ConstantData.backgroundColors,
          body: Column(

            children: [
              Expanded(child: Container(
                color: ConstantData.primaryColor,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ClipPath(
                        clipper: MyClipper(),
                        child: Container(
                          height: SizeConfig.safeBlockVertical * 52,
                          width: double.infinity,
                          color: ConstantData.backgroundColors,
                        ),
                      ),
                    ),
                    Stack(
                      children: [
                        Container(

                            height: SizeConfig.safeBlockVertical * 52,
                            width: double.infinity,
                            child: Container(
                              child: Stack(
                                children: [
                                  _appBar(),
                                  _questionView(),
                                ],
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 57,
                            right: SizeConfig.safeBlockVertical * 0.8,
                            left: SizeConfig.safeBlockVertical * 0.8,
                          ),



                          child:Column(
                            children: [
                              Expanded(child: _optionView(),flex: 1,),
                              Container(
                                height: 55,
                                child:AdmobBanner(
                                  adUnitId: ConstantData.getBannerAdUnitId(),
                                  adSize: bannerSize,
                                  listener: (AdmobAdEvent event,
                                      Map<String, dynamic> args) {
                                    handleEvent(event, args, 'Banner');
                                  },
                                  onBannerCreated:
                                      (AdmobBannerController controller) {


                                  },
                                ),
                              )
                            ],
                          )
                          // child: _optionView(),
                        ),
                      ],
                    )
                  ],
                ),
              ),flex: 1,),



            ],
          ),

        ),
        onWillPop: _requestPop);
  }


  Widget getOptionCell(int position){
    return Visibility(
      visible: listView[position].viewVisibility,
      child: BouncingWidget(

        child: Stack(
          children: [
            Container(
                margin: EdgeInsets.only(
                    top: SizeConfig.safeBlockVertical * 1.2,
                    bottom: SizeConfig.safeBlockVertical * 1.2,
                    left: SizeConfig.safeBlockVertical * 8,
                    right: SizeConfig.safeBlockVertical * 8),
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8),
                  color: _getViewColor(position),

                ),
                child: Center(
                  child: AutoSizeText(
                    (modelQuizList.length > 0)
                        ? modelQuiz.allOptionList[position]
                        : "",
                    maxLines: 1,
                    style: TextStyle(
                        fontFamily: "SFProText",
                        fontWeight: FontWeight.w500,
                        color: _getTextColor(position)),
                  ),
                )),
          ],
        ),

        duration: Duration(milliseconds: 100),
        scaleFactor: 1.5,
        onPressed: () {
          _checkAnswer(modelQuiz.allOptionList[position], position);
        },


      ),
    );
  }

  Widget _optionView() {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Column(
        children: [
          Expanded(

            child: getOptionCell(0),
            // child: Visibility(
            //   visible: listView[0].viewVisibility,
            //   child: GestureDetector(
            //     child: Stack(
            //       children: [
            //         Container(
            //             margin: EdgeInsets.only(
            //                 top: SizeConfig.safeBlockVertical * 1.2,
            //                 bottom: SizeConfig.safeBlockVertical * 1.2,
            //                 left: SizeConfig.safeBlockVertical * 8,
            //                 right: SizeConfig.safeBlockVertical * 8),
            //             decoration: BoxDecoration(
            //               borderRadius: new BorderRadius.circular(8),
            //               color: _getViewColor(0),
            //               // border: Border.all(
            //               //     color: _getViewColor(0),
            //               //     // color: (listView[0].is) ? Colors.green : Colors.white,
            //               //     width: 2)
            //             ),
            //             child: Center(
            //               child: AutoSizeText(
            //                 (modelQuizList.length > 0)
            //                     ? modelQuiz.allOptionList[0]
            //                     : "",
            //                 maxLines: 1,
            //                 style: TextStyle(
            //                     fontFamily: "SFProText",
            //                     fontWeight: FontWeight.w500,
            //                     color: _getTextColor(0)),
            //               ),
            //             )),
            //       ],
            //     ),
            //     onTap: () {
            //       _checkAnswer(modelQuiz.allOptionList[0], 0);
            //     },
            //   ),
            // ),
            flex: 1,
          ),
          Expanded(
            child: getOptionCell(1),
            // child:
            // Visibility(
            //   visible: listView[1].viewVisibility,
            //   child: GestureDetector(
            //     child: Stack(
            //       children: [
            //         Container(
            //             margin: EdgeInsets.only(
            //                 top: SizeConfig.safeBlockVertical * 1.2,
            //                 bottom: SizeConfig.safeBlockVertical * 1.2,
            //                 left: SizeConfig.safeBlockVertical * 8,
            //                 right: SizeConfig.safeBlockVertical * 8),
            //             decoration: BoxDecoration(
            //               borderRadius: new BorderRadius.circular(8),
            //               color: _getViewColor(1),
            //               // border: Border.all(
            //               //     color: _getViewColor(1),
            //               //     // color: (listView[1]) ? Colors.green : Colors.white,
            //               //     width: 2)
            //             ),
            //             child: Center(
            //               child: AutoSizeText(
            //                 (modelQuizList.length > 0)
            //                     ? modelQuiz.allOptionList[1]
            //                     : "",
            //                 maxLines: 1,
            //                 style: TextStyle(
            //                     fontFamily: "SFProText",
            //                     fontWeight: FontWeight.w500,
            //                     color: _getTextColor(1)),
            //               ),
            //             )),
            //       ],
            //     ),
            //     onTap: () {
            //       _checkAnswer(modelQuiz.allOptionList[1], 1);
            //     },
            //   ),
            // ),
            flex: 1,
          ),
          Expanded(
            // child: Visibility(
            //     visible: listView[2].viewVisibility,
            //     child: GestureDetector(
            //       child: Stack(
            //         children: [
            //           Container(
            //               margin: EdgeInsets.only(
            //                   top: SizeConfig.safeBlockVertical * 1.2,
            //                   bottom: SizeConfig.safeBlockVertical * 1.2,
            //                   left: SizeConfig.safeBlockVertical * 8,
            //                   right: SizeConfig.safeBlockVertical * 8),
            //               decoration: BoxDecoration(
            //                 borderRadius: new BorderRadius.circular(8),
            //                 color: _getViewColor(2),
            //                 // border: Border.all(
            //                 //     color: _getViewColor(2),
            //                 //     // color: (listView[2]) ? Colors.green : Colors.white,
            //                 //     width: 2)
            //               ),
            //               child: Center(
            //                 child: AutoSizeText(
            //                   (modelQuizList.length > 0)
            //                       ? modelQuiz.allOptionList[2]
            //                       : "",
            //                   maxLines: 1,
            //                   style: TextStyle(
            //                       fontFamily: "SFProText",
            //                       fontWeight: FontWeight.w500,
            //                       color: _getTextColor(2)),
            //                 ),
            //               )),
            //         ],
            //       ),
            //       onTap: () {
            //         _checkAnswer(modelQuiz.allOptionList[2], 2);
            //       },
            //     )),
            child: getOptionCell(2),
            flex: 1,
          ),
          Expanded(
            child: getOptionCell(3),
            // child: Visibility(
            //   visible: listView[3].viewVisibility,
            //   child: GestureDetector(
            //     child: Stack(
            //       children: [
            //         Container(
            //             margin: EdgeInsets.only(
            //                 top: SizeConfig.safeBlockVertical * 1.2,
            //                 bottom: SizeConfig.safeBlockVertical * 1.2,
            //                 left: SizeConfig.safeBlockVertical * 8,
            //                 right: SizeConfig.safeBlockVertical * 8),
            //             decoration: BoxDecoration(
            //               borderRadius: new BorderRadius.circular(8),
            //               color: _getViewColor(3),
            //               // border: Border.all(
            //               //     color: _getViewColor(3),
            //               //     // color: (listView[3]) ? Colors.green : Colors.white,
            //               //     width: 2)
            //             ),
            //             child: Center(
            //               child: AutoSizeText(
            //                 (modelQuizList.length > 0)
            //                     ? modelQuiz.allOptionList[3]
            //                     : "",
            //                 maxLines: 1,
            //                 style: TextStyle(
            //                     fontFamily: "SFProText",
            //                     fontWeight: FontWeight.w500,
            //                     color: _getTextColor(3)),
            //               ),
            //             )),
            //
            //         // Align(
            //         //   alignment: Alignment.centerRight,
            //         //   child:
            //
            //         // ),
            //       ],
            //     ),
            //     onTap: () {
            //       _checkAnswer(modelQuiz.allOptionList[3], 3);
            //     },
            //   ),
            // ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  Color _getViewColor(int i) {
    if (listView.length > 0) {
      if (listView[i].visibility) {
        if (listView[i].isCheckAnswer) {
          return Colors.green;
        } else {
          return Colors.red;
        }
      } else {
        return ConstantData.cardBackground;
      }
    } else {
      return ConstantData.cardBackground;
    }
  }

  Color _getTextColor(int i) {
    if (listView.length > 0) {
      if (listView[i].visibility) {
        if (listView[i].isCheckAnswer) {
          return Colors.white;
        } else {
          return Colors.white;
        }
      } else {
        return ConstantData.textColors;
      }
    } else {
      return ConstantData.textColors;
    }
  }

  void _checkAnswer(String s, int pos) {
    if (_isClick) {
      setState(() {
        listView[pos].visibility = true;

        if (s.trim() == (modelQuiz.answer.trim()) ||
            s.trim().toLowerCase() == (modelQuiz.answer.trim().toLowerCase())) {
          _nextQuiz();
          listView[pos].isCheckAnswer = true;
          _isClick = false;
        } else {
          listView[pos].isCheckAnswer = false;
        }
      });
    }
  }

  void _nextQuiz() {
    new Future.delayed(new Duration(seconds: ConstantData.delayTime), () {
      setState(() {
        _isClick = true;
        listView = null;
        listView = new List();

        listView.add(new TestModel(false));
        listView.add(new TestModel(false));
        listView.add(new TestModel(false));
        listView.add(new TestModel(false));

        if (_selectedPosition < modelQuizList.length - 1) {
          _selectedPosition++;

          modelQuiz = modelQuizList[_selectedPosition];
          questionImage = modelQuiz.image;
        } else {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Wrap(
                    children: [
                      Container(
                        width: double.infinity,
                        padding:
                            EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: ConstantData.getGradientColors(),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              S.of(context).practiceTitle,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  fontFamily: "SFProText",
                                  color: Colors.white),
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                  S.of(context).practiceDone,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 15,
                                      fontFamily: "SFProText",
                                      color: Colors.white),
                                )),
                            Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: InkWell(
                                      child: Text(
                                        S.of(context).ok,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      ),
                                      onTap: () {
                                        // ads.removeBannerAd();
                                        Navigator.of(context).pop(true);
                                      }),
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
        }
      });
    });
  }

  Widget _questionView() {
    return Container(
      margin: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 10),
      height: SizeConfig.safeBlockVertical * 48,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 35, left: 35, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(5),
              color: ConstantData.shadow2,
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 25, left: 25, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(8),
              color: ConstantData.shadow1,
            ),
          ),
          Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 15, right: 15, left: 15),
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.circular(10),
                color: ConstantData.cardBackground,
              ),
              child: Container(
                padding: EdgeInsets.all(8),
                child: Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(
                            top: SizeConfig.safeBlockVertical * 6),
                        child: Column(
                          children: [
                            Visibility(
                              visible: (questionImage != null),
                              // visible: (questionImage != null),
                              child: Expanded(
                                // child: Image.asset(
                                //   "assets/images/" + questionImage,
                                //   fit: BoxFit.none,
                                // ),
                                child: _setImage(),

                                flex: 1,
                              ),
                            ),
                            Visibility(
                              visible: true,
                              // modelQuizList[_selectedPosition].question != null,
                              child: Expanded(
                                child: Center(
                                  child: AutoSizeText(
                                    (modelQuizList.length > 0)
                                        ? modelQuiz.question
                                        : "Question",
                                    maxLines: 3,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        color: ConstantData.textColors),
                                  ),
                                ),
                                flex: 1,
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget _setImage() {
    if (questionImage == null) {
      return Container();
    } else {
      return Image.asset(
        "assets/images/" + questionImage,
        fit: BoxFit.none,
      );
    }
  }

  Widget _appBar() {
    return Container(
        height: SizeConfig.safeBlockVertical * 8,
        margin: EdgeInsets.only(right: 15),
        child: Row(
          children: [
            Container(
              child: new IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                  size: 20,
                ),
                onPressed: () {
                  _requestPop();
                },
              ),
            ),
            Text(
              subCatModel.title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: "SFProText",
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  color: Colors.white),
            ),
            new Spacer(),
            Text(
              (_selectedPosition + 1).toString(),
              style: TextStyle(
                  fontFamily: "SFProText",
                  fontWeight: FontWeight.w800,
                  fontSize: 22,
                  color: Colors.white),
            ),
            Text(
              "/$_totalSize",
              style: TextStyle(
                  fontFamily: "SFProText",
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: Colors.white),
            ),
          ],
          // ),
          // ],
        ));
  }

  // Ads ads;

  @override
  void dispose() {
    // ads.removeBannerAd();
    // ads?.dispose();
    super.dispose();
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        print("loaded--true");
        setState(() {});
        break;
      case AdmobAdEvent.opened:
        break;
      case AdmobAdEvent.closed:
        break;
      case AdmobAdEvent.failedToLoad:
        break;

      default:
    }
  }


  Future<bool> _requestPop() {
    // ads.removeBannerAd();
    print("back----true");

    Navigator.of(context).pop();
    return new Future.value(true);
  }
}
