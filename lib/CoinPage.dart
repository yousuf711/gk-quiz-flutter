import 'dart:async';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';

import 'generated/l10n.dart';
import 'model/CoinModel.dart';
import 'util/ConstantData.dart';

import 'util/PrefData.dart';
import 'util/SizeConfig.dart';
import 'util/winwheel.dart';

class CoinPage extends StatefulWidget {
  // final Ads ad;

  // CoinPage(this.ad);

  _CoinPageState createState() => _CoinPageState();
}

class _CoinPageState extends State<CoinPage> with WidgetsBindingObserver {
  static WinwheelController ctrl;
  bool isPlaying = false;
  String btnString = "ghgh";
  bool _isViewVisible = true;
  bool _isDispose = false;

  String checkTimeString = "hgg";
  String _timeString = "ghgh";
  Timer timer;
  bool _isRewarded = false;
  bool _isSound = true;

  int videoCount = 0;
  List<CoinModel> coinModel = new List();

  Future<void> _setViews() async {
    _isSound = await PrefData.getSound();
    _isViewVisible = await PrefData.getCheckOut();

    setState(() {});
    print("fgfgfg0----" + _isViewVisible.toString());
    String s1 = await PrefData.getCheckOutTime();
    _timeString = await ConstantData.getPrintTime(s1);

    setState(() {
      print("fgfgfg012----" + _timeString.toString());
    });

    if (!_isViewVisible) {
      timer = Timer.periodic(Duration(milliseconds: 100), (_) {
        // Runs after every 1000ms
        if (_isDispose) {
          timer.cancel();
        } else {
          setState(() {
            if (!_isViewVisible) {
              checkTimeString = S.of(context).checkAfter +
                  S.of(context).space +
                  _timeString +
                  S.of(context).space +
                  S.of(context).time;
            } else {
              _cancelTimer();
              _setCheckViews();
            }
          });
        }
        setState(() {
          _setString();
          print("_timeString----" + _timeString.toString());
        });

        print("_isViewVisible----" + _isViewVisible.toString());
      });
    }
  }

  Future<void> _setString() async {
    _isViewVisible = await PrefData.getCheckOut();
    String s1 = await PrefData.getCheckOutTime();
    _timeString = await ConstantData.getPrintTime(s1);
  }

  void _cancelTimer() {
    if (timer != null) {
      timer.cancel();
    }
    advancedPlayer.dispose();
  }

  AdmobReward rewardAd;

  AudioPlayer advancedPlayer;

  // final assetsAudioPlayer = AssetsAudioPlayer();
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    ConstantData.setThemePosition();

    rewardAd = AdmobReward(
      adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        print("event12----1--true" + event.toString());
        handleEvent(event, args, 'Reward');
      },
    );

    rewardAd.load();

    _setCoinList();

    print("rtrt--true1");

    WidgetsBinding.instance.addObserver(this);

    _setCheckViews();
    print("rtrt--true2");
  }

  Future<void> _setCoinList() async {
    String s = await PrefData.getCoinsDateList();

    if (s != null) {
      coinModel = CoinModel.decode(s);
    }

    print("coinModel---1" + coinModel.length.toString());
    setState(() {
      final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('dd-mm-yyyy');
      final String currentDay = formatter.format(now);


      if (coinModel.length > 0) {
        bool isAdd = true;

        for (int i = 0; i < coinModel.length; i++) {
          if (coinModel[i].date.contains(currentDay)) {
            videoCount = coinModel[i].videoCount;
            isAdd = false;
            break;
          }
        }
        if (isAdd) {
          videoCount = 3;
          coinModel.add(new CoinModel(date: currentDay, videoCount: 3));
        }
      } else {
        videoCount = 3;
        coinModel.add(new CoinModel(date: currentDay, videoCount: 3));
      }

      String s = CoinModel.encode(coinModel);
      PrefData.setCoinsDateList(s);
      print("coinModel---" + coinModel.length.toString());
    });
  }

  Future<void> addCoinList() async {
    String s = await PrefData.getCoinsDateList();
    List<CoinModel> coinModel = CoinModel.decode(s);

    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd-mm-yyyy');
    final String currentDay = formatter.format(now);

    if (coinModel.length > 0) {
      for (int i = 0; i < coinModel.length; i++) {
        if (coinModel[i].date.contains(currentDay)) {
          coinModel[i].videoCount = videoCount;
          break;
        }
      }
      String s = CoinModel.encode(coinModel);
      PrefData.setCoinsDateList(s);
    }

    setState(() {});
    _setCoinList();
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.closed:
        print("videoCLose---true");
        rewardAd.load();

        if (_isRewarded) {
          _timeOutDialog(ConstantData.addVideoCoin);
          videoCount--;
          addCoinList();
        }

        break;
      case AdmobAdEvent.rewarded:
        _isRewarded = true;

        break;
      default:
    }
  }

  _setCheckViews() {
    setState(() {
      _setViews();
    });
  }

  Future<void> _addCoin(int coin) async {
    int coins = await PrefData.getCoins();
    PrefData.setCoins((coins + (coin * 10)));
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _cancelTimer();
    } else if (state == AppLifecycleState.resumed) {
      _setCheckViews();
    }
  }

  @override
  void dispose() {
    // ads?.dispose();
    if (!_isViewVisible) {
      timer.cancel();
      timer = null;
    }
    _cancelTimer();

    setState(() {
      _isDispose = true;
    });

    // TODO: implement dispose
    rewardAd.dispose();
    super.dispose();
    print("dispose----true");

    if (isPlaying) {
      ctrl.pause();
    }
  }

  Future<void> _isLoaded() async {
    // _isVideoLoaded = true;
    // _isVideoLoaded = await rewardAd.isLoaded;

    setState(() {});
  }

  void stopAudio() async {
    // assetsAudioPlayer.stop();
    if (_isSound) {
      await advancedPlayer.stop();
      // assetsAudioPlayer.pause();
      // AudioManager.instance.toPause();
      //   audioPlugin.stop();
    }
  }

  Future<ByteData> loadAsset() async {
    return await rootBundle.load('sounds/tack_voice.mp3');
  }

  void startAudio() async {
    // assetsAudioPlayer.play();
    if (_isSound) {
      // audioPlugin.play('tack_voice.mp3',isLocal: true);
      // audioPlugin.play('asset/tack_voice.mp3',isLocal: true);

      advancedPlayer = await AudioCache().loop('tack_voice.mp3');

      // audioPlugin.play('asset/tack_voice.mp3');
      // assetsAudioPlayer.play();
      // AudioManager.instance.play();

      // audioCache.play('tack_voice.mp3');
      // audioCache.play('tack_voice.mp3');
    }
  }

  _showToast(String s) {
    Toast.show(s, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  @override
  Widget build(BuildContext context) {
    btnString = S.of(context).start;
    _timeString = S.of(context).time;

    if (_isViewVisible) {
      checkTimeString = S.of(context).time;
    }

    SizeConfig().init(context);
    ConstantData.setThemePosition();
    _setCheckViews();
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: ConstantData.backgroundColors,
            centerTitle: true,
            title: Text(S.of(context).coins,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    fontFamily: "SFProText",
                    color: ConstantData.textColors)),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: Icon(
                    Icons.keyboard_backspace_outlined,
                    color: ConstantData.textColors,
                  ),
                  onPressed: _requestPop,
                );
              },
            ),
          ),
          backgroundColor: ConstantData.backgroundColors,
          body: Container(
              margin: EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  Card(
                    margin: EdgeInsets.all(10),
                    color: ConstantData.cardBackground,
                    elevation: 1.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Row(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(left: 10, top: 10, bottom: 10),
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: ConstantData.getGradientColors(),
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          child: Icon(
                            Icons.play_arrow,
                            color: Colors.white,
                            size: 30,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                              left: SizeConfig.safeBlockVertical * 1),
                          child: Text(
                            S.of(context).watchVideo,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                fontFamily: "SFProText",
                                color: ConstantData.textColors,
                                decoration: TextDecoration.none),
                          ),
                        ),
                        new Spacer(),
                        InkWell(
                          child: Container(
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.all(10),
                              height: 40,
                              // width: 80,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: ConstantData.getGradientColors(),
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(7))),
                              child: Center(
                                child: Text(
                                  S.of(context).watch +
                                      " " +
                                      " ( " +
                                      videoCount.toString() +
                                      " )",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                      fontFamily: "SFProText",
                                      color: Colors.white,
                                      decoration: TextDecoration.none),
                                ),
                              )),
                          onTap: () {
                            if (videoCount > 0) {
                              _isLoaded();
                              showVideo();
                            }
                          },
                        ),
                      ],
                    ),
                  ),

                  Container(
                      width: double.infinity,
                      child: Card(
                        margin: EdgeInsets.all(10),
                        color: ConstantData.cardBackground,
                        elevation: 1.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Visibility(
                              child: Container(
                                  decoration: BoxDecoration(
                                    color:
                                        ConstantData.primaryColor.withAlpha(30),
                                  ),
                                  margin: EdgeInsets.only(
                                      left: 10,
                                      right: 10,
                                      bottom: 20,
                                      top: SizeConfig.safeBlockVertical * 22),
                                  child: Winwheel(
                                    handleCallback: ((handler) {
                                      ctrl = handler;
                                      return;
                                    }),
                                    controller: ctrl,
                                    // numSegments: 8,
                                    outerRadius: 120,
                                    innerRadius: 15,
                                    strokeStyle: Colors.white,
                                    textFontSize: 16.0,
                                    textFillStyle: Colors.red,
                                    textFontWeight: FontWeight.bold,
                                    textAlignment: WinwheelTextAlignment.center,
                                    textOrientation:
                                        WinwheelTextOrientation.horizontal,
                                    wheelImage: 'assets/planes.png',
                                    drawMode: WinwheelDrawMode.code,
                                    drawText: true,
                                    imageOverlay: false,
                                    lineWidth: 100,
                                    textMargin: 10,
                                    pointerAngle: 0,
                                    pointerGuide: PointerGuide(
                                        display: true,
                                        lineWidth: 1,
                                        strokeStyle: Colors.red),
                                    segments: <Segment>[
                                      Segment(
                                        fillStyle: ConstantData.color1,
                                        textFillStyle: Colors.black,
                                        text: '10',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color2,
                                        textFillStyle: Colors.black,
                                        text: '20',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color3,
                                        textFillStyle: Colors.black,
                                        text: '30',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color4,
                                        textFillStyle: Colors.black,
                                        text: '40',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color5,
                                        textFillStyle: Colors.black,
                                        text: '50',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color6,
                                        textFillStyle: Colors.black,
                                        text: '60',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color7,
                                        textFillStyle: Colors.black,
                                        text: '70',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color8,
                                        textFillStyle: Colors.black,
                                        text: '80',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color9,
                                        textFillStyle: Colors.black,
                                        text: '90',
                                        strokeStyle: Colors.transparent,
                                      ),
                                      Segment(
                                        fillStyle: ConstantData.color10,
                                        textFillStyle: Colors.black,
                                        text: '100',
                                        strokeStyle: Colors.transparent,
                                      ),
                                    ],
                                    pins: Pin(
                                      // visible: true,
                                      number: 16,
                                      margin: 6,
                                      // outerRadius: 5,
                                      fillStyle: Colors.transparent,
                                    ),
                                    animation: WinwheelAnimation(
                                      type: WinwheelAnimationType.spinToStop,
                                      spins: 3,
                                      duration: const Duration(
                                        seconds: 2,
                                      ),
                                      callbackFinished: (int segment) {
                                        setState(() {
                                          stopAudio();

                                          var today = new DateTime.now();
                                          var fiftyDaysFromNow = today
                                              .add(new Duration(hours: 50));
                                          today.add(Duration(hours: 2));
                                          final DateFormat formatter =
                                              DateFormat('hh:mm:ss');
                                          PrefData.setRewardTime(formatter
                                              .format(fiftyDaysFromNow));

                                          _addCoin(segment);
                                          btnString = S.of(context).done;
                                          _setCheckViews();
                                          PrefData.setCheckOut(false);
                                          int coin = 10 * segment;
                                          _timeOutDialog(coin);
                                          isPlaying = false;

                                          setState(() {});
                                        });

                                        print('animation finished');
                                        print(
                                            "int=======" + segment.toString());
                                      },
                                      callbackBefore: () {
                                        setState(() {
                                          isPlaying = true;
                                        });
                                      },
                                    ),
                                  )),
                              visible: _isViewVisible,
                            ),
                            Visibility(
                              child: InkWell(
                                child: Container(
                                    margin: EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                        bottom: 10,
                                        top: SizeConfig.safeBlockVertical * 20),

                                    // margin: EdgeInsets.all(10),
                                    height: 40,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          colors:
                                              ConstantData.getGradientColors(),
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(7))),
                                    child: Center(
                                      child: Text(
                                        btnString,
                                        style: TextStyle(
                                            fontFamily: "SFProText",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            color: Colors.white,
                                            decoration: TextDecoration.none),
                                      ),
                                    )),
                                onTap: () {
                                  if (!isPlaying) {
                                    ctrl.play();

                                    setState(() {
                                      btnString = S.of(context).loading;
                                      startAudio();
                                    });
                                  }
                                },
                              ),
                              visible: _isViewVisible,
                            )
                          ],
                        ),
                      )),

                  //       ],
                  //     ),
                  //   ),
                  // ),

                  Visibility(
                    visible: _isViewVisible ? false : true,
                    child: Container(
                      height: SizeConfig.safeBlockVertical * 25,
                      width: double.infinity,
                      child: Card(
                        margin: EdgeInsets.all(10),
                        color: ConstantData.cardBackground,
                        elevation: 1.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: Center(
                          child: Text(
                            checkTimeString,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                fontFamily: "SFProText",
                                color: ConstantData.textColors,
                                decoration: TextDecoration.none),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: ListView(
                        children: <Widget>[
                          // IconButton(
                          //   iconSize: 62,
                          //   color: Theme.of(context).primaryColor,
                          //   icon: Icon(
                          //     isPlaying
                          //         ? Icons.pause_circle_outline
                          //         : Icons.play_circle_outline,
                          //   ),
                          //   onPressed: () {
                          //     if (isPlaying) {
                          //       ctrl.pause();
                          //
                          //       setState(() {
                          //         isPlaying = false;
                          //       });
                          //     } else {
                          //       ctrl.play();
                          //
                          //       setState(() {
                          //         isPlaying = true;
                          //       });
                          //     }
                          //   },
                          // ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              // FlatButton(
                              //   onPressed: () {
                              //     print('add');
                              //     List colors = [
                              //       Colors.pink,
                              //       Colors.purple,
                              //       Colors.amber,
                              //       Colors.red,
                              //       Colors.blue,
                              //       Colors.cyan,
                              //       Colors.deepPurple,
                              //       Colors.indigo,
                              //       Colors.lightBlue
                              //     ];
                              //     math.Random random = new math.Random();
                              //     ctrl.addSegment(
                              //       Segment(
                              //         fillStyle: colors[random.nextInt(colors.length)],
                              //       ),
                              //     );
                              //   },
                              //   child: Row(
                              //     children: [
                              //       Icon(Icons.add),
                              //       SizedBox(
                              //         width: 4,
                              //       ),
                              //       Text('add segment'),
                              //     ],
                              //   ),
                              // ),
                              // FlatButton(
                              //   onPressed: () {
                              //     ctrl.deleteSegment();
                              //   },
                              //   child: Row(
                              //     children: [
                              //       Icon(
                              //         Icons.delete,
                              //         size: 20,
                              //       ),
                              //       SizedBox(
                              //         width: 4,
                              //       ),
                              //       Text('delete segment'),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
        onWillPop: _requestPop);
  }

  Future<bool> _requestPop() {
    print("back----true");
    Navigator.of(context).pop();
    return new Future.value(true);
  }

  void showVideo() async {
    // if (_isVideoShow) {

    _isRewarded = false;
    if (await rewardAd.isLoaded) {
      rewardAd.show();
    } else {
      _showToast(S.of(context).strVideoError);
    }
  }

  // }

  void _timeOutDialog(int coin) {
    AlertDialog _createMaterialAlertDialog() => new AlertDialog(
          backgroundColor: ConstantData.cardBackground,
          title: new Text(
            S.of(context).congratulations,
            style: TextStyle(
              color: ConstantData.textColors,
              fontFamily: "SFProText",
            ),
          ),
          content: new Text(
            S.of(context).youHaveGot +
                S.of(context).space +
                coin.toString() +
                S.of(context).space +
                S.of(context).coins,
            style: TextStyle(
              color: ConstantData.textColors,
              fontFamily: "SFProText",
            ),
          ),
          actions: <Widget>[
            new MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: new Text(
                  S.of(context).ok,
                  style: TextStyle(
                    color: ConstantData.textColors,
                    fontFamily: "SFProText",
                  ),
                )),
          ],
        );

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return _createMaterialAlertDialog();
        });
  }
}
