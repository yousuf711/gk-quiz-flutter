// import 'package:ads/ads.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gk_design/util/ConstantData.dart';

import '../CoinPage.dart';

class MyBlinkingButton extends StatefulWidget {
  // final Ads ad;
  //
  // MyBlinkingButton(this.ad);


  @override
  _MyBlinkingButtonState createState() => _MyBlinkingButtonState();
}

class _MyBlinkingButtonState extends State<MyBlinkingButton>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    _animationController =
    new AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat(reverse: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ConstantData.setThemePosition();
    return FadeTransition(
      opacity: _animationController,
      child: MaterialButton(
        onPressed: () => null,
        child:
            InkWell(
              child:  Image.asset(
                "assets/images/coins.png",
                height: 28,
                width: 25,
                color: ConstantData.primaryColor,
              ),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CoinPage(),
                      // builder: (context) => CoinPage(widget.ad),

                    ));
              },
            )



        ,
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}