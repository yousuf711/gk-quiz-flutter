import 'package:flutter/material.dart';
import 'package:flutter_gk_design/util/PrefData.dart';
import 'package:flutter_gk_design/util/ThemeColor.dart';

enum MyThemeKeys { LIGHT, DARK, DARKER }

class MyThemes {
  static  ThemeData lightTheme = ThemeData();

  static  ThemeData darkTheme = ThemeData( );




  static Future<ThemeData> setLightTheme() async {

    int pos = await PrefData.getThemePos();

    ThemeData themeData = ThemeData(
        primaryColor:ThemeColor.themeColorsList[pos],
      primaryColorDark: ThemeColor.themeColorsDarkList[pos],
      backgroundColor: ThemeColor.themeBgColorsList[pos],
    );

    return themeData;
  }

  static Future<ThemeData> setDarkTheme() async {

    int pos = await PrefData.getThemePos();


    print("pos111----"+pos.toString());


    ThemeData themeData = ThemeData(
      primaryColor:ThemeColor.themeColorsList[pos],
      primaryColorDark: ThemeColor.themeColorsDarkList[pos],
      backgroundColor:  "#0D0D0D".toColor(),
    );

    print("pos1113----"+ThemeColor.themeColorsList[pos].toString());

    return themeData;
  }
  // static ThemeData getThemeFromKey(MyThemeKeys themeKey) {
  //   setLightTheme().then((path){
  //     lightTheme = path;
  //     switch (themeKey) {
  //       case MyThemeKeys.LIGHT:
  //         return lightTheme;
  //       case MyThemeKeys.DARK:
  //         return darkTheme;
  //       case MyThemeKeys.DARKER:
  //         return darkerTheme;
  //       default:
  //         return lightTheme;
  //     }
  //   });
  //
  //   setDarkTheme().then((path){
  //     darkTheme = path;
  //     switch (themeKey) {
  //       case MyThemeKeys.LIGHT:
  //         return lightTheme;
  //       case MyThemeKeys.DARK:
  //         return darkTheme;
  //       case MyThemeKeys.DARKER:
  //         return darkerTheme;
  //       default:
  //         return lightTheme;
  //     }
  //
  //   });
  //
  //   print("MyThemeKeys----"+MyThemeKeys.DARK.toString()+"===="+themeKey.toString());
  //
  //   print("pos11134----"+darkTheme.primaryColor.toString());
  //
  //
  //
  // }
}
