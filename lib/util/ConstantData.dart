import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_gk_design/generated/l10n.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import 'PrefData.dart';
import 'ThemeColor.dart';

class ConstantData {
  // static var timer = 1;
  static var timer = 30;
  static var hintCoin = 10;
  static var historyCoin = 10;
  static var levelSize = 30;
  static var delayTime = 1;
  static int addVideoCoin = 50;
  static double bannerSize = 65;

  // static var DelayTime = 2;

  static Color color1 = "#E65051".toColor();
  static Color color2 = "#2FAF74".toColor();
  static Color color3 = "#F3F3E9".toColor();
  static Color color4 = "#FBA629".toColor();
  static Color color5 = "#CA3D77".toColor();
  static Color color6 = "#39A1E8".toColor();
  static Color color7 = "#2FAF74".toColor();
  static Color color8 = "#FBA629".toColor();
  static Color color9 = "#CA3D77".toColor();
  static Color color10 = "#F3F3E9".toColor();

  // static Color primaryColor = "#04B3A2".toColor(),
  //     primaryDarkColor="#038074".toColor(),
  //     backgroundColors=ThemeColor.navcolorPrimary,
  //     cellColors=ThemeColor.navcolorPrimary,
  //     cardBackground,
  //     bottomNavColor,
  //     shadow1,
  //     shadow2;

  static Color primaryColor,
      primaryDarkColor,
      backgroundColors,
      cellColors,
      cardBackground,
      bottomNavColor,
      shadow1,
      shadow2;
  static Color textColors;
  static ThemeData themeData;
  static int yourStoryId = 2;

  static setThemePosition() async {
    int getThemePos = await PrefData.getThemePos();


    int posGet = await PrefData.getIsDarkMode();

    themeData = ThemeData(
      primaryColor: ThemeColor.themeColorsList[getThemePos],
      primaryColorDark: ThemeColor.themeColorsDarkList[getThemePos],
      backgroundColor: posGet == 0
          ? ThemeColor.themeBgColorsList[getThemePos]
          : "#0D0D0D".toColor(),
    );

    ConstantData.bottomNavColor = ThemeColor.themeBgColorsList[getThemePos];
    ConstantData.primaryColor = ThemeColor.themeColorsList[getThemePos];
    ConstantData.primaryDarkColor = ThemeColor.themeColorsDarkList[getThemePos];
    print("getcolor3=${ConstantData.primaryColor}");

    if (posGet == 1) {
      ConstantData.backgroundColors = "#121212".toColor();
      // ConstantData.backgroundColors = "#0D0D0D".toColor();

      ConstantData.shadow1 = ThemeColor.nightShadow2;
      ConstantData.shadow2 = ThemeColor.nightShadow1;
      ConstantData.textColors = Colors.white;
      ConstantData.cardBackground = "#1E2834".toColor();
    } else {
      ConstantData.backgroundColors = ThemeColor.themeBgColorsList[getThemePos];
      ConstantData.shadow1 = ThemeColor.lightShadow1;
      ConstantData.shadow2 = ThemeColor.lightShadow2;
      ConstantData.textColors = Colors.black;
      ConstantData.cardBackground = Colors.white;
    }
    ConstantData.cellColors = ThemeColor.themeBgColorsList[getThemePos];
  }

  static List<Color> getGradientColors() {
    return [ConstantData.primaryColor, ConstantData.primaryDarkColor];
  }

 //  static String getAppAdUnitId() {
 //    return Platform.isAndroid
 //        ? 'ca-app-pub-3940256099942544~3347511713'
 //        : 'ca-app-pub-3940256099942544~1458002511';
 //  }
 //
 //  static String getRewardBasedVideoAdUnitId() {
 //    return Platform.isAndroid
 //        ? 'ca-app-pub-3940256099942544/5224354917'
 //        : 'ca-app-pub-3940256099942544/1712485313';
 //  }
 //
 //  static String getScreenAppId() {
 //    return Platform.isAndroid
 //        ? 'ca-app-pub-3940256099942544/1033173712'
 //        : 'ca-app-pub-3940256099942544/4411468910';
 //  }
 // static String getBannerAppId() {
 //    return   Platform.isAndroid
 //        ? 'ca-app-pub-3940256099942544/6300978111'
 //        : 'ca-app-pub-3940256099942544/2934735716';
 //  }

  // static String getInterstitialAdUnitId() {
  //   if (Platform.isIOS) {
  //     return 'ca-app-pub-3940256099942544/4411468910';
  //   } else if (Platform.isAndroid) {
  //     return 'ca-app-pub-3940256099942544/1033173712';
  //   }
  //   return null;
  // }


  static String getInterstitialAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/4411468910';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/1033173712';
    }
    return "";
  }

  static String getRewardBasedVideoAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/1712485313';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/5224354917';
    }
    return "";
  }



  static String getBannerAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/2934735716';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/6300978111';
    }
    return null;
  }


  static Future<void> shareApp(BuildContext contexts) async {
    Share.share(S.of(contexts).appName, subject: 'https://flutter.dev/');
  }

  static launchEmail(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static Future<String> getPrintTime(String string) async {
    //
    var format = DateFormat("hh:mm:ss");

    var one = format.parse(string);
    final DateTime now = DateTime.now();

    final DateFormat formatter = DateFormat('hh:mm:ss');
    final String formatted = formatter.format(now);
    // dateWithT = formatted.substring(0, 8) + 'T' + formatted.substring(8);

    var two = format.parse(formatted);

    final split = two.difference(one).toString().split(':');

    String strHour = split[0];
    String strMin = split[1];
    String strSec = split[2].split(".")[0];

    if (int.parse(strHour) >= 3) {
      PrefData.setCheckOut(true);
    }
    if (int.parse(strHour) < 0) {
      PrefData.setCheckOut(true);
    }

    if (int.parse(strMin) < 0) {
      PrefData.setCheckOut(true);
    }

    if (int.parse(strSec) < 0) {
      PrefData.setCheckOut(true);
    }

    strHour = (2 - int.parse(strHour)).toString();
    strMin = (59 - int.parse(strMin)).toString();
    strSec = (60 - int.parse(strSec)).toString();

    if (strHour.length <= 1) {
      strHour = 0.toString() + strHour;
    }

    if (strMin.length <= 1) {
      strMin = 0.toString() + strMin;
    }

    if (strSec.length <= 1) {
      strSec = 0.toString() + strSec;
    }

    String rewardTime = await PrefData.getRewardTime();
    String s = strHour + ":" + strMin + ":" + strSec;
    if (s == rewardTime) {
      PrefData.setCheckOut(true);
    }
    // return "0"+(3 -int.parse(str_hour)).toString() + ":" +  (60 -int.parse(str_min)).toString() + ":" +  (60 -int.parse(str_sec)).toString() ;
    return s;
  }

  static getTimeString(String s) async {
    final DateFormat formatter = DateFormat('hh:mm:ss');

    print("string------" + s.toString());
    String date;
    if (s != null) {
      DateTime date1 = formatter.parse(s);
      DateTime date2 = formatter.parse(getCurrentTime());
      date = printDifference(date1, date2);
    }

    return date;
  }

  static String getCurrentTime() {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('hh:mm:ss');
    return formatter.format(now);
  }

  static String printDifference(DateTime startDate, DateTime endDate) {
    //milliseconds
    int different = endDate.microsecond - startDate.millisecond;

    // System.out.println("startDate : " + startDate);
    // System.out.println("endDate : " + endDate);
    // System.out.println("different : " + different);

    int secondsInMilli = 1000;
    int minutesInMilli = secondsInMilli * 60;
    int hoursInMilli = minutesInMilli * 60;

    double elapsedHours = different / hoursInMilli;
    different = different % hoursInMilli;

    double elapsedMinutes = different / minutesInMilli;
    different = different % minutesInMilli;

    double elapsedSeconds = different / secondsInMilli;

    if (elapsedHours >= 3) {
      PrefData.setCheckOut(true);
    }
    if (elapsedHours < 0) {
      PrefData.setCheckOut(true);
    }

    if (elapsedMinutes < 0) {
      PrefData.setCheckOut(true);
    }

    if (elapsedSeconds < 0) {
      PrefData.setCheckOut(true);
    }

    String strHour = elapsedHours.toString();
    String strMin = elapsedMinutes.toString();
    String strSec = elapsedSeconds.toString();

    // String str_hour = String.valueOf(elapsedHours);
    // String str_min = String.valueOf(elapsedMinutes);
    // String str_sec = String.valueOf(elapsedSeconds);

    if (strHour.length <= 1) {
      strHour = 0.toString() + strHour;
    }

    if (strMin.length <= 1) {
      strMin = 0.toString() + strMin;
    }

    if (strSec.length <= 1) {
      strSec = 0.toString() + strSec;
    }

    return strHour + ":" + strMin + ":" + strSec;
  }
}
