

import 'dart:ui';
import 'package:flutter/material.dart';


class ThemeColor with ChangeNotifier{
  static Color colorPrimary="#04B3A2".toColor();
  static Color colorPrimaryDark="#038074".toColor();
  static Color navcolorPrimary="#EAFCFE".toColor();
  static Color deepPurpleColorPrimary="#673AB7".toColor();
  static Color deepPurpleColorPrimaryDark="#4F2C8F".toColor();
  static Color navdeepPurpleColorPrimary="#F6F3FD".toColor();
  static Color pinkColorPrimary="#E82E6D".toColor();
  static Color pinkColorPrimaryDark="#CA1553".toColor();
  static Color navpinkColorPrimary="#FDF1F5".toColor();
  static Color orangeColorPrimary="#FE5722".toColor();
  static Color orangeColorPrimaryDark="#EB281A".toColor();
  static Color navorangeColorPrimary="#FAF3F1".toColor();
  static Color greenColorPrimary="#4CAF50".toColor();
  static Color greenColorPrimaryDark="#1A911E".toColor();
  static Color navgreenColorPrimary="#EFFAF0".toColor();
  static Color grayColorPrimary="#607D8B".toColor();
  static Color grayColorPrimaryDark="#3C545F".toColor();
  static Color navgrayColorPrimary="#EFF6FA".toColor();
  static Color accentColor="#FFC107".toColor();


  static Color lightShadow1="#E4E4E4".toColor();
  static Color lightShadow2="#EFEEEE".toColor();

  static Color nightShadow1="#283545".toColor();
  static Color nightShadow2="#24303E".toColor();



  // static List<Color> themeColorsList
  static final themeColorsList = <Color>[colorPrimary,deepPurpleColorPrimary,pinkColorPrimary,orangeColorPrimary,greenColorPrimary,grayColorPrimary];
  static final themeColorsDarkList = <Color>[colorPrimaryDark,deepPurpleColorPrimaryDark,pinkColorPrimaryDark,orangeColorPrimaryDark
    ,greenColorPrimaryDark,grayColorPrimaryDark];
  static final themeBgColorsList = <Color>[navcolorPrimary,navdeepPurpleColorPrimary,navpinkColorPrimary,navorangeColorPrimary,navgreenColorPrimary,navgrayColorPrimary];

  static ThemeData darkTheme = ThemeData(
    // primarySwatch: Colors.grey,
    // brightness: Brightness.dark,
    // cardColor: "#1E2834".toColor(),
    // textSelectionColor: Colors.white,
    // textTheme: TextTheme().apply(bodyColor: Colors.white),
    // accentColor: Colors.white,
    // accentIconTheme: IconThemeData(color: Colors.black),
    // dividerColor: Colors.black12,
  );

  static ThemeData lightTheme = ThemeData(
    // primarySwatch: Colors.grey,
    // brightness: Brightness.light,
    // cardColor: Colors.white,
    // textTheme: TextTheme().apply(bodyColor: Colors.black87),
    // textSelectionColor: Colors.black,
    // accentColor: Colors.black,
    // accentIconTheme: IconThemeData(color: Colors.white),
    // dividerColor: Colors.white54,
  );

  static final themesList = <ThemeData>[lightTheme,darkTheme];

  ThemeData _themeData;

  ThemeData getTheme() {
    print("themedata===$_themeData");
    return _themeData;
  }

  // getThemeDatas() async {
  //   // ThemeData data;
  //
  //   if (await isDarkTheme()) {
  //     print("darktheme==true");
  //     return darkTheme;
  //   } else {
  //     print("darktheme==false");
  //     return lightTheme;
  //   }
  //
  // }
  //
  // isDarkTheme() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   // prefs.get('themeMode');
  //   String getval = prefs.getString('themeMode') ?? 'light';
  //   print("getval===$getval");
  //   if (getval == 'light') {
  //     return false;
  //   }
  //   return true;
  // }
  //
  //
  // themeNotifier() {
  //   PrefrenceManager.readData('themeMode').then((value) {
  //     print('value read from storage: ' + value.toString());
  //     // var themeMode = value ?? 'dark';
  //     var themeMode = value ?? 'light';
  //     print("theme mode===$themeMode");
  //     if (themeMode == 'light') {
  //       _themeData = lightTheme;
  //     } else {
  //       print('setting dark theme');
  //       _themeData = darkTheme;
  //     }
  //     notifyListeners();
  //   });
  // }

  // void setDarkMode() async {
  //   _themeData = darkTheme;
  //   PrefrenceManager.saveData('themeMode', 'dark');
  //   notifyListeners();
  // }
  //
  // void setLightMode() async {
  //   _themeData = lightTheme;
  //   PrefrenceManager.saveData('themeMode', 'light');
  //   notifyListeners();
  // }
}

extension ColorExtension on String {
  toColor() {
    var hexColor = this.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      return Color(int.parse("0x$hexColor"));
    }
  }
}