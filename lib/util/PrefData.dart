import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
  import 'package:shared_preferences/shared_preferences.dart';

class PrefData {
  static String fontSizes = "storytextSize";
  static String coinDate = "coinDate";
  static String themePos = "themePosition";
  static String coins = "coins";
  static String isCheckOut = "IsCheckOut";
  static String checkOutTime = "CheckOutTime";
  static String vibration = "vibration";
  static String reminder = "reminder";
  static String sound = "sound";
  static String sliderId = "sliderId";
  static String darkModes = "QuizDarkMode";
  static String lastReadStory = "lastReadStory";
  static String appLanguage = "appLanguage";
  static String fontSize = "fontSize";
  static String reward = "reward";

  static addFontSize(int sizes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(fontSizes, sizes);
  }

  static setRewardTime(String coin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(reward, coin);
  }

  static getRewardTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String intValue = prefs.getString(reward) ?? null;

    print("intValue===$intValue");
    return intValue;
  }


  static setCoinsDateList(String coin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(coinDate, coin);
  }

  static getCoinsDateList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String intValue = prefs.getString(coinDate) ?? null;
    print("intValue54===$intValue");
    return intValue;
  }


  getAppLanguage(BuildContext context) async {

    return "en";
  }

  setAppLanguage(String sizes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(appLanguage, sizes);
  }

  setLastReadStory(int sizes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(lastReadStory, sizes);
  }

  static setDarkModes(int sizes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(darkModes, sizes);
  }

  static setThemePos(int sizes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(themePos, sizes);
  }

  static setCoins(int coin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(coins, coin);
  }

  static getCoins() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(coins) ?? 0;

    print("intValue===$intValue");
    return intValue;
  }

  static setCheckOut(bool ischeck) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(isCheckOut, ischeck);
    print("time---"+ischeck.toString());

    if (!ischeck) {
      setCheckOutTime();
    }
  }

  static getCheckOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(isCheckOut) ?? true;
  }

  static setCheckOutTime() async {
    final DateTime now = DateTime.now();
    // final DateFormat formatter = DateFormat('yyyyMMddHHmmss');
    final DateFormat formatter = DateFormat('hh:mm:ss');
    final String formatted = formatter.format(now);


    print("time---"+formatted.toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(checkOutTime, formatted);
  }

  static getCheckOutTime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(checkOutTime) ?? null;
  }

  static setVibration(bool rem) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(vibration, rem);
  }

  static getVibration() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool intValue = prefs.getBool(vibration) ?? true;
    return intValue;
  }

  static setSound(bool rem) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(sound, rem);
  }

  static getSound() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool intValue = prefs.getBool(sound) ?? true;
    return intValue;
  }


  static setReminder(int rem) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(reminder, rem);
  }

  static getReminder() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(reminder) ?? 1;
    return intValue;
  }




  static setSliderId(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(sliderId, id);
  }

  static getSliderId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(sliderId) ?? 1;
    return intValue;
  }




  getFontSize() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(fontSizes) ?? 13;
    return intValue;
  }

  getLastReadStory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int intValue = prefs.getInt(lastReadStory) ?? 0;
    return intValue;
  }

  static getIsDarkMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    int intValue = prefs.getInt(darkModes) ?? 0;
    return intValue;
  }

  static getThemePos() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    int intValue = prefs.getInt(themePos) ?? 0;
    return intValue;
  }


}
