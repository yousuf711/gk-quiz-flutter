// import 'package:ads/ads.dart';
// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gk_design/model/MainModel.dart';
import 'package:flutter_gk_design/model/SubCategories.dart';
import 'package:flutter_gk_design/util/PrefData.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:toast/toast.dart';

import 'QuizPage.dart';
import 'ReviewHistory.dart';
import 'db/database_helper.dart';
import 'generated/l10n.dart';
import 'model/DataModel.dart';
import 'model/ProgressModel.dart';
import 'util/ConstantData.dart';

class ScorePage extends StatefulWidget {
  final ProgressModel progressModel;
  final SubCategories subCatModel;
  final MainModel modelCat;
  final int rightCount, wrongCount, refId;

  // final Ads ad;

  ScorePage(
      {this.subCatModel,
      this.modelCat,
      this.progressModel,
      this.rightCount,
      this.wrongCount,
      this.refId,
      // this.ad,
      this.onChanged});

  final ValueChanged<bool> onChanged;

  @override
  _ScorePage createState() => _ScorePage(
        subCatModel: subCatModel,
        modelCat: modelCat,
        progressModel: progressModel,
        rightCount: rightCount,
        wrongCount: wrongCount,
        refId: refId,
        onChanged: onChanged,
      );
}

class _ScorePage extends State<ScorePage> with WidgetsBindingObserver {
  SubCategories subCatModel;
  ProgressModel progressModel;
  MainModel modelCat;
  int rightCount, wrongCount;
  int _coin = 0;
  int refId = 0;
  final ValueChanged<bool> onChanged;

  DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  double _countDownPercent = 0;
  bool isHistoryAvail = false;

  // _ScorePage(this.subCatModel, this.modelCat, this.progressModel,
  //     this._rightCount, this._wrongCount, this._refId);

  _ScorePage(
      {this.subCatModel,
      this.modelCat,
      this.progressModel,
      this.rightCount,
      this.wrongCount,
      this.refId,
      this.onChanged});

  // Ads ads;
  AdmobInterstitial interstitialAd;
  AdmobReward rewardAd;
  bool _isRewarded = false;

  @override
  void dispose() {
    interstitialAd.dispose();
    rewardAd.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    ConstantData.setThemePosition();

    interstitialAd = AdmobInterstitial(
      adUnitId: ConstantData.getInterstitialAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        if (event == AdmobAdEvent.closed) {
          _passQuizData(progressModel);
        }
      },
    );
    interstitialAd.load();

    rewardAd = AdmobReward(
      adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        print("event12----1--true" + event.toString());
        handleEvent(event, args, 'Reward');
      },
    );

    rewardAd.load();

    //
    // ads = Ads(ConstantData.getAppAdUnitId());
    // // ads = widget.ad;
    // // ads.hideBannerAd();
    // var videoListener =
    //     (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
    //   if (event == RewardedVideoAdEvent.rewarded) {
    //     _passHistoryData();
    //   } else if (event == RewardedVideoAdEvent.closed) {
    //   } else if (event == RewardedVideoAdEvent.loaded) {
    //     _isVideoLoaded = true;
    //   }
    // };
    //
    // ads.setVideoAd(
    //   adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
    //   keywords: ['dart', 'java'],
    //   childDirected: true,
    //   testDevices: null,
    //   listener: videoListener,
    // );

    // ads.setFullScreenAd(
    //   adUnitId: ConstantData.getScreenAppId(),
    //   keywords: ['dart', 'java'],
    //   childDirected: true,
    //   testDevices: null,
    // );
    //
    // ads.screenListener = (MobileAdEvent event) {
    //   switch (event) {
    //     case MobileAdEvent.loaded:
    //       _isScreenLoaded = true;
    //       break;
    //     case MobileAdEvent.closed:
    //       _passQuizData(progressModel);
    //       break;
    //     default:
    //       print("There's a 'new' MobileAdEvent?!");
    //   }
    // };

    _getCoin();

    _databaseHelper.getHistoryData(refId).then((value) {
      if (value != null) {
        setState(() {
          if (value.length > 0) {
            isHistoryAvail = true;
          } else {
            isHistoryAvail = false;
          }
        });
      }
    });

    WidgetsBinding.instance.addObserver(this);
  }

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    print("event00000---" + event.toString());
    switch (event) {
      case AdmobAdEvent.loaded:
        break;
      case AdmobAdEvent.opened:
        break;
      case AdmobAdEvent.closed:
        rewardAd.load();

        if (_isRewarded) {
          _passHistoryData();
        }
        break;
      case AdmobAdEvent.failedToLoad:
        break;
      case AdmobAdEvent.rewarded:
        _isRewarded = true;

        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    ConstantData.setThemePosition();
    SizeConfig().init(context);
    _getCoin();
    return WillPopScope(
        child: Scaffold(
          backgroundColor: ConstantData.backgroundColors,
          body: Container(
            child: Column(
              children: [_appBar(), _cardView(), _scoreView(), _bottomView()],
            ),
          ),
        ),
        onWillPop: _requestPop);
  }

  Widget _scoreView() {
    return Container(
      margin: EdgeInsets.only(top: 45, bottom: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            S.of(context).score + " : " + progressModel.score.toString(),
            maxLines: 1,
            style: TextStyle(
                fontSize: 18,
                fontFamily: "SFProText",
                color: ConstantData.textColors,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            S.of(context).bestScore +
                " : " +
                progressModel.highScore.toString(),
            maxLines: 1,
            style: TextStyle(
                fontFamily: "SFProText",
                fontSize: 18,
                color: ConstantData.textColors,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              // margin: EdgeInsets.symmetric(vertical: 5),

              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(progressModel.progress > 0 ? Icons.star : Icons.star_border,
                  color: ConstantData.textColors, size: 30),
              Icon(progressModel.progress > 1 ? Icons.star : Icons.star_border,
                  color: ConstantData.textColors, size: 30),
              Icon(progressModel.progress > 2 ? Icons.star : Icons.star_border,
                  color: ConstantData.textColors, size: 30),
            ],
          )),
          SizedBox(
            height: 0,
          ),
          InkWell(
            child: Container(
                margin: EdgeInsets.all(25),
                height: 45,
                width: 170,
                decoration: BoxDecoration(
                    color: _getColor(),
                    borderRadius: BorderRadius.all(Radius.circular(7))),
                child: Center(
                  child: Text(
                    S.of(context).reviewAnswer,
                    style: TextStyle(
                        fontFamily: "SFProText",
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                        color: Colors.white,
                        decoration: TextDecoration.none),
                  ),
                )),
            onTap: () {
              if (progressModel.progress > 0 && isHistoryAvail) {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return HistoryDialog(
                        onSelectedOkChanged: (value) {
                          setState(() {
                            if (value == 0) {
                              showVideo();
                            } else {
                              Navigator.pop(context);
                              _sendData();
                            }
                          });
                        },
                      );
                    });
              }
            },
          )
        ],
      ),
    );
  }

  void showVideo() async {
    if (await rewardAd.isLoaded) {
      _isRewarded = false;
      rewardAd.show();
      Navigator.pop(context);
    } else {
      _showToast(S.of(context).strVideoError);
    }
  }

  Future<void> _sendData() async {
    int coins = await PrefData.getCoins();
    if ((coins - ConstantData.historyCoin) >= 0) {
      PrefData.setCoins((coins - 10));
      _passHistoryData();
    } else {
      print("toast---true12" + coins.toString());
      _showToast(S.of(context).coinsError);
    }
  }

  _showToast(String s) {
    Toast.show(s, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  void _passHistoryData() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ReviewHistory(refId),
        // builder: (context) => ReviewHistory(refId,widget.ad),
      ),
    );
  }

  Widget _bottomView() {
    print("ddd---true");
    return Expanded(
      child: Container(
        height: double.infinity,
        margin: EdgeInsets.all(15),
        child: Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: 70,
                  child: Row(
                    children: [
                      Expanded(
                          child: InkWell(
                            child: _button(Icons.refresh, S.of(context).retry,
                                ConstantData.cardBackground),
                            onTap: () async {
                              if (await interstitialAd.isLoaded) {
                                interstitialAd.show();
                              } else {
                                _passQuizData(progressModel);
                              }
                            },
                          ),
                          flex: 1),
                      Expanded(
                          child: InkWell(
                            child: _button(Icons.navigate_next,
                                S.of(context).next, _getNext()),
                            onTap: () async {
                              if (progressModel.progress > 3) {
                                if ((progressModel.levelNo <=
                                    ConstantData.levelSize)) {
                                  _databaseHelper
                                      .getProgressLevelsByCat(
                                          subCatModel.title,
                                          subCatModel.id.toString(),
                                          (progressModel.id + 1))
                                      .then((value) {
                                    print("value---" + value.toString());
                                    if (value != null) {
                                      _passQuizData(value[0]);
                                    }
                                  });
                                }
                              }
                            },
                          ),
                          flex: 1),
                    ],
                  ),
                ),
                Container(
                  height: 70,
                  child: Row(
                    children: [
                      Expanded(
                          child: InkWell(
                            child: _button(Icons.share, S.of(context).share,
                                ConstantData.cardBackground),
                            onTap: () {
                              ConstantData.shareApp(context);
                            },
                          ),
                          flex: 1),
                      Expanded(
                          child: InkWell(
                            child: _button(Icons.home, S.of(context).home,
                                ConstantData.cardBackground),
                            onTap: () {
                              _requestPop();
                            },
                          ),
                          flex: 1),
                    ],
                  ),
                )
              ],
            )),
      ),
      flex: 2,
    );
  }

  void _handleTapboxChanged(DataModel newValue) {
    if (newValue != null) {
      if (newValue.isPass) {
        Navigator.of(context).pop();
      }

      this.progressModel = newValue.progressModel;
      this.modelCat = newValue.modelCat;
      this.subCatModel = newValue.subCatModel;
      this.rightCount = newValue.rightCount;
      this.wrongCount = newValue.wrongCount;
      this.refId = newValue.refId;

      setState(() {});
      setState(() {});
    }
  }

  Future<bool> _passQuizData(ProgressModel progressModel) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => QuizPage(
          progressModel: progressModel,
          subCatModel: this.subCatModel,
          mainModel: this.modelCat,
          onChanged: _handleTapboxChanged,
        ),
        // builder: (context) => GridItemDetails(this.item),
      ),
    );
    return new Future.value(true);
  }

  Widget _button(IconData icon, String s, Color color) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Card(
        color: color,
        margin: EdgeInsets.all(8),
        elevation: 1.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 30,
              color: ConstantData.textColors,
            ),
            Container(
              margin: EdgeInsets.only(left: 5),
              child: Text(
                s,
                maxLines: 1,
                style: TextStyle(
                    fontFamily: "SFProText",
                    fontSize: 20,
                    color: ConstantData.textColors,
                    fontWeight: FontWeight.w600),
              ),
            )
          ],
        ),
      ),
    );
  }

  Color _getColor() {
    if (progressModel.progress > 0 && isHistoryAvail) {
      return ConstantData.primaryColor;
    } else {
      return ConstantData.primaryColor.withOpacity(0.5);
    }
  }

  Color _getNext() {
    print("condition----" +
        (progressModel.progress == 3).toString() +
        "----" +
        (progressModel.levelNo <= ConstantData.levelSize).toString());
    if (progressModel.progress > 0) {
      if ((progressModel.levelNo <= ConstantData.levelSize)) {
        return ConstantData.cardBackground;
      } else {
        return ConstantData.cardBackground.withOpacity(0.5);
      }
    } else {
      return ConstantData.cardBackground.withOpacity(0.5);
    }
  }

  Widget _cardView() {
    return Container(
      height: SizeConfig.safeBlockVertical * 26,
      child: Card(
          margin: EdgeInsets.all(15),
          color: ConstantData.primaryColor,
          elevation: 2,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: ConstantData.getGradientColors(),
                ),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            padding: EdgeInsets.only(left: 15, top: 15, bottom: 15),
            height: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    height: double.infinity,
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/right.png',
                                      height: 22,
                                      width: 22,
                                      fit: BoxFit.fitWidth),
                                  Container(
                                    margin: EdgeInsets.only(left: 8),
                                    child: Text(
                                      rightCount.toString(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: "SFProText",
                                          fontWeight: FontWeight.w500,
                                          fontSize: 15,
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 12),
                            Row(
                              children: [
                                Image.asset(
                                  'assets/images/wrong.png',
                                  height: 22,
                                  width: 22,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 8),
                                  child: Text(
                                    wrongCount.toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15,
                                        color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 12),
                            Row(
                              children: [
                                Image.asset(
                                  'assets/images/coin.png',
                                  height: 22,
                                  width: 22,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 8),
                                  child: Text(
                                    _coin.toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15,
                                        color: Colors.white),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: Container(),
                  flex: 1,
                ),
                Expanded(
                  child: Container(
                      height: double.infinity,
                      child: Align(
                          alignment: Alignment.center, child: _progressView())),
                  flex: 2,
                )
              ],
            ),
          )),
    );
  }

  Widget _progressView() {
    return Container(
      height: double.infinity,
      child: Stack(
        children: [
          Center(
            child: CircularStepProgressIndicator(
              totalSteps: ConstantData.levelSize,
              currentStep: progressModel.levelNo,
              stepSize: 8,
              selectedColor: ConstantData.primaryColor,
              unselectedColor: ConstantData.backgroundColors,
              padding: 0,
              width: 110,
              height: 110,
              selectedStepSize: 8,
              roundedCap: (_, __) => true,
            ),
          ),
          // ),

          Container(
            height: double.infinity,
            child: Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      (progressModel.levelNo).toString(),
                      // (progressModel.level_no).toString(),
                      style: TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.w800,
                          fontSize: 22,
                          color: Colors.white),
                    ),
                    Text(
                      "/" + ConstantData.levelSize.toString(),
                      style: TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: Colors.white),
                    ),
                  ],
                ),
                new Text(
                  S.of(context).levels,
                  style: new TextStyle(
                      fontFamily: "SFProText",
                      fontWeight: FontWeight.w500,
                      fontSize: 18.0,
                      color: Colors.white),
                ),
              ],
            )),
          )

          // ,
        ],
      ),
    );
  }

  Future<void> _getCoin() async {
    _coin = await PrefData.getCoins();

    _countDownPercent =
        ((progressModel.levelNo * ConstantData.levelSize) / 100);

    print("_countDownPercent----" + _countDownPercent.toString() + "----");
    setState(() {});
  }

  Widget _appBar() {
    return Container(
        color: ConstantData.backgroundColors,
        height: SizeConfig.safeBlockVertical * 7,
        child: Stack(
          children: [
            Container(
              height: SizeConfig.safeBlockVertical * 7,
              child: Center(
                child: Text(
                  subCatModel.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "SFProText",
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      color: ConstantData.textColors),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5),
                  child: new IconButton(
                    icon: Icon(
                      Icons.arrow_back_outlined,
                      color: ConstantData.textColors,
                      size: 25,
                    ),
                    onPressed: () {
                      _requestPop();
                    },
                  ),
                ),
                new Spacer(),
              ],
            )
          ],
        ));
  }

  Future<bool> _requestPop() {
    print("back----true");
    widget.onChanged(null);
    Navigator.of(context).pop();

    return new Future.value(true);
  }
}

class HistoryDialog extends StatefulWidget {
  HistoryDialog({this.onSelectedOkChanged});

  final ValueChanged<int> onSelectedOkChanged;

  @override
  _HistoryDialog createState() => _HistoryDialog();
}

class _HistoryDialog extends State<HistoryDialog> {
  @override
  void initState() {
    _setTheme();
    super.initState();
  }

  Future<void> _setTheme() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Wrap(
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: ConstantData.cardBackground,
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Column(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          margin: EdgeInsets.all(15),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: ConstantData.textColors,
                              width: 1,
                            ),
                          ),
                          child: Icon(
                            Icons.close,
                            color: ConstantData.textColors,
                            size: 20,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.only(left: 15, right: 15),
                        padding: EdgeInsets.only(left: 20, right: 15),
                        height: SizeConfig.safeBlockVertical * 8,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: ConstantData.getGradientColors(),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/video_icon.png",
                              height: SizeConfig.safeBlockVertical * 4.5,
                              width: SizeConfig.safeBlockVertical * 4.5,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20),
                              child: Text(
                                S.of(context).watchVideoAds,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "SFProText",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.white),
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () {
                        widget.onSelectedOkChanged(0);
                      },
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.all(15),
                        padding: EdgeInsets.only(left: 20, right: 15),
                        height: SizeConfig.safeBlockVertical * 8,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: ConstantData.getGradientColors(),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              "assets/images/coin.png",
                              height: SizeConfig.safeBlockVertical * 4.5,
                              width: SizeConfig.safeBlockVertical * 4.5,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    S.of(context).viewAnswer,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: "SFProText",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        color: Colors.white),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 2),
                                    child: Text(
                                      S.of(context).useCoins,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: "SFProText",
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () {
                        widget.onSelectedOkChanged(1);
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
