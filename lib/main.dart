// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gk_design/SettingPage.dart';
import 'package:flutter_gk_design/model/MainModel.dart';
import 'package:flutter_gk_design/util/ConstantData.dart';
import 'package:flutter_gk_design/util/MyBlinkingButton.dart';
import 'package:flutter_gk_design/util/PrefData.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:toast/toast.dart';
import 'CoinPage.dart';
import 'LevelPage.dart';
import 'ReminderPage.dart';
import 'db/database_helper.dart';
import 'generated/l10n.dart';
import 'model/CoinModel.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();


  final int helloAlarmID = 0;
  await AndroidAlarmManager.initialize();
  Admob.initialize();

  runApp(MyApp());
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  // Admob.initialize();
  // final int helloAlarmID = 0;
  // await AndroidAlarmManager.initialize();

  await AndroidAlarmManager.periodic(const Duration(seconds: 60), helloAlarmID, printHello);

}

void printHello() {
  print("hello----world! isolate='");

  FlutterLocalNotificationsPlugin flip = new FlutterLocalNotificationsPlugin();
  var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
  var ios = new IOSInitializationSettings();
  var settings = new InitializationSettings(android: android, iOS: ios);
  flip.initialize(settings);

  print("homePage12---true");
  print("homePage---true");

  DatabaseHelper _databaseHelper = DatabaseHelper.instance;

  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('hh:mm a','en_US');
  final DateFormat dayFormatter = DateFormat('EE','en_US');
  final String currentTime = formatter.format(now);
  final String currentDay = dayFormatter.format(now);
  print("currentDay----" + currentDay + "---");

  _showRewardNotification(flip);

  _databaseHelper.getNotificationData(currentTime).then((value) {
    if (value != null && value.length > 0) {
      print("value12---" + value.length.toString());

      if (value[0].repeat != null) {
        if (value[0].repeat.contains(currentDay)) {
          _showNotificationWithDefaultSound(flip);
        }
      }
    }
  });
}

Future _showRewardNotification(flip) async {

  final DateTime now = DateTime.now();

  String rewardTime = await PrefData.getRewardTime();

  final DateFormat formatter = DateFormat('hh:mm:ss');

  if (rewardTime !=null) {
    if (formatter.format(now) == rewardTime) {
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'your channel id', 'your channel name', 'your channel description',
          importance: Importance.max, priority: Priority.high);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();

      // initialise channel platform for both Android and iOS device.
      var platformChannelSpecifics = new NotificationDetails(
          android: androidPlatformChannelSpecifics,
          iOS: iOSPlatformChannelSpecifics);

      await flip.show(0, "Reward", 'Daily Reward', platformChannelSpecifics,
          payload: 'Default_Sound');
    }else{
      PrefData.setRewardTime(null);
    }
  }
}

Future _showNotificationWithDefaultSound(flip) async {
  // Show a notification after every 15 minute with the first
  // appearance happening a minute after invoking the method

  int _isReminder;

  _isReminder = await PrefData.getReminder();


  if (_isReminder == 1 ? true : false) {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();

    // initialise channel platform for both Android and iOS device.
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    await flip.show(0, "Reminder", 'Daily Reminder', platformChannelSpecifics,
        payload: 'Default_Sound');
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ConstantData.setThemePosition();
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English US
        const Locale('en', 'GB'), // English UK
        // ... other locales the app supports
      ],
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: ConstantData.primaryColor,
        primaryColorDark: ConstantData.primaryDarkColor,
        accentColor: ConstantData.primaryColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  List<MainModel> mainCatList = new List();
  int _coin = 0;
  int theme = 0;


  List<CoinModel> coinModel = new List();



  void setMainData() {
    _databaseHelper.getAllMainCatList().then((value) {
      setState(() {
        print("value=---" + value.toString());
        value.forEach((element) {
          MainModel mainCat = MainModel.fromMap(element);

          if (mainCat.id == 1) {
            mainCat.isExpand = true;
          } else {
            mainCat.isExpand = false;
          }

          _databaseHelper.getAllSubCatByMainCat(mainCat.id).then((value) {
            mainCat.subModelList = value;
            print("subModelList=---" +
                mainCat.id.toString() +
                "----" +
                mainCat.subModelList.length.toString());

            setState(() {});
          });

          if (mainCat.id != 2) {
            mainCatList.add(mainCat);
          }
        });
      });
    });
    //
    //
  }

  Future<void> _getCoin() async {
    _coin = await PrefData.getCoins();
    theme = await PrefData.getThemePos();
    setState(() {
    });
  }

  RateMyApp _rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
    googlePlayIdentifier: 'com.example.flutter_gk_design',
    // googlePlayIdentifier: 'fr.skyost.example',
    appStoreIdentifier: '1491556149',
  );

  // Ads ads;
  @override
  void initState() {
    super.initState();
    // ads = Ads(ConstantData.getAppAdUnitId());
    setMainData();
    _getCoin();
    ConstantData.setThemePosition();
    setState(() {});
    print('ads---reuw');

    _rateMyApp.init().then((_) {
      _rateMyApp.conditions.forEach((condition) {
        if (condition is DebuggableCondition) {
          // We iterate through our list of conditions and we print all debuggable ones.
        }
      });

      print('Are all conditions met ? ' +
          (_rateMyApp.shouldOpenDialog ? 'Yes' : 'No'));

      // if (_rateMyApp.shouldOpenDialog) _buildShowStarRateDialog(context);
    });
  }

  Future<bool> _requestPop() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Wrap(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: ConstantData.getGradientColors(),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).exit,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 22,
                            fontFamily: "SFProText",
                            color: Colors.white),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            S.of(context).exitMsg,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                                fontFamily: "SFProText",
                                color: Colors.white),
                          )),
                      Container(
                        padding: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                                child: Container(
                                    height:
                                    SizeConfig.safeBlockVertical * 6,
                                    padding: EdgeInsets.only(right: 15),
                                    child: Center(
                                      child: Text(
                                        S.of(context).yes,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      ),
                                    )),
                                onTap: () {
                                  Navigator.pop(context);
                                  Future.delayed(
                                      const Duration(milliseconds: 200),
                                          () {
                                        SystemChannels.platform.invokeMethod(
                                            'SystemNavigator.pop');
                                      });
                                }),
                            InkWell(
                                child: Container(
                                    height:
                                    SizeConfig.safeBlockVertical * 6,
                                    child: Center(
                                      child: Text(
                                        S.of(context).no,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      ),
                                    )),
                                onTap: () {
                                  Navigator.pop(context);
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        }) ??
        false;

    // Future.delayed(const Duration(milliseconds: 200), () {
    //   SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    // });
    // return new Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    ConstantData.setThemePosition();
    setState(() {});

    double height = SizeConfig.safeBlockVertical * 8;
    double margin = SizeConfig.safeBlockVertical * 2.5;
    double padding = SizeConfig.safeBlockVertical * 1.5;
    double size = SizeConfig.safeBlockVertical * 5;
    double cellSize = SizeConfig.safeBlockVertical * 10;

    return WillPopScope(
        child: Scaffold(
            backgroundColor: ConstantData.backgroundColors,
            drawer: Drawer(
              child: Container(
                  color: ConstantData.backgroundColors,
                  child: ListView(
                    padding: const EdgeInsets.all(0.0),
                    children: [
                      DrawerHeader(
                        padding: const EdgeInsets.all(0.0),
                        child: Image.asset("assets/images/drawer_icon.png"),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: ConstantData.getGradientColors(),
                            )),
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).dashBoard,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.home_outlined,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {},
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).reminder,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.notifications_none,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ReminderPage(),
                              ));
                        },
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).coins + ": " + _coin.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Image.asset(
                          "assets/images/coins.png",
                          height: 28,
                          width: 25,
                          color: ConstantData.textColors,
                        ),
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).dailyReward,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.card_giftcard,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CoinPage(),
                                // builder: (context) => CoinPage(ads),
                              ));
                        },
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).settings,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.settings,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SettingPage(),
                              )).then((value) {
                            setState(() {
                              ConstantData.setThemePosition();
                            });
                          });
                        },
                      ),
                      Divider(
                        height: 1,
                        thickness: 1,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          S.of(context).communicate,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).privacyPolicy,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.privacy_tip_outlined,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          ConstantData.launchURL("https://google.com");
                        },
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).share,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.share,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          ConstantData.shareApp(context);
                        },
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).rateUs,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.rate_review,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          _buildShowStarRateDialog(context);
                        },
                      ),
                      ListTile(
                        title: Text(
                          S.of(context).feedback,
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              fontFamily: "SFProText",
                              color: ConstantData.textColors),
                        ),
                        leading: Icon(
                          Icons.send,
                          size: 28,
                          color: ConstantData.textColors,
                        ),
                        onTap: () {
                          ConstantData.launchEmail('demo@gmail.com',
                              'Flutter Email Test', 'Hello Flutter');
                        },
                      ),
                    ],
                  )),
            ),
            appBar: AppBar(
              backgroundColor: ConstantData.backgroundColors,
              elevation: 0,
              iconTheme: IconThemeData(color: ConstantData.textColors),
              title: Text(
                S.of(context).appName,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    fontFamily: "SFProText",
                    color: ConstantData.textColors),
              ),
              actions: [
                MyBlinkingButton(),
                // MyBlinkingButton(ads),
              ],
            ),
            body: Container(
              height: double.infinity,
              padding: EdgeInsets.only(left: margin, right: margin),
              child: ListView.builder(
                itemCount: mainCatList.length,
                shrinkWrap: true,
                primary: true,
                itemBuilder: (context, index) {
                  return Container(
                    child: Stack(
                      children: [
                        InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: ConstantData.getGradientColors(),
                                ),
                                borderRadius:
                                BorderRadius.all(Radius.circular(8))),
                            padding:
                            EdgeInsets.only(left: padding, right: padding),
                            margin: EdgeInsets.only(
                                top: (index == 10) ? 0 : margin),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: margin),
                                  width: size,
                                  height: size,
                                  decoration: new BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Text(
                                      mainCatList[index].title[0].toUpperCase(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20,
                                          fontFamily: "SFProText",
                                          color: ConstantData.primaryColor),
                                    ),
                                  ),
                                ),
                                Text(
                                  mainCatList[index].title,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      fontFamily: "SFProText",
                                      color: Colors.white),
                                ),
                                new Spacer(),
                                Icon(
                                  mainCatList[index].isExpand
                                      ? Icons.keyboard_arrow_down
                                      : Icons.navigate_next,
                                  size: 25,
                                  color: Colors.white,
                                )
                              ],
                            ),
                            height: height,
                          ),
                          onTap: () {
                            mainCatList[index].isExpand =
                            !mainCatList[index].isExpand;
                            mainCatList.forEach((element) {
                              if (element.id != mainCatList[index].id)
                                element.isExpand = false;
                              print("mainList=---" +
                                  mainCatList.length.toString());
                            });

                            setState(() {});
                          },
                        ),
                        Visibility(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(8),
                                    bottomRight: Radius.circular(8)),
                                color: ConstantData.cardBackground),

                            // padding: EdgeInsets.all(margin),
                            margin:
                            EdgeInsets.only(top: ((height + margin) - 5)),
                            padding:
                            EdgeInsets.only(top: margin, bottom: margin),

                            child: GridView.count(
                              crossAxisCount: 3,
                              childAspectRatio: 1.1,
                              mainAxisSpacing: 12,
                              crossAxisSpacing: 8,
                              shrinkWrap: true,
                              children: List.generate(
                                mainCatList[index].subModelList != null
                                    ? mainCatList[index].subModelList.length
                                    : 0,
                                    (i) {
                                  return InkWell(
                                    child: Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(bottom: 5),
                                              width: cellSize,
                                              height: cellSize,
                                              child: Card(
                                                color: ConstantData.cellColors,
                                                margin: EdgeInsets.all(10),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(10),
                                                ),
                                                child: Center(
                                                  child: Image.asset(
                                                    "assets/images/" +
                                                        mainCatList[index]
                                                            .subModelList[i]
                                                            .icon,
                                                    height: 25,
                                                    width: 25,
                                                    color:
                                                    ConstantData.primaryColor,
                                                  ),
                                                  //
                                                ),
                                              ),
                                            ),
                                            Text(
                                              mainCatList[index]
                                                  .subModelList[i]
                                                  .title
                                                  .trim(),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontFamily: "SFProText",
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 15,
                                                  color: ConstantData.textColors),
                                            ),
                                          ],
                                        )),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => LevelPage(
                                                mainCatList[index],
                                                mainCatList[index]
                                                    .subModelList[i]),
                                          ));
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          visible: mainCatList[index].isExpand,
                        )
                      ],
                    ),
                  );
                },
              ),
            )),
        onWillPop: _requestPop);
  }

  _buildShowStarRateDialog(BuildContext context) {
    _rateMyApp.showStarRateDialog(context, actionsBuilder: (_, count) {
      final Widget cancelButton = FlatButton(
        child: Text(
          S.of(context).cancel,
          style: new TextStyle(
              fontFamily: "SFProText", color: ConstantData.primaryColor),
        ),
        onPressed: () async {
          Navigator.pop(context);

          setState(() {});
        },
      );
      if (count == null || count == 0) {
        // If there is no rating (or a 0 star rating), we only have to return our cancel button.
        return [cancelButton];
      }

      // Otherwise we can do some little more things...
      String message = 'You\'ve put ' + count.round().toString() + ' star(s). ';
      Color color;
      switch (count.round()) {
        case 1:
          message += S.of(context).didThisAppHurtYouPhysically;
          color = Colors.red;
          break;
        case 2:
          message += S.of(context).thatsNotReallyCoolMan;
          color = Colors.orange;
          break;
        case 3:
          message += S.of(context).wellItsAverage;
          color = Colors.blue;
          break;
        case 4:
          message += S.of(context).thisIsCoolLikeThisApp;
          color = Colors.deepOrange;
          break;
        case 5:
          message += S.of(context).great3;
          color = Colors.green;
          break;
      }

      return [
        FlatButton(
          child: Text(
            S.of(context).ok,
            style: new TextStyle(
                fontFamily: "SFProText", color: ConstantData.primaryColor),
          ),
          onPressed: () async {
            print(message);
            print("msgs&$message");

            Toast.show(message, context,
                duration: Toast.LENGTH_SHORT,
                gravity: Toast.BOTTOM,
                backgroundColor: color,
                textColor: Colors.white);

            await _rateMyApp.callEvent(RateMyAppEventType.rateButtonPressed);
            Navigator.pop<RateMyAppDialogButton>(
                context, RateMyAppDialogButton.rate);

            setState(() {});
          },
        ),
        cancelButton,
      ];
    });
  }
}
// https://github.com/Nilesh1206/Flutter_Task1