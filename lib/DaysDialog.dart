import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gk_design/util/ConstantData.dart';

import 'generated/l10n.dart';

class DaysDialog extends StatefulWidget {
  final ValueChanged<List<String>> onSelectedOkChanged;
  final List<String> dayList;
  final List<String> selectedList;

  DaysDialog({this.dayList, this.selectedList, this.onSelectedOkChanged});

  @override
  _DaysDialog createState() => _DaysDialog();
}

class _DaysDialog extends State<DaysDialog> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ConstantData.setThemePosition();
    return AlertDialog(
      title: Text(S.of(context).selectDays,
          textAlign: TextAlign.center,
          style: new TextStyle(
            fontFamily: "SFProText",
            color: ConstantData.textColors,
          )),
      insetPadding: EdgeInsets.all(50),
      backgroundColor: ConstantData.cardBackground,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      content: Container(
        width: double.maxFinite,
        child: ListView.builder(
            shrinkWrap: true,
            itemExtent: 40.0,
            itemCount: widget.dayList.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(
                  widget.dayList[index],
                  style: new TextStyle(
                    fontFamily: "SFProText",
                    fontSize: 15,
                    color: ConstantData.textColors,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                trailing: Icon(
                  (widget.selectedList.contains(widget.dayList[index]))
                      ? Icons.check_box
                      : Icons.check_box_outline_blank,
                  color: (widget.selectedList.contains(widget.dayList[index]))
                      ? ConstantData.primaryColor
                      : Colors.grey,
                  size: 25,
                ),
                onTap: () {
                  if (widget.selectedList.contains(widget.dayList[index])) {
                    widget.selectedList.remove(widget.dayList[index]);
                  } else {
                    widget.selectedList.add(widget.dayList[index]);
                  }

                  setState(() {});
                },
              );
            }),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(S.of(context).cancel.toUpperCase(),
              style: TextStyle(fontSize: 15,
                  fontFamily: "SFProText",color: ConstantData.primaryColor)),
          onPressed: () {
            widget.onSelectedOkChanged(widget.selectedList);
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text(S.of(context).ok.toUpperCase(),
              style: TextStyle(fontSize: 15,
                  fontFamily: "SFProText",color: ConstantData.primaryColor)),
          onPressed: () {
            widget.onSelectedOkChanged(widget.selectedList);
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
