import 'dart:collection';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_gk_design/model/HistoryModel.dart';
import 'package:flutter_gk_design/model/ModelQuiz.dart';
import 'package:flutter_gk_design/model/ProgressModel.dart';
import 'package:flutter_gk_design/model/RemiderModel.dart';
import 'package:flutter_gk_design/model/SubCategories.dart';



import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final _databaseName = 'general_knowledge';
  static final _databaseVersion = 1;
  static final tableMainCat = 'category_table';
  static final tableReminder = 'tbl_reminder';
  static final tableSubCat = 'sub_category';
  static final progressTable = 'progress_table';
  static final historyTable = 'history_table';
  static final historyTableData = 'history_data';
  static final quizTable = 'quiz_data';

  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await _initDatabase();
    }

    return _database;
  }

  _initDatabase() async {
    var databasepath = await getDatabasesPath();
    String path = join(databasepath, _databaseName);

    var exists = await databaseExists(path);

    if (!exists) {
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (e) {
        print(e);
      }

      ByteData data = await rootBundle.load(join("assets", _databaseName));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      await File(path).writeAsBytes(bytes, flush: true);
    } else {}
    return await openDatabase(path, version: _databaseVersion, readOnly: false);
  }


  Future<List> getAllMainCatList() async {
    Database database = await instance.database;
    // var results = await database.query("SELECT * FROM $table_main_cat");
    var results = await database.query(tableMainCat);
    return results.toList();
  }


  Future<SubCategories> getSubCatByid(int id) async {
    // Future<ModelMainCat> getMainCatByid(Database assetDB, int id) async {
    // final db = assetDB;
    Database database = await instance.database;
    var res = await database
    // .query("sub_category where title = 'Football'");
        .query(tableSubCat, where: "id" + " = ?", whereArgs: [id]);
    SubCategories product =
    res.isNotEmpty ? SubCategories.fromMap(res.first) : null;
    return product;
  }

  Future<List<SubCategories>> getAllSubCatByMainCat(int id) async {
    Database database = await instance.database;

    var res = await database
    // .rawQuery("SELECT * FROM "+tableSubCat);
    // .query(tableSubCat, where: '$columnId' + " = ?", whereArgs: [id]);
        .query(tableSubCat, where: "ref_id" + " = ?", whereArgs: [id]);
    List<SubCategories> list = res.isNotEmpty
        ? res.map((c) => SubCategories.fromMap(c)).toList()
        : null;
    return list;
  }


  Future<List<ProgressModel>> getProgressLevels(
      String tableName, String type) async {
    Database database = await instance.database;

    var res = await database.query(progressTable,
        where: 'tableName=? AND type=?', whereArgs: [tableName, type]);

    List<ProgressModel> list = res.isNotEmpty
        ? res.map((c) => ProgressModel.fromMap(c)).toList()
        : null;
    return list;
  }

  Future<List<ModelQuiz>> getAllQuiz(int refId) async {
    Database database = await instance.database;

    var res = await database
        .query(quizTable, where: 'sub_cat_id=?', whereArgs: [refId]);

    List<ModelQuiz> list =
    res.isNotEmpty ? res.map((c) => ModelQuiz.fromMap(c)).toList() : null;
    return list;
  }

  Future<List<HistoryModel>> getHistoryData(int id) async {
    Database database = await instance.database;
    var res = await database
        .query(historyTableData, where: "refId" + " = ?", whereArgs: [id]);

    List<HistoryModel> list = res.isNotEmpty
        ? res.map((c) => HistoryModel.fromMap(c)).toList()
        : null;

    return list;
  }



  Future<int> insertHistoryData(HistoryModel historyModel) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['id'] = historyModel.id;
    map['refId'] = historyModel.refId;
    map['answer'] = historyModel.answer;
    map['optionList'] = historyModel.optionList;
    map['image'] = historyModel.image;
    map['userAnswer'] = historyModel.userAnswer;
    map['question'] = historyModel.question;
    print("insert----"+historyModel.answer);
    return await db.insert(historyTableData, map, nullColumnHack: 'id');

  }


  Future<List<ProgressModel>> getProgressLevelsByCat(String tableName, String type,int id) async {
    Database database = await instance.database;


    var res = await database.query(progressTable,
        where: 'tableName=? AND type=? AND id =?', whereArgs: [tableName, type,id]);



    // var res = await database
    //     .query(progressTable, where: "ref_id" + " = ?", whereArgs: [id]);
    List<ProgressModel> list = res.isNotEmpty
        ? res.map((c) => ProgressModel.fromMap(c)).toList()
        : null;
    return list;
  }



  Future<List<HistoryModel>> getRefIdList() async {
    Database database = await instance.database;
    var results = await database.query(historyTableData);

    List<HistoryModel> list = results.isNotEmpty
        ? results.map((c) => HistoryModel.fromMap(c)).toList()
        : null;
    return list;
  }


  Future<int> updateLevel(ProgressModel progressModel) async {
    // Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['progress'] = progressModel.progress;
    map['high_score'] = progressModel.highScore;
    map['score'] = progressModel.score;
    map['isShow'] = progressModel.isShow;

    return await db.update(progressTable, map,
        where: 'id = ?', whereArgs: [progressModel.id]);
    // return await db.insert(table_main_cat, mainCat.toMap(),nullColumnHack: 'id');
  }


  Future<int> unlockLevel(int id) async {
    // Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();

    map['isShow'] = 1;

    return await db.update(progressTable, map,
        where: 'id = ?', whereArgs: [id]);
    // return await db.insert(table_main_cat, mainCat.toMap(),nullColumnHack: 'id');
  }


  Future<int> insertHistory(String name, int id) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['name'] = name;
    map['refId'] = id;

    return await db.insert(historyTable, map, nullColumnHack: 'id');
  }

  Future<int> insertLevel(
      int progress, String tableName, int levelNo, String type) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['progress'] = progress;
    map['tableName'] = tableName;
    map['level_no'] = levelNo;
    map['high_score'] = 0;
    map['score'] = 0;
    map['type'] = type;

    if (levelNo == 1) {
      map['isShow'] = 1;
    } else {
      map['isShow'] = 0;
    }
    return await db.insert(progressTable, map, nullColumnHack: 'id');
  }



  Future<List<ReminderModel>> getReminderList() async {
    Database database = await instance.database;
    // var results = await database.query("SELECT * FROM $table_main_cat");
    var results = await database.query(tableReminder);

    List<ReminderModel> list = results.isNotEmpty
        ? results.map((c) => ReminderModel.fromMap(c)).toList()
        : null;

    print("object-value--$list");


    return list;
  }

  Future<int> insertReminder(String time, String repeat, int isOn) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['time'] = time;
    map['repeat'] = repeat;
    map['ison'] = isOn;

    return await db.insert(tableReminder, map, nullColumnHack: 'id');
  }

  Future<int> updateReminder(int ison, int id) async {
    // Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    var map = new HashMap<String, dynamic>();
    map['ison'] = ison;

    return await db
        .update(tableReminder, map, where: 'id = ?', whereArgs: [id]);
    // return await db.insert(table_main_cat, mainCat.toMap(),nullColumnHack: 'id');
  }
  Future<int> deleteRemider(int ids) async {
    Database database = await instance.database;
    print("id---"+ids.toString());
    return await database.rawDelete('DELETE FROM tbl_reminder WHERE id = ?', [ids]);
  }

  Future<List<ReminderModel>> getNotificationData(String time) async {
    Database database = await instance.database;
    var res = await database
        .query(tableReminder, where: "time" + " = ?", whereArgs: [time]);

    List<ReminderModel> list = res.isNotEmpty
        ? res.map((c) => ReminderModel.fromMap(c)).toList()
        : null;

    return list;
  }


}
