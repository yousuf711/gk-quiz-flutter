import 'dart:async';
import 'dart:convert';
import 'dart:math';

// import 'package:ads/ads.dart';
// import 'package:ads/ads.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gk_design/model/DataModel.dart';
import 'package:flutter_gk_design/model/MainModel.dart';
import 'package:flutter_gk_design/model/ModelQuiz.dart';
import 'package:flutter_gk_design/model/ModelQuiz1.dart';
import 'package:flutter_gk_design/model/ProgressModel.dart';
import 'package:flutter_gk_design/model/SubCategories.dart';
import 'package:flutter_gk_design/model/TestModel.dart';
import 'package:flutter_gk_design/util/MyClipper.dart';
import 'package:flutter_gk_design/util/PrefData.dart';
import 'package:flutter_gk_design/util/SizeConfig.dart';
import 'package:intl/intl.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:toast/toast.dart';
import 'package:vibration/vibration.dart';

import 'db/database_helper.dart';
import 'generated/l10n.dart';
import 'model/HistoryModel.dart';
import 'model/SubCategories.dart';
import 'util/ConstantData.dart';

String testDevice = 'YOUR_DEVICE_ID';

class QuizPage extends StatefulWidget {
  final ProgressModel progressModel;
  final SubCategories subCatModel;
  final MainModel mainModel;

  final ValueChanged<DataModel> onChanged;

  // final Ads ad;

  QuizPage(
      {this.progressModel, this.subCatModel, this.mainModel, this.onChanged});

  // {this.progressModel, this.subCatModel, this.mainModel,this.ad, this.onChanged});

  @override
  _QuizPage createState() => _QuizPage(
        progressModel: progressModel,
        subCatModel: subCatModel,
        mainModel: mainModel,
        onChanged: onChanged,
      );
}

//
String getCString(String myString) {
  return myString.substring(0, 1).toUpperCase() + myString.substring(1);
}

List shuffle(List items) {
  var random = new Random();
  for (var i = items.length - 1; i > 0; i--) {
    // Pick a pseudorandom number according to the list length
    var n = random.nextInt(i + 1);

    var temp = items[i];
    items[i] = items[n];
    items[n] = temp;
  }

  return items;
}

class _QuizPage extends State<QuizPage>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  Color appBarBackground;

  double topPosition;
  String questionImage;
  final SubCategories subCatModel;
  final MainModel mainModel;
  Animation<dynamic> animation;

  ProgressModel progressModel;
  List<TestModel> listView = new List();
  List<HistoryModel> historyModel = new List();
  bool isClick = true;
  int _fiftyAlpha = 255;

  int _timerAlpha = 255;
  bool _isHelpLine = false;
  bool _isRewarded = false;

  int _audienceAlpha = 255;
  bool isDialogOpen = false;
  bool _isVibration = true;

  ModelQuiz1 modelQuiz;
  List<ModelQuiz1> modelQuizList = List<ModelQuiz1>();
  DatabaseHelper _databaseHelper = DatabaseHelper.instance;
  int _selectedPosition = 0;
  int _totalSize = 0;
  int _rightCount = 0;
  int _wrongCount = 0;
  int _score = 0;
  int _plusScore = 0;
  int _helpCount = 0;
  bool _isVideoComplete = false;
  int _coin = 0;
  int _countDown = ConstantData.timer;
  Color _primaryColor;
  double _countDownPercent = 0;
  Timer _timer;
  int _refId = 1;
  int _fontSize = 16;
  AdmobReward rewardAd;
  bool isNightMode = false;
  final ValueChanged<DataModel> onChanged;

  _QuizPage(
      {this.progressModel, this.subCatModel, this.mainModel, this.onChanged});

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _cancelTimer();
    } else if (state == AppLifecycleState.resumed) {
      startTimer();
    }
  }

  Future<void> _setFontSize() async {
    _fontSize = await PrefData().getFontSize();
    _isVibration = await PrefData.getVibration();
    setState(() {});
  }
  void _getQuizList() {
    // modelQuizList =  List();

    _databaseHelper.getAllQuiz(subCatModel.id).then((value) {
      print("value258-----" + _selectedPosition.toString());
      // print("value258----$value----"+_selectedPosition.toString());
      List<dynamic> tempList;
      List<ModelQuiz1> selectedQuizList=[];
      var rng = new Random();
      rootBundle.loadString('assets/quiz_1.json').then((locaFile) {
        tempList = json.decode(locaFile);
        for (var i = 0; i < 10; i++) {
          selectedQuizList.add(ModelQuiz1.fromJson(tempList[rng.nextInt(tempList.length)]));
        }
        if (selectedQuizList != null) {
          // setState(() {
          print("modelQuizListlength----" + value.length.toString());

          modelQuizList = selectedQuizList;

          _totalSize = modelQuizList.length;

          //
          for (int i = 0; i < modelQuizList.length; i++) {
            ModelQuiz1 dataModel = modelQuizList[i];

            List<String> optionList = List();

            optionList.add(getCString(dataModel.option_1));
            optionList.add(getCString(dataModel.option_2));
            optionList.add(getCString(dataModel.option_3));
            optionList.add(getCString(dataModel.option_4));

            shuffle(optionList);

            modelQuizList[i].allOptionList = optionList;

            // print("allOptionList----" +
            //     modelQuizList[_selectedPosition].allOptionList[0]);

            if (modelQuizList[i].image == null ||
                modelQuizList[i].image.isEmpty) {
              modelQuizList[i].isVisibleImage = false;
            } else {
              modelQuizList[i].isVisibleImage = true;
            }
          }
          // });

          setState(() {
            if (modelQuizList.length > 0) {
              modelQuiz = modelQuizList[_selectedPosition];
              questionImage = modelQuiz.image;
              listView = new List();
              listView.add(new TestModel(false));
              listView.add(new TestModel(false));
              listView.add(new TestModel(false));
              listView.add(new TestModel(false));
            }
            startTimer();
            _isHelpLine = true;
          });
        }


      });
    });
  }
  void _getQuizList1() {
    // modelQuizList =  List();

    // _databaseHelper.getAllQuiz(subCatModel.id).then((value) {
    //   print("value258-------------------------------------------------------" + _selectedPosition.toString());
    //   // print("value258----$value----"+_selectedPosition.toString());
    //   if (value != null) {
    //     // setState(() {
    //     print("modelQuizListlength----" + value.length.toString());
    //
    //     modelQuizList = value;
    //
    //     _totalSize = modelQuizList.length;
    //
    //     //
    //     for (int i = 0; i < modelQuizList.length; i++) {
    //       ModelQuiz dataModel = modelQuizList[i];
    //
    //       List<String> optionList = List();
    //
    //       optionList.add(getCString(dataModel.option_1));
    //       optionList.add(getCString(dataModel.option_2));
    //       optionList.add(getCString(dataModel.option_3));
    //       optionList.add(getCString(dataModel.option_4));
    //
    //       shuffle(optionList);
    //
    //       modelQuizList[i].allOptionList = optionList;
    //
    //       // print("allOptionList----" +
    //       //     modelQuizList[_selectedPosition].allOptionList[0]);
    //
    //       if (modelQuizList[i].image == null ||
    //           modelQuizList[i].image.isEmpty) {
    //         modelQuizList[i].isVisibleImage = false;
    //       } else {
    //         modelQuizList[i].isVisibleImage = true;
    //       }
    //     }
    //     // });
    //
    //     setState(() {
    //       if (modelQuizList.length > 0) {
    //         modelQuiz = modelQuizList[_selectedPosition];
    //         questionImage = modelQuiz.image;
    //         listView = new List();
    //         listView.add(new TestModel(false));
    //         listView.add(new TestModel(false));
    //         listView.add(new TestModel(false));
    //         listView.add(new TestModel(false));
    //       }
    //       startTimer();
    //       _isHelpLine = true;
    //     });
    //   }
    // });
  }

  void startTimer() {
    _cancelTimer();
    print("_countDown12" + _countDown.toString());
    var oneSec = Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_countDown < 1) {
            timer.cancel();
            if (!isDialogOpen) {
              isDialogOpen = true;
              _timeOutDialog();
            }
          } else {
            setState(() {
              _countDown = _countDown - 1;
              _countDownPercent =
                  ((_countDown * ConstantData.timer) / 100) / 10;
              _plusScore = getPlusScore(_countDown);
              print("countDown--" + _countDownPercent.toString());
            });
          }
        },
      ),
    );
  }

  void _nextQuiz() {
    new Future.delayed(new Duration(seconds: ConstantData.delayTime), () {
      setState(() {
        _cancelTimer();
        isClick = true;
        _isHelpLine = true;
        listView = null;
        listView = new List();

        listView.add(new TestModel(false));
        listView.add(new TestModel(false));
        listView.add(new TestModel(false));
        listView.add(new TestModel(false));

        if (_selectedPosition < modelQuizList.length - 1) {
          _selectedPosition++;

          modelQuiz = modelQuizList[_selectedPosition];
          questionImage = modelQuiz.image;
          _countDown = ConstantData.timer;
          startTimer();
        } else {
          _passData();
        }
      });
    });
  }

  Future<bool> _passData() {
    rewardAd.dispose();
    var now = new DateTime.now();
    var date = DateFormat('yy-dd-mm-hh:mm:ss').format(now);

    if (_score > progressModel.highScore) {
      progressModel.highScore = _score;
    }
    progressModel.score = _score;

    double percentageProgress = (_rightCount * 100) / modelQuizList.length;

    if (percentageProgress < 35) {
      progressModel.progress = 1;
    } else if (percentageProgress > 35 && percentageProgress < 75) {
      progressModel.progress = 2;
    } else if (percentageProgress > 75) {
      progressModel.progress = 3;
    }
    print("_refId---" + _refId.toString());

    print("percentageProgress---" +
        percentageProgress.toString() +
        "---" +
        _refId.toString());
    if (progressModel.progress > 0) {
      _databaseHelper.insertHistory(S.of(context).test + date, _refId);

      if (ConstantData.levelSize >= progressModel.levelNo) {
        _databaseHelper.unlockLevel((progressModel.id + 1));
      }
    }
    _databaseHelper.updateLevel(progressModel);

    print("rightCount---" + _rightCount.toString());

    DataModel dataModel = new DataModel();
    dataModel.progressModel = progressModel;
    dataModel.subCatModel = subCatModel;
    dataModel.modelCat = mainModel;
    dataModel.rightCount = _rightCount;
    dataModel.wrongCount = _wrongCount;
    dataModel.refId = _refId;

    Navigator.of(context).pop();
    widget.onChanged(dataModel);

    return new Future.value(false);
  }

  void _getLiveDialog() {
    _isVideoComplete = true;
    //
    // AlertDialog _createMaterialAlertDialog() => new AlertDialog(
    //       backgroundColor: ConstantData.cardBackground,
    //       title: new Text(
    //         S.of(context).congratulations,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       content: new Text(
    //         S.of(context).liveMessage,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       actions: <Widget>[
    //         new MaterialButton(
    //             onPressed: () {
    //               Navigator.pop(context);
    //               isDialogOpen = false;
    //               setState(() {
    //                 _helpCount = _helpCount - 2;
    //                 _nextQuiz();
    //               });
    //             },
    //             child: new Text(
    //               S.of(context).ok,
    //               style: TextStyle(color: ConstantData.textColors),
    //             )),
    //       ],
    //     );

    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return _createMaterialAlertDialog();
    //     });

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Wrap(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: ConstantData.getGradientColors(),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).congratulations,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 22,
                            fontFamily: "SFProText",
                            color: Colors.white),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            S.of(context).liveMessage,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                                fontFamily: "SFProText",
                                color: Colors.white),
                          )),
                      InkWell(
                          child: Container(
                              height: SizeConfig.safeBlockVertical * 6,
                              padding: EdgeInsets.only(right: 15),
                              child: Center(
                                child: Text(
                                  S.of(context).ok,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                      fontFamily: "SFProText",
                                      color: Colors.white),
                                ),
                              )),
                          onTap: () {
                            Navigator.pop(context);
                            isDialogOpen = false;
                            setState(() {
                              _helpCount = _helpCount - 2;
                              _nextQuiz();
                            });
                          }),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  void _getShowVideoDialog() {
    // AlertDialog _createMaterialAlertDialog() => new AlertDialog(
    //       backgroundColor: ConstantData.cardBackground,
    //       title: new Text(
    //         S.of(context).gameOverHeader,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       content: new Text(
    //         S.of(context).gameOverMessage,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       actions: <Widget>[
    //         new MaterialButton(
    //           onPressed: () {
    //             // ads.showVideoAd(state: this);
    //
    //             // if (!_isVideoComplete) {
    //             if (_isVideoLoaded) {
    //               ads.showVideoAd(state: this);
    //               Navigator.pop(context);
    //             } else {
    //               _showToast(S.of(context).strVideoError);
    //             }
    //             // }
    //           },
    //           child: new Text(
    //             S.of(context).showVideo,
    //             style: TextStyle(color: ConstantData.textColors),
    //           ),
    //         ),
    //         new MaterialButton(
    //             onPressed: () {
    //               Navigator.pop(context);
    //               _passData();
    //             },
    //             child: new Text(
    //               S.of(context).gameOver,
    //               style: TextStyle(color: ConstantData.textColors),
    //             )),
    //       ],
    //     );
    //
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return _createMaterialAlertDialog();
    //     });

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Wrap(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: ConstantData.getGradientColors(),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).gameOverHeader,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 22,
                            fontFamily: "SFProText",
                            color: Colors.white),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            S.of(context).gameOverMessage,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                                fontFamily: "SFProText",
                                color: Colors.white),
                          )),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                                child: Container(
                                  padding: EdgeInsets.only(right: 15),
                                  height: SizeConfig.safeBlockVertical * 6,
                                  child: Center(
                                      child: Text(
                                    S.of(context).showVideo,
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 15,
                                        fontFamily: "SFProText",
                                        color: Colors.white),
                                  )),
                                ),
                                onTap: () async {
                                  if (await rewardAd.isLoaded) {
                                    _isRewarded = false;
                                    // videoAds.showVideoAd(state: this);
                                    rewardAd.show();
                                    Navigator.pop(context);
                                  } else {
                                    _showToast(S.of(context).strVideoError);
                                  }
                                }),
                            InkWell(
                                child: Container(
                                  height: SizeConfig.safeBlockVertical * 6,
                                  child: Center(
                                    child: Text(
                                      S.of(context).gameOver,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 15,
                                          fontFamily: "SFProText",
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.pop(context);
                                  _passData();
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  void _timeOutDialog() {
    // AlertDialog _createMaterialAlertDialog() => new AlertDialog(
    //       backgroundColor: ConstantData.cardBackground,
    //       title: new Text(
    //         S.of(context).timeOut,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       content: new Text(
    //         S.of(context).playNextQuiz,
    //         style: TextStyle(color: ConstantData.textColors),
    //       ),
    //       actions: <Widget>[
    //         new MaterialButton(
    //           onPressed: () {
    //             Navigator.pop(context);
    //             _nextQuiz();
    //           },
    //           child: new Text(
    //             S.of(context).nextQuiz,
    //             style: TextStyle(color: ConstantData.textColors),
    //           ),
    //         ),
    //         new MaterialButton(
    //             onPressed: () {
    //               Navigator.pop(context);
    //               _passData();
    //             },
    //             child: new Text(
    //               S.of(context).gameOver,
    //               style: TextStyle(color: ConstantData.textColors),
    //             )),
    //       ],
    //     );
    //
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return _createMaterialAlertDialog();
    //     });

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Wrap(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: ConstantData.getGradientColors(),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).timeOut,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 22,
                            fontFamily: "SFProText",
                            color: Colors.white),
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            S.of(context).playNextQuiz,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                                fontFamily: "SFProText",
                                color: Colors.white),
                          )),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                                child: Container(
                                    padding: EdgeInsets.only(right: 15),
                                    height: SizeConfig.safeBlockVertical * 6,
                                    child: Center(
                                      child: Text(
                                        S.of(context).nextQuiz,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      ),
                                    )),
                                onTap: () {
                                  Navigator.pop(context);
                                  _nextQuiz();
                                }),
                            InkWell(
                                child: Container(
                                    padding: EdgeInsets.only(right: 15),
                                    height: SizeConfig.safeBlockVertical * 6,
                                    child: Center(
                                      child: Text(
                                        S.of(context).gameOver,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      ),
                                    )),
                                onTap: () {
                                  Navigator.pop(context);
                                  _passData();
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  void _cancelTimer() {
    print("timer---true");
    if (_timer != null) {
      _timer.cancel();
    }
  }

  @override
  void dispose() {
    rewardAd.dispose();
    _cancelTimer();
    super.dispose();
  }

  // Ads videoAds;

  void handleEvent(
      AdmobAdEvent event, Map<String, dynamic> args, String adType) {
    switch (event) {
      case AdmobAdEvent.loaded:
        break;
      case AdmobAdEvent.opened:
        break;
      case AdmobAdEvent.closed:
        print("videoCLose---true");

        rewardAd.load();

        if (_isRewarded) {
          _getLiveDialog();
        } else {
          if (!_isVideoComplete) {
            _passData();
          }
        }

        break;
      case AdmobAdEvent.failedToLoad:
        break;
      case AdmobAdEvent.rewarded:
        _isRewarded = true;
        break;
      default:
    }
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    ConstantData.setThemePosition();
    _setFontSize();

    // videoAds = widget.ad;

    rewardAd = AdmobReward(
      adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        print("event12----1--true" + event.toString());
        handleEvent(event, args, 'Reward');
      },
    );

    // rewardAd = AdmobReward(
    //   adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
    //   listener: (AdmobAdEvent event, Map<String, dynamic> args) {
    //
    //     print("event12----1" + event.toString());
    //     if (event == AdmobAdEvent.closed){
    //       print("event12----" + event.toString());
    //     }
    //
    //     // if (event == AdmobAdEvent.closed) rewardAd.load();
    //     handleEvent(event, args, 'Reward');
    //   },
    // );

    rewardAd.load();

    // videoAds = Ads(ConstantData.getAppAdUnitId());

    // var videoListener =
    //     (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
    //   if (event == RewardedVideoAdEvent.rewarded) {
    //     _getLiveDialog();
    //   } else if (event == RewardedVideoAdEvent.closed) {
    //     _passData();
    //   } else if (event == RewardedVideoAdEvent.loaded) {
    //     print("video------video loaded.");
    //     _isVideoLoaded = true;
    //     setState(() {});
    //   } else if (event == RewardedVideoAdEvent.failedToLoad) {
    //     print("video---video failed.");
    //   }
    // };
    //
    // videoAds.setVideoAd(
    //   adUnitId: ConstantData.getRewardBasedVideoAdUnitId(),
    //   keywords: ['dart', 'java'],
    //   childDirected: false,
    //   testDevices: null,
    //   listener: videoListener,
    // );

    topPosition = -80;
    appBarBackground = Colors.transparent;

    print("isRun---true$_selectedPosition");
    ConstantData.setThemePosition();

    _primaryColor = ConstantData.primaryColor;
    _getQuizList();

    _databaseHelper.getRefIdList().then((value) {
      setState(() {
        if (value != null) {
          _refId = value[value.length - 1].refId;
          //     // print("_refId12--$_refId");
        }
        _refId++;
      });
    });

    WidgetsBinding.instance.addObserver(this);
  }

  Future<bool> _requestPop() {
    _cancelTimer();

    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => AllLevel(this.subCatModel, this.MainModel),
    //   ),
    // );
    // DataModel dataModel = new DataModel();
    // dataModel.isPass = true;
    // widget.onChanged(dataModel);
    // print("back---1234");

    return showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Wrap(
                  children: [
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(SizeConfig.safeBlockVertical * 3),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: ConstantData.getGradientColors(),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            S.of(context).quit,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 22,
                                fontFamily: "SFProText",
                                color: Colors.white),
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Text(
                                S.of(context).quitMsg,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 18,
                                    fontFamily: "SFProText",
                                    color: Colors.white),
                              )),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                InkWell(
                                    child: Container(
                                        height:
                                            SizeConfig.safeBlockVertical * 6,
                                        padding: EdgeInsets.only(right: 15),
                                        child: Center(
                                          child: Text(
                                            S.of(context).no,
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 15,
                                                fontFamily: "SFProText",
                                                color: Colors.white),
                                          ),
                                        )),
                                    onTap: () {
                                      Navigator.pop(context);
                                      startTimer();
                                    }),
                                InkWell(
                                    child: Container(
                                      height: SizeConfig.safeBlockVertical * 6,
                                      child: Center(
                                          child: Text(
                                        S.of(context).yes,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15,
                                            fontFamily: "SFProText",
                                            color: Colors.white),
                                      )),
                                    ),
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.of(context).pop();
                                    })
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            }) ??
        false;
    // Navigator.of(context).pop();

    // return new Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return WillPopScope(
      child: Scaffold(
        backgroundColor: ConstantData.primaryColor,
        body: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: ClipPath(
                clipper: MyClipper(),
                child: Container(
                  height: SizeConfig.safeBlockVertical * 55,
                  width: double.infinity,
                  color: ConstantData.backgroundColors,
                ),
              ),
            ),
            Column(
              children: [
                Expanded(
                    child: Stack(
                  children: [
                    Container(
                        height: SizeConfig.safeBlockVertical * 50,
                        width: double.infinity,
                        // color: ConstantData.primaryColor,
                        child: Container(
                          // padding: const EdgeInsets.only(right: 10),
                          child: Stack(
                            children: [
                              _appBar(),
                              _questionView(),
                            ],
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          top: SizeConfig.safeBlockVertical * 57,
                          right: SizeConfig.safeBlockVertical * 0.8,
                          left: SizeConfig.safeBlockVertical * 0.8,
                          bottom: SizeConfig.safeBlockVertical * 0.8),
                      child: Column(
                        children: [
                          Expanded(
                            child: _optionView(),
                            flex: 5,
                          ),
                          Expanded(
                            child: _bottomView(),
                            flex: 1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ))
              ],
            )
          ],
        ),
      ),
      onWillPop: _requestPop,
    );
  }

  Widget _setImage() {
    print("question_image--$questionImage");
    if (questionImage == null || questionImage.isEmpty) {
      return Container();
    } else {
      return Image.asset(
        "assets/images/" + questionImage,
        fit: BoxFit.none,
      );
    }
  }

  Widget _questionView() {
    return Container(
      margin: EdgeInsets.only(top: SizeConfig.safeBlockVertical * 8),
      height: SizeConfig.safeBlockVertical * 48,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 35, left: 35, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(5),
              color: ConstantData.shadow2,
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(right: 25, left: 25, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.circular(8),
              color: ConstantData.shadow1,
            ),
          ),
          Container(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: 15, right: 15, left: 15),
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.circular(10),
                color: ConstantData.cardBackground,
              ),
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [
                    _dataView(),
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(
                              top: SizeConfig.safeBlockVertical * 3),
                          child: Column(
                            children: [
                              Visibility(
                                visible: (questionImage != null),
                                child: Expanded(
                                  child: _setImage(),
                                  flex: 1,
                                ),
                              ),
                              Visibility(
                                visible: true,
                                child: Expanded(
                                  child: Center(
                                    child: AutoSizeText(
                                      (modelQuizList.length > 0)
                                          ? modelQuiz.question
                                          : "Question",
                                      maxLines: 3,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: "SFProText",
                                          fontWeight: FontWeight.w500,
                                          color: ConstantData.textColors,
                                          fontSize: _fontSize.toDouble()),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                              )
                            ],
                          )),
                      flex: 1,
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget _dataView() {
    return Container(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Image.asset('assets/images/right.png',
                            height: 15, width: 15, fit: BoxFit.fitWidth),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            _rightCount.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: ConstantData.textColors),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Image.asset(
                          'assets/images/wrong.png',
                          height: 15,
                          width: 15,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            _wrongCount.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: ConstantData.textColors),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Image.asset(
                          'assets/images/coin.png',
                          height: 15,
                          width: 15,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            _coin.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: "SFProText",
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: ConstantData.textColors),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              flex: 1,
            ),
            Expanded(
              child: _progressView(),
              flex: 1,
            ),
            Expanded(
              child: _scoreView(),
              flex: 1,
            )
          ],
        ));
  }

  Widget _progressView() {
    return Align(
        alignment: Alignment.topCenter,
        child: Container(
            width: 60,
            height: 60,
            child: Center(
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: CircularStepProgressIndicator(
                      totalSteps: ConstantData.timer,
                      currentStep: _countDown,
                      stepSize: 5,
                      selectedColor: ConstantData.primaryColor,
                      unselectedColor: ConstantData.backgroundColors,
                      padding: 0,
                      width: 70,
                      height: 70,
                      selectedStepSize: 5,
                      roundedCap: (_, __) => true,
                    ),
                  ),

                  Align(
                    alignment: Alignment.center,
                    child: new Text(
                      "$_countDown",
                      style: new TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                          color: ConstantData.textColors),
                    ),
                  )

                  // ,
                ],
              ),
            )));
  }

  int getPlusScore(int countTime) {
    if (countTime < 30 && countTime >= 25) {
      return 500;
    } else if (countTime < 25 && countTime >= 15) {
      return 400;
    } else if (countTime < 15 && countTime >= 5) {
      return 250;
    } else {
      return 100;
    }
  }

  Widget _scoreView() {
    return Container(
        child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              S.of(context).score,
              style: TextStyle(
                  fontFamily: "SFProText",
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  color: ConstantData.textColors),
            ),
            SizedBox(width: 5),
            Column(
              children: [
                Container(
                  // margin: EdgeInsets.only(left: 5),
                  child: Text(
                    _score.toString(),
                    style: TextStyle(
                        fontFamily: "SFProText",
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                        color: ConstantData.textColors),
                  ),
                ),
                // SizedBox(height: 5),

                Container(
                  // margin: EdgeInsets.only(left: 5),
                  child: Text(
                    S.of(context).plusSign + _plusScore.toString(),
                    style: TextStyle(
                        fontFamily: "SFProText",
                        fontWeight: FontWeight.w300,
                        fontSize: 12,
                        color: ConstantData.textColors),
                  ),
                ),
              ],
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Icon(_helpCount <= 2 ? Icons.favorite : Icons.favorite_border,
                // Icon(_helpCount > 0 ? Icons.favorite : Icons.favorite_border,
                color: _primaryColor,
                size: 20),
            Icon(_helpCount <= 1 ? Icons.favorite : Icons.favorite_border,
                // Icon(_helpCount > 1 ? Icons.favorite : Icons.favorite_border,
                color: _primaryColor,
                size: 20),
            Icon(_helpCount <= 0 ? Icons.favorite : Icons.favorite_border,
                // Icon(_helpCount > 2 ? Icons.favorite : Icons.favorite_border,
                color: _primaryColor,
                size: 20),
          ],
        )
      ],
    )
        //   children: [

        );
  }

  Widget _appBar() {
    return Container(
        height: SizeConfig.safeBlockVertical * 7,
        margin: EdgeInsets.only(right: 15),
        child: Stack(
          children: [
            Container(
              height: SizeConfig.safeBlockVertical * 7,
              child: Center(
                child: Text(
                  subCatModel.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "SFProText",
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      color: Colors.white),
                ),
              ),
            ),
            Container(
              height: SizeConfig.safeBlockVertical * 7,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: new IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: 20,
                      ),
                      onPressed: () {
                        _requestPop();
                      },
                    ),
                  ),
                  Text(
                    (_selectedPosition + 1).toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 22,
                        fontFamily: "SFProText",
                        color: Colors.white),
                  ),
                  Text(
                    "/$_totalSize",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        fontFamily: "SFProText",
                        color: Colors.white),
                  ),
                  new Spacer(),
                  GestureDetector(
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: Center(
                          child: Icon(
                        Icons.skip_next,
                        color: ConstantData.primaryColor,
                        size: 22,
                      )),
                    ),
                    onTap: () {
                      setState(() {
                        _skipData();
                      });
                    },
                  ),
                ],
              ),
            )
          ],
        ));
  }

  _showToast(String s) {
    Toast.show(s, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);

    // Custom Toast Position
  }

  void _checkAnswer(String s, int pos) {
    if (isClick) {
      setState(() {
        HistoryModel hi = new HistoryModel();
        hi.question = modelQuiz.question;
        hi.answer = modelQuiz.answer;
        hi.userAnswer = s;
        hi.optionList = modelQuiz.allOptionList.toString();
        hi.image = modelQuiz.image;
        // hi.id = (historyModel.length + 1);
        hi.refId = _refId;
        _databaseHelper.insertHistoryData(hi);

        print("refId56---" + _refId.toString());

        _databaseHelper.getHistoryData(_refId).then((value) {
          print("value---" + value.length.toString());
        });

        historyModel.add(hi);
        listView[pos].visibility = true;
        isClick = false;

        // if (s.trim() == (modelQuiz.answer.trim()) ||
        //     s.trim().toLowerCase() == (modelQuiz.answer.trim().toLowerCase())) {
        //

        if ((pos+1).toString() == (modelQuiz.answer)) {
          _nextQuiz();
          _addCoin();
          listView[pos].isCheckAnswer = true;
        } else {
          vibrate();
          _wrongCount++;
          _minusCoin();
          _helpCount++;
          listView[pos].isCheckAnswer = false;

          if (_helpCount > 3) {
            if (!_isVideoComplete) {
              _cancelTimer();
              _getShowVideoDialog();
            } else {
              _passData();
            }

            // _requestPop();
          } else {
            _nextQuiz();
          }
        }
      });
    }
  }

  Future<void> _minusCoin() async {
    int coins = await PrefData.getCoins();

    if ((coins - 5) > 0) {
      PrefData.setCoins((coins - 5));
    } else {
      PrefData.setCoins(0);
    }

    _coin = await PrefData.getCoins();
  }

  Future<void> vibrate() async {
    if (_isVibration) {
      if (await Vibration.hasVibrator()) {
        Vibration.vibrate();
      }
    }
  }

  Future<void> _addCoin() async {
    int coins = await PrefData.getCoins();

    _rightCount++;
    _score = _score + _plusScore;
    PrefData.setCoins((coins + 10));
    _coin = await PrefData.getCoins();
  }

  Future<void> _skipData() async {
    int coins = await PrefData.getCoins();
    if ((coins - ConstantData.hintCoin) >= 0) {
      _cancelTimer();
      PrefData.setCoins((coins - 10));
      _coin = await PrefData.getCoins();
      _nextQuiz();
    } else {
      print("toast---true12" + coins.toString());
      _showToast(S.of(context).coinsError);
    }
  }

  Color _getViewColor(int i) {
    if (listView.length > 0) {
      if (listView[i].visibility) {
        if (listView[i].isCheckAnswer) {
          return Colors.green;
        } else {
          // return Colors.white;
          return Colors.red;
        }
      } else {
        return ConstantData.cardBackground;
      }
    } else {
      return ConstantData.cardBackground;
    }
  }

  Color _getTextColor(int i) {
    if (listView.length > 0) {
      if (listView[i].visibility) {
        if (listView[i].isCheckAnswer) {
          return Colors.white;
        } else {
          return Colors.white;
        }
      } else {
        return ConstantData.textColors;
      }
    } else {
      return ConstantData.textColors;
    }
  }

  Widget getOptionCell(int position) {
    return Visibility(
      visible: (listView != null && listView.length > position)
          ? listView[position].viewVisibility
          : true,
      child: BouncingWidget(
          duration: Duration(milliseconds: 100),
          scaleFactor: 1.5,
          onPressed: () {
            _checkAnswer(modelQuiz.allOptionList[position], position);
          },
          child: Stack(
            children: [
              Container(
                  child: Card(
                color: _getViewColor(position),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                margin: EdgeInsets.only(
                    top: SizeConfig.safeBlockVertical * 1.2,
                    bottom: SizeConfig.safeBlockVertical * 1.2,
                    left: SizeConfig.safeBlockVertical * 6,
                    right: SizeConfig.safeBlockVertical * 6),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Center(
                    child: AutoSizeText(
                      (modelQuizList.length > 0)
                          ? modelQuiz.allOptionList[position]
                          : "",
                      maxLines: 1,
                      style: TextStyle(
                          fontFamily: "SFProText",
                          fontWeight: FontWeight.w500,
                          color: _getTextColor(position)),
                    ),
                  ),
                ),
              )),
              Container(
                margin: EdgeInsets.only(
                    top: SizeConfig.safeBlockVertical * 1.2,
                    bottom: SizeConfig.safeBlockVertical * 1.2,
                    left: SizeConfig.safeBlockVertical * 8,
                    right: SizeConfig.safeBlockVertical * 8),
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8),
                  color: Colors.transparent,
                ),
                child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        (modelQuizList.length > 0)
                            ? listView[position].percentage
                            : "",
                        style: TextStyle(
                            fontFamily: "SFProText",
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: _primaryColor),
                      ),
                    )),
              ),
            ],
          )
          // InkWell(
          //   child: ,
          //   onTap: () {
          //
          //   },
          // ),
          ),
    );
  }

  Widget _optionView() {
    return Container(
      // alignment: FractionalOffset.bottomCenter,
      child: Column(
        children: [
          Expanded(
            child: getOptionCell(0),
            flex: 1,
          ),
          Expanded(
            child: getOptionCell(1),
            flex: 1,
          ),
          Expanded(
            child: getOptionCell(2),
            flex: 1,
          ),
          Expanded(
            child: getOptionCell(3),
            flex: 1,
          ),
        ],
      ),
    );
  }

  void _timerAnswer() {
    for (int i = 0; i < listView.length; i++) {
      setState(() {
        listView[i].viewVisibility = false;
      });
    }

    int answerNumber = 0;
    for (int i = 0; i < modelQuiz.allOptionList.length; i++) {
      if (modelQuiz.allOptionList[i].toString() == modelQuiz.answer) {
        answerNumber = i;
      }
    }

    listView[answerNumber].viewVisibility = true;
  }

  void _percentage() {
    final int random1 = new Random().nextInt((100 - 70) + 1) + 70;
    final int random2 = new Random().nextInt((70 - 45) + 1) + 45;
    final int random3 = new Random().nextInt((45 - 30) + 1) + 30;
    final int random4 = new Random().nextInt((30 - 10) + 1) + 10;

    int answerPosition = 0;

    for (int i = 0; i < modelQuiz.allOptionList.length; i++) {
      if (modelQuiz.allOptionList[i].toString() == modelQuiz.answer) {
        answerPosition = i;
      }
    }

    List<int> integerArrayList = new List();
    integerArrayList.add(random2);
    integerArrayList.add(random3);
    integerArrayList.add(random4);

    shuffle(integerArrayList);

    setState(() {
      listView[answerPosition].percentage = "$random1%";
      int count = -1;
      for (int i = 0; i < listView.length; i++) {
        if (i != answerPosition) {
          count++;
          int per = integerArrayList[count];
          listView[i].percentage = "$per%";
          print("listView===" +
              listView[i].percentage.toString() +
              "===" +
              (modelQuizList.length > 0).toString());
        }
      }
    });
  }

  void _helpLineMethod() {
    Random randomNumber = new Random();
    String checkAnswer;

    checkAnswer = modelQuiz.answer;

    int answerNumber = 0;
    for (int i = 0; i < modelQuiz.allOptionList.length; i++) {
      if (modelQuiz.allOptionList[i].toString() == (checkAnswer)) {
        answerNumber = i;
      }
    }

    print("answerNumber0----" + answerNumber.toString() + "----" + checkAnswer);
    int helpTag = randomNumber.nextInt(3) + 1;

    if (helpTag == answerNumber) {
      helpTag = randomNumber.nextInt(3) + 1;
    }

    if (helpTag == answerNumber) {
      helpTag = randomNumber.nextInt(3) + 1;
    }

    for (int i = 0; i < listView.length; i++) {
      setState(() {
        listView[i].viewVisibility = false;
      });
    }

    print("answerNumber0----" +
        answerNumber.toString() +
        "----" +
        helpTag.toString());

    listView[helpTag].viewVisibility = true;
    listView[answerNumber].viewVisibility = true;
    setState(() {});
  }

  Widget _bottomView() {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              child: Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ConstantData.primaryColor.withAlpha(_fiftyAlpha),
                    // gradient: LinearGradient(
                    //   begin: Alignment.topLeft,
                    //   end: Alignment.bottomRight,
                    //   colors: _colors,
                    // ),
                  ),
                  child: Center(
                    child: Image.asset(
                      "assets/images/speed.png",
                      height: 25,
                      width: 25,
                      color: Colors.white,
                    ),
                  )),
              onTap: () {
                setState(() {
                  if (_isHelpLine && _fiftyAlpha != 130) {
                    _isHelpLine = false;
                    _fiftyAlpha = 130;
                    _helpLineMethod();
                  } else {
                    _showToast(S.of(context).lifelineError);
                  }
                });
              },
            ),
            flex: 1,
          ),
          Expanded(
            child: GestureDetector(
              child: Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ConstantData.primaryColor.withAlpha(_timerAlpha),
                  ),
                  child: Center(
                    child: Icon(
                      Icons.timer_outlined,
                      color: Colors.white,
                      size: 25,
                    ),
                  )),
              onTap: () {
                if (_isHelpLine && _timerAlpha != 130) {
                  _isHelpLine = false;
                  _timerAlpha = 130;
                  _timerAnswer();
                } else {
                  _showToast(S.of(context).lifelineError);
                }
              },
            ),
            flex: 1,
          ),
          Expanded(
            child: GestureDetector(
              child: Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ConstantData.primaryColor.withAlpha(_audienceAlpha),
                  ),
                  child: Center(
                    child: Icon(
                      Icons.supervised_user_circle,
                      color: Colors.white,
                      size: 25,
                    ),
                  )),
              onTap: () {
                if (_isHelpLine && _audienceAlpha != 130) {
                  _isHelpLine = false;
                  _audienceAlpha = 130;
                  _percentage();
                } else {
                  _showToast(S.of(context).lifelineError);
                }
              },
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }
}
